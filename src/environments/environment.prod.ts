export const environment = {
  production: true,
  GRAPHQL_HTTPS_BACKEND: 'https://ehealth-consulting-backend.herokuapp.com/graphql',
  GRAPHQL_WSS_BACKEND: 'wss://ehealth-consulting-backend.herokuapp.com/graphql'
};
