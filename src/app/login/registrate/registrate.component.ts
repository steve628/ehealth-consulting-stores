import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmailValidatorService } from '@app-graphql/services/email-validator.service';
import { UserGqlService } from '@app-graphql/services/user-gql.service';
import { PasswordValidator } from '@app-core/validators/password-validator';
import { RegistrationData } from '@app-graphql/generated/graphql';

@Component({
  selector: 'app-registrate',
  templateUrl: './registrate.component.html',
  styleUrls: ['./registrate.component.css']
})
export class RegistrateComponent implements OnInit {
  registerForm: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private evs: EmailValidatorService,
    private userService: UserGqlService
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.email], [this.evs]],
      forename: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      passwords: this.fb.group({
        password: ['', [Validators.required, Validators.minLength(5)]],
        passwordRepeat: ''
      }, { validators: PasswordValidator.passwordEquality })
    });
    // Validators.pattern('^(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=\\D*\\d)[A-Za-z\\d!$%@#£€*?&]{8,}$');
  }

  registrate(): void {
    const data: RegistrationData = {
      email: this.registerForm.get('email').value,
      forename: this.registerForm.get('forename').value,
      lastname: this.registerForm.get('lastname').value,
      password: this.registerForm.get('passwords').get('password').value
    };
    this.userService.registrateUser(data);
    alert('Prüfen Sie ihre Emails!'); // replace with dialog box
    this.registerForm.reset();
    this.router.navigate(['../', 'login']);
  }
}
