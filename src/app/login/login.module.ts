import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { SigninComponent } from './signin/signin.component';
import { RegistrateComponent } from './registrate/registrate.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ErrorModule } from '../error/error.module';
import { RegistrateConsultantComponent } from './registrate-consultant/registrate-consultant.component';
import { MaterialModule } from '@app-material/material.module';
import { CoreModule } from '@app-core/core.module';


@NgModule({
  declarations: [SigninComponent, RegistrateComponent, RegistrateConsultantComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    ReactiveFormsModule,
    ErrorModule,
    MaterialModule,
    CoreModule
  ]
})
export class LoginModule { }
