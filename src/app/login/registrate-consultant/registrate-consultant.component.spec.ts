import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrateConsultantComponent } from './registrate-consultant.component';

describe('RegistrateConsultantComponent', () => {
  let component: RegistrateConsultantComponent;
  let fixture: ComponentFixture<RegistrateConsultantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrateConsultantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrateConsultantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
