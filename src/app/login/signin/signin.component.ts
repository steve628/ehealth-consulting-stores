import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserGqlService } from '@app-graphql/services/user-gql.service';
import { AuthorizationService } from '@app-core/services/authorization.service';
import { select, Store } from '@ngrx/store';
import { getToken, State } from '@app-store/index';
import { LoginUserAction } from '@app-store/user/actions/user.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  isLoading = false;
  errorMessage: string;
  private sub: Subscription;


  constructor(
    private router: Router,
    private fb: FormBuilder,
    private userService: UserGqlService,
    private auth: AuthorizationService,
    private store: Store<State>
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  ngOnDestroy(): void {
    if (this.sub && !this.sub.closed) {
      this.sub.unsubscribe();
    }
  }

  private createForm(): void {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.minLength(5),
        Validators.required]]
    });
    // Validators.pattern('\'^(?=.*[A-Z])(?=.*[a-z])(?=.*\\\\d)[A-Za-z\\\\d!$%@#£€*?&]{8,}$\'')
  }

  login(): void {
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;

    this.store.dispatch(new LoginUserAction(email, password));
    this.sub = this.store.pipe(select(getToken)).subscribe(res => {
      this.isLoading = res.loading;
      if (!res.loading && !res.error && res.token) {
        this.auth.setAuthorization(res.token);
        this.router.navigate(['../', 'dashboard']);
      } else {
        this.loginForm.get('password').reset();
        this.errorMessage = 'E-Mail oder Password inkorrekt';
      }
      /* if (res.token && !res.loading) {
        this.auth.setAuthorization(res.token);
        this.router.navigate(['../', 'dashboard']);
      } else {
        this.loginForm.get('password').reset();
      } */
    });
  }

}
