import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ThemeService, ThemeStyle } from '@app-core/services/theme.service';
import { UserGqlService } from '@app-graphql/services/user-gql.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SwUpdate } from '@angular/service-worker';
import { MediaMatcher } from '@angular/cdk/layout';
import { IosInstallComponent } from './ios-install/ios-install.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  currentTheme$: Observable<ThemeStyle>;

  constructor(
    private themeService: ThemeService,
    private userService: UserGqlService,
    private snackBar: MatSnackBar,
    private swUpdate: SwUpdate,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.userService.logoutUser();
  }

  ngOnInit(): void {
    this.currentTheme$ = this.themeService.currentTheme;
    this.setupUpdates();
    this.checkIos();
  }

  private setupUpdates(): void {
    this.swUpdate.available.subscribe(u => {
      this.swUpdate.activateUpdate().then(e => {
        const message = 'Die App wurde aktualisiert';
        const action = 'Ok, Reload!';
        this.snackBar.open(message, action).onAction().subscribe(() => location.reload());
      });
    });
    this.swUpdate.checkForUpdate().catch(err => console.log(err));
  }

  private checkIos(): void {
    // Code from https://itnext.io/part-1-building-a-progressive-web-application-pwa-with-angular-material-and-aws-amplify-5c741c957259
    // Detects if device is on iOS
    const isIos = () => {
      const userAgent = window.navigator.userAgent.toLowerCase();
      return /iphone|ipad|ipod/.test(userAgent);
    };
    // Detects if device is in standalone mode
    const isInStandaloneMode = () => ('standalone' in (window as any).navigator) && ((window as any).navigator.standalone);

    // Checks if should display install popup notification:
    if (isIos() && !isInStandaloneMode()) {
      this.snackBar.openFromComponent(IosInstallComponent, {
        duration: 8000,
        horizontalPosition: 'start',
        panelClass: ['mat-elevation-z3']
      });
    }
  }
}
