import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HelpRoutingModule } from './help-routing.module';
import { FaqComponent } from './faq/faq.component';
import { DataProtectionComponent } from './data-protection/data-protection.component';
import { ImprintComponent } from './imprint/imprint.component';
import { MaterialModule } from '@app-material/material.module';


@NgModule({
  declarations: [FaqComponent, DataProtectionComponent, ImprintComponent],
  imports: [
    CommonModule,
    HelpRoutingModule,
    MaterialModule
  ]
})
export class HelpModule { }
