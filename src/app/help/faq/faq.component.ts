import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FaqService, Question } from '@app-core/services/faq.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  faq$: Observable<Question[]>;

  constructor(private faqService: FaqService) { }

  ngOnInit(): void {
    this.faq$ = this.faqService.Faq;
  }

}
