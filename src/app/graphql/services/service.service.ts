import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  CreateServiceGQL,
  CreateServiceMutation,
  GetAllServicesGQL,
  GetAllServicesQuery,
  GetOneServiceGQL,
  GetOneServiceQuery,
  ServiceInput,
  ServiceOrderByInput,
  ServiceUpdateInput,
  ServiceWhereInput,
  UpdateServiceGQL,
  UpdateServiceMutation
} from '@app-graphql/generated/graphql';
import { map, takeUntil } from 'rxjs/operators';
import { FetchResult } from 'apollo-link';
import { ApolloQueryResult } from 'apollo-client';

export interface ServiceSearch {
  where?: ServiceWhereInput;
  orderBy?: ServiceOrderByInput;
  skip?: number;
  after?: string;
  before?: string;
  first?: number;
  last?: number;
}

@Injectable({
  providedIn: 'root'
})
export class ServiceService implements OnDestroy {
  private destroy$ = new Subject();

  constructor(
    private create: CreateServiceGQL,
    private getOne: GetOneServiceGQL,
    private getAll: GetAllServicesGQL,
    private update: UpdateServiceGQL
  ) { }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  createService(data: ServiceInput): Observable<FetchResult<CreateServiceMutation>> {
    return this.create.mutate({data});
    // return this.create.mutate({ data }).pipe(map(res => res.data.createService as Service));
  }

  getService(id: string): Observable<ApolloQueryResult<GetOneServiceQuery>> {
    return this.getOne.fetch({id});
    // return this.getOne.fetch({ id }).pipe(map(res => res.data.service as Service));
  }

  getAllServices(search?: ServiceSearch): Observable<ApolloQueryResult<GetAllServicesQuery>> {
    return this.getAll.fetch(search, {fetchPolicy: 'network-only'});
    // return this.getAll.fetch(search, { fetchPolicy: 'network-only' }).pipe(map(res => res.data.services as Service[]));
  }

  updateService(data: ServiceUpdateInput, serviceId: string): Observable<FetchResult<UpdateServiceMutation>> {
    return this.update.mutate({data, serviceId});
    // return this.update.mutate({ data, serviceId }).pipe(map(res => res.data.updateService as Service));
  }
}
