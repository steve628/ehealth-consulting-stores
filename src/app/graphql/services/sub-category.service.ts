import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  CreateSubCategoryGQL,
  CreateSubCategoryMutation,
  GetAllSubCategoriesGQL,
  GetSubCategoryGQL,
  SubCategory,
  SubCategoryOrderByInput,
  SubCategoryWhereInput
} from '@app-graphql/generated/graphql';
import { map, takeUntil } from 'rxjs/operators';
import { FetchResult } from 'apollo-link';

export interface SubCategorySearch {
  where?: SubCategoryWhereInput;
  orderBy?: SubCategoryOrderByInput;
  skip?: number;
  after?: string;
  before?: string;
  first?: number;
  last?: number;
}

@Injectable({
  providedIn: 'root'
})
export class SubCategoryService implements OnDestroy {
  private destroy$ = new Subject();

  constructor(
    private create: CreateSubCategoryGQL,
    private getOne: GetSubCategoryGQL,
    private getAll: GetAllSubCategoriesGQL
  ) { }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  createSubCategory(name: string, categoryId: string): Observable<FetchResult<CreateSubCategoryMutation>> {
    return this.create.mutate({name, categoryId});
    // return this.create.mutate({name, categoryId}).pipe(map(res => res.data.createSubCategory as SubCategory));
  }

  getSubCategory(id: string): Observable<SubCategory> {
    return this.getOne.fetch({id}).pipe(map(res => res.data.subCategory as SubCategory));
  }

  getAllSubCategories(search?: SubCategorySearch): Observable<SubCategory[]> {
    return this.getAll.fetch(search).pipe(map(res => res.data.subCategories as SubCategory[]));
  }
}
