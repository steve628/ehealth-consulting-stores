import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  Category,
  CategoryOrderByInput,
  CategoryWhereInput,
  CreateCategoryGQL,
  CreateCategoryMutation,
  GetAllCategoriesGQL,
  GetAllCategoriesQuery,
  GetCategoryGQL,
  GetCategoryQuery
} from '@app-graphql/generated/graphql';
import { map, takeUntil } from 'rxjs/operators';
import { ApolloQueryResult } from 'apollo-client';
import { FetchResult } from 'apollo-link';

export interface CategorySearch {
  where?: CategoryWhereInput;
  orderBy?: CategoryOrderByInput;
  skip?: number;
  after?: string;
  before?: string;
  first?: number;
  last?: number;
}

@Injectable({
  providedIn: 'root'
})
export class CategoryService implements OnDestroy {
  private destroy$ = new Subject();

  constructor(
    private getOne: GetCategoryGQL,
    private getAll: GetAllCategoriesGQL,
    private create: CreateCategoryGQL
  ) { }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  getCategory(id: string): Observable<ApolloQueryResult<GetCategoryQuery>> {
    return this.getOne.fetch({id});
    // return this.getOne.fetch({id}).pipe(map(res => res.data.category as Category));
  }

  getAllCategories(search?: CategorySearch): Observable<ApolloQueryResult<GetAllCategoriesQuery>> {
    return this.getAll.fetch(search);
    // return this.getAll.fetch(search).pipe(map(res => res.data.categories as Category[]));
  }

  createCategory(name: string): Observable<FetchResult<CreateCategoryMutation>> {
    return this.create.mutate({name});
    // return this.create.mutate({name}).pipe(map(res => res.data.createCategory as Category));
  }
}
