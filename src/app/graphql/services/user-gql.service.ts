import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Apollo } from 'apollo-angular';
import {
  AuthPayload,
  GetFavoriteConsultantsGQL,
  GetFavoriteServicesGQL,
  GetFavoriteServicesQuery,
  GetMyServicesGQL,
  GetMyServicesQuery,
  LoginUserGQL,
  LoginUserQuery,
  RegistrateUserGQL,
  RegistrationData,
  UpdateUserSettingsGQL,
  UpdateUserSettingsMutation,
  User,
  UserUpdateInput
} from '../generated/graphql';
import { map, takeUntil } from 'rxjs/operators';
import { AuthorizationService } from '@app-core/services/authorization.service';
import { ServiceSearch } from '@app-graphql/services/service.service';
import { ApolloQueryResult } from 'apollo-client';
import { FetchResult } from 'apollo-link';

@Injectable({
  providedIn: 'root'
})
export class UserGqlService implements OnDestroy {
  private destroy$ = new Subject();

  constructor(
    private apollo: Apollo,
    private authService: AuthorizationService,
    private login: LoginUserGQL,
    private registrate: RegistrateUserGQL,
    private updateSettings: UpdateUserSettingsGQL,
    private favoriteServices: GetFavoriteServicesGQL,
    private favoriteConsultants: GetFavoriteConsultantsGQL,
    private myServices: GetMyServicesGQL
  ) { }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  loginUser(email: string, password: string): Observable<ApolloQueryResult<LoginUserQuery>> {
    return this.login.fetch({email, password});
    // return this.login.fetch({email, password}).pipe(map(res => res.data.login as AuthPayload));
  }

  registrateUser(input: RegistrationData): void {
    this.registrate.mutate({data: input}).pipe(takeUntil(this.destroy$)).subscribe();
  }

  updateUserSettings(data: UserUpdateInput): Observable<FetchResult<UpdateUserSettingsMutation>> {
    return this.updateSettings.mutate({data});
    // return this.updateSettings.mutate({data}).pipe(map(res => res.data.updateUserSettings as User));
  }

  getFavoriteServices(search?: ServiceSearch): Observable<ApolloQueryResult<GetFavoriteServicesQuery>> {
    return this.favoriteServices.fetch(search);
    // return this.favoriteServices.fetch(search).pipe(map(res => res.data.user as User));
  }

  getMyServices(search?: ServiceSearch): Observable<ApolloQueryResult<GetMyServicesQuery>> {
    return this.myServices.fetch(search);
    // return this.myServices.fetch(search).pipe(map(res => res.data.user as User));
  }

  logoutUser(): void {
    this.authService.clearAuthorization();
    this.apollo.getClient().clearStore();
    this.apollo.getClient().cache.reset();
    console.log('Cache cleared');
  }
}
