import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  CreateTopicGQL,
  GetAllTopicsGQL,
  GetTopicGQL,
  Topic,
  TopicInput, TopicOrderByInput, TopicWhereInput
} from '@app-graphql/generated/graphql';
import { map, takeUntil } from 'rxjs/operators';

export interface TopicSearch {
  where?: TopicWhereInput;
  orderBy?: TopicOrderByInput;
  skip?: number;
  after?: string;
  before?: string;
  first?: number;
  last?: number;
}

@Injectable({
  providedIn: 'root'
})
export class TopicService implements OnDestroy {
  private destroy$ = new Subject();

  constructor(
    private getOne: GetTopicGQL,
    private getAll: GetAllTopicsGQL,
    private create: CreateTopicGQL
  ) { }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  getTopic(id: string): Observable<Topic> {
    return this.getOne.fetch({id}).pipe(map(res => res.data.topic as Topic));
  }

  getAllTopics(search?: TopicSearch): Observable<Topic[]> {
    return this.getAll.fetch(search).pipe(map(res => res.data.topics as Topic[]));
  }

  createTopic(data: TopicInput): void {
    this.create.mutate({data}).pipe(takeUntil(this.destroy$)).subscribe();
  }
}
