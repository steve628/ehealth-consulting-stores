import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApolloLink, split } from 'apollo-link';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { HttpLink, HttpLinkHandler } from 'apollo-angular-link-http';
import { getMainDefinition } from 'apollo-utilities';
import { WebSocketLink } from 'apollo-link-ws';
import { Apollo, ApolloModule } from 'apollo-angular';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '@app-env/environment';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ApolloModule
  ]
})
export class GraphqlModule {
  private errorLink: ApolloLink;
  private authMiddleware: ApolloLink;
  private splitProtocols: ApolloLink;
  private wsEndpointLink: WebSocketLink;
  private subscriptionClient: SubscriptionClient;
  private httpEndpointLink: HttpLinkHandler;
  private linkChain: ApolloLink;

  constructor(
    private apollo: Apollo,
    private httpLink: HttpLink
  ) {
    this.authMiddleware = this.httpAuthLink();
    this.httpEndpointLink = httpLink.create({ uri: environment.GRAPHQL_HTTPS_BACKEND });
    this.subscriptionClient = this.subscriptionClientLink();
    // @ts-ignore
    this.subscriptionClient.maxConnectTimeGenerator.setMin(10000);
    this.subscriptionClient.onError(this.refreshSubscription);
    this.wsEndpointLink = new WebSocketLink(this.subscriptionClient);
    this.splitProtocols = this.protocolSplit();
    this.linkChain = ApolloLink.from([
      this.authMiddleware,
      this.splitProtocols
    ]);
    apollo.create({
      link: this.linkChain,
      cache: new InMemoryCache()
    });
  }

  restartSubscriptions = () => {
    this.subscriptionClient.close(true, true);
  }

  // tslint:disable-next-line:typedef
  omitTypename(key, value) {
    // aLog.log('omitTypename ', key, value)
    return key === '__typename' ? undefined : value;
  }

  refreshSubscription = () => {
    this.subscriptionClient.close(false, false);
  }

  getToken = () => {
    const token = localStorage.getItem('token');
    return (token) ? token : undefined;
  }

  protocolSplit = () => split(
    // split based on operation type
    ({ query }) => {
      const definition = getMainDefinition(query);
      return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
    },
    this.wsEndpointLink,
    this.httpEndpointLink,
  )

  /** See Issue here:
   * https://github.com/apollographql/apollo-client/issues/1564#issuecomment-357492659
   * Currently unused
   */
  omitTypenameLink = () => new ApolloLink((operation, forward) => {
    if (operation.variables) {
      operation.variables = JSON.parse(JSON.stringify(operation.variables), this.omitTypename);
    }
    return forward(operation);
  })

  httpAuthLink = () => new ApolloLink((operation, forward) => {
    // add the authorization to the headers
    if (this.getToken()) {
      operation.setContext({
        headers: (this.getToken() ?
          new HttpHeaders().set('Authorization', `Bearer ${this.getToken()}`) :
          undefined)
      });
    }
    // aLog.log('running auth Middleware'); //runs at every request
    return forward(operation);
  })

  subscriptionClientLink = () => new SubscriptionClient(
    environment.GRAPHQL_WSS_BACKEND,

    {
      // get headers asynchronously and set in connectionParams
      connectionParams: async () => {
        if (this.getToken()) {
          return { Authorization: `Bearer ${this.getToken()}` };
        }
      },

      lazy: true,
      // set reconnect to true for reconnecting whenever the connection is closed
      reconnect: true,
      timeout: 3000,
    },
  )

}
