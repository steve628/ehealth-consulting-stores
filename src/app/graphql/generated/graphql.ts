import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: any }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
  Long: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};



export type Query = {
  __typename?: 'Query';
  /** login user */
  login: AuthPayload;
  /** checks if email address is allready taken */
  checkEmailAddress: Scalars['Boolean'];
  category: Category;
  categories: Array<Category>;
  service: Service;
  services: Array<Service>;
  subCategory: SubCategory;
  subCategories: Array<SubCategory>;
  topic: Topic;
  topics: Array<Topic>;
  /** get additional informations about login user */
  user: User;
  categoriesConnection: CategoryConnection;
  chat?: Maybe<Chat>;
  chats: Array<Maybe<Chat>>;
  chatsConnection: ChatConnection;
  message?: Maybe<Message>;
  messages: Array<Maybe<Message>>;
  messagesConnection: MessageConnection;
  review?: Maybe<Review>;
  reviews: Array<Maybe<Review>>;
  reviewsConnection: ReviewConnection;
  servicesConnection: ServiceConnection;
  subCategoriesConnection: SubCategoryConnection;
  topicsConnection: TopicConnection;
  users: Array<Maybe<User>>;
  usersConnection: UserConnection;
  node?: Maybe<Node>;
};


export type QueryLoginArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type QueryCheckEmailAddressArgs = {
  email: Scalars['String'];
};


export type QueryCategoryArgs = {
  id: Scalars['ID'];
};


export type QueryCategoriesArgs = {
  where?: Maybe<CategoryWhereInput>;
  orderBy?: Maybe<CategoryOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryServiceArgs = {
  id: Scalars['ID'];
};


export type QueryServicesArgs = {
  where?: Maybe<ServiceWhereInput>;
  orderBy?: Maybe<ServiceOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QuerySubCategoryArgs = {
  id: Scalars['ID'];
};


export type QuerySubCategoriesArgs = {
  where?: Maybe<SubCategoryWhereInput>;
  orderBy?: Maybe<SubCategoryOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryTopicArgs = {
  id: Scalars['ID'];
};


export type QueryTopicsArgs = {
  where?: Maybe<TopicWhereInput>;
  orderBy?: Maybe<TopicOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryCategoriesConnectionArgs = {
  where?: Maybe<CategoryWhereInput>;
  orderBy?: Maybe<CategoryOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryChatArgs = {
  where: ChatWhereUniqueInput;
};


export type QueryChatsArgs = {
  where?: Maybe<ChatWhereInput>;
  orderBy?: Maybe<ChatOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryChatsConnectionArgs = {
  where?: Maybe<ChatWhereInput>;
  orderBy?: Maybe<ChatOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryMessageArgs = {
  where: MessageWhereUniqueInput;
};


export type QueryMessagesArgs = {
  where?: Maybe<MessageWhereInput>;
  orderBy?: Maybe<MessageOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryMessagesConnectionArgs = {
  where?: Maybe<MessageWhereInput>;
  orderBy?: Maybe<MessageOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type QueryReviewsArgs = {
  where?: Maybe<ReviewWhereInput>;
  orderBy?: Maybe<ReviewOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryReviewsConnectionArgs = {
  where?: Maybe<ReviewWhereInput>;
  orderBy?: Maybe<ReviewOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryServicesConnectionArgs = {
  where?: Maybe<ServiceWhereInput>;
  orderBy?: Maybe<ServiceOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QuerySubCategoriesConnectionArgs = {
  where?: Maybe<SubCategoryWhereInput>;
  orderBy?: Maybe<SubCategoryOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryTopicsConnectionArgs = {
  where?: Maybe<TopicWhereInput>;
  orderBy?: Maybe<TopicOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryUsersArgs = {
  where?: Maybe<UserWhereInput>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryUsersConnectionArgs = {
  where?: Maybe<UserWhereInput>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryNodeArgs = {
  id: Scalars['ID'];
};

export type Mutation = {
  __typename?: 'Mutation';
  /** registrate user */
  registrate: User;
  /** updates the settings of a user */
  updateUserSettings: User;
  /** creates a service from a consultant */
  createService: Service;
  /**  updates a service from a consultant */
  updateService: Service;
  /** creates a category from an consultant */
  createCategory: Category;
  /** creates a subcategory from an consultant */
  createSubCategory: SubCategory;
  /** creates an topic from an consultant */
  createTopic: Topic;
  updateCategory?: Maybe<Category>;
  updateManyCategories: BatchPayload;
  upsertCategory: Category;
  deleteCategory?: Maybe<Category>;
  deleteManyCategories: BatchPayload;
  createChat: Chat;
  updateChat?: Maybe<Chat>;
  upsertChat: Chat;
  deleteChat?: Maybe<Chat>;
  deleteManyChats: BatchPayload;
  createMessage: Message;
  updateMessage?: Maybe<Message>;
  updateManyMessages: BatchPayload;
  upsertMessage: Message;
  deleteMessage?: Maybe<Message>;
  deleteManyMessages: BatchPayload;
  createReview: Review;
  updateReview?: Maybe<Review>;
  updateManyReviews: BatchPayload;
  upsertReview: Review;
  deleteReview?: Maybe<Review>;
  deleteManyReviews: BatchPayload;
  updateManyServices: BatchPayload;
  upsertService: Service;
  deleteService?: Maybe<Service>;
  deleteManyServices: BatchPayload;
  updateSubCategory?: Maybe<SubCategory>;
  updateManySubCategories: BatchPayload;
  upsertSubCategory: SubCategory;
  deleteSubCategory?: Maybe<SubCategory>;
  deleteManySubCategories: BatchPayload;
  updateTopic?: Maybe<Topic>;
  updateManyTopics: BatchPayload;
  upsertTopic: Topic;
  deleteTopic?: Maybe<Topic>;
  deleteManyTopics: BatchPayload;
  createUser: User;
  updateUser?: Maybe<User>;
  updateManyUsers: BatchPayload;
  upsertUser: User;
  deleteUser?: Maybe<User>;
  deleteManyUsers: BatchPayload;
};


export type MutationRegistrateArgs = {
  data: RegistrationData;
};


export type MutationUpdateUserSettingsArgs = {
  data: UserUpdateInput;
};


export type MutationCreateServiceArgs = {
  data: ServiceInput;
};


export type MutationUpdateServiceArgs = {
  data: ServiceUpdateInput;
  serviceId: Scalars['ID'];
};


export type MutationCreateCategoryArgs = {
  name: Scalars['String'];
};


export type MutationCreateSubCategoryArgs = {
  name: Scalars['String'];
  categoryId: Scalars['ID'];
};


export type MutationCreateTopicArgs = {
  data: TopicInput;
};


export type MutationUpdateCategoryArgs = {
  data: CategoryUpdateInput;
  where: CategoryWhereUniqueInput;
};


export type MutationUpdateManyCategoriesArgs = {
  data: CategoryUpdateManyMutationInput;
  where?: Maybe<CategoryWhereInput>;
};


export type MutationUpsertCategoryArgs = {
  where: CategoryWhereUniqueInput;
  create: CategoryCreateInput;
  update: CategoryUpdateInput;
};


export type MutationDeleteCategoryArgs = {
  where: CategoryWhereUniqueInput;
};


export type MutationDeleteManyCategoriesArgs = {
  where?: Maybe<CategoryWhereInput>;
};


export type MutationCreateChatArgs = {
  data: ChatCreateInput;
};


export type MutationUpdateChatArgs = {
  data: ChatUpdateInput;
  where: ChatWhereUniqueInput;
};


export type MutationUpsertChatArgs = {
  where: ChatWhereUniqueInput;
  create: ChatCreateInput;
  update: ChatUpdateInput;
};


export type MutationDeleteChatArgs = {
  where: ChatWhereUniqueInput;
};


export type MutationDeleteManyChatsArgs = {
  where?: Maybe<ChatWhereInput>;
};


export type MutationCreateMessageArgs = {
  data: MessageCreateInput;
};


export type MutationUpdateMessageArgs = {
  data: MessageUpdateInput;
  where: MessageWhereUniqueInput;
};


export type MutationUpdateManyMessagesArgs = {
  data: MessageUpdateManyMutationInput;
  where?: Maybe<MessageWhereInput>;
};


export type MutationUpsertMessageArgs = {
  where: MessageWhereUniqueInput;
  create: MessageCreateInput;
  update: MessageUpdateInput;
};


export type MutationDeleteMessageArgs = {
  where: MessageWhereUniqueInput;
};


export type MutationDeleteManyMessagesArgs = {
  where?: Maybe<MessageWhereInput>;
};


export type MutationCreateReviewArgs = {
  data: ReviewCreateInput;
};


export type MutationUpdateReviewArgs = {
  data: ReviewUpdateInput;
  where: ReviewWhereUniqueInput;
};


export type MutationUpdateManyReviewsArgs = {
  data: ReviewUpdateManyMutationInput;
  where?: Maybe<ReviewWhereInput>;
};


export type MutationUpsertReviewArgs = {
  where: ReviewWhereUniqueInput;
  create: ReviewCreateInput;
  update: ReviewUpdateInput;
};


export type MutationDeleteReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationDeleteManyReviewsArgs = {
  where?: Maybe<ReviewWhereInput>;
};


export type MutationUpdateManyServicesArgs = {
  data: ServiceUpdateManyMutationInput;
  where?: Maybe<ServiceWhereInput>;
};


export type MutationUpsertServiceArgs = {
  where: ServiceWhereUniqueInput;
  create: ServiceCreateInput;
  update: ServiceUpdateInput;
};


export type MutationDeleteServiceArgs = {
  where: ServiceWhereUniqueInput;
};


export type MutationDeleteManyServicesArgs = {
  where?: Maybe<ServiceWhereInput>;
};


export type MutationUpdateSubCategoryArgs = {
  data: SubCategoryUpdateInput;
  where: SubCategoryWhereUniqueInput;
};


export type MutationUpdateManySubCategoriesArgs = {
  data: SubCategoryUpdateManyMutationInput;
  where?: Maybe<SubCategoryWhereInput>;
};


export type MutationUpsertSubCategoryArgs = {
  where: SubCategoryWhereUniqueInput;
  create: SubCategoryCreateInput;
  update: SubCategoryUpdateInput;
};


export type MutationDeleteSubCategoryArgs = {
  where: SubCategoryWhereUniqueInput;
};


export type MutationDeleteManySubCategoriesArgs = {
  where?: Maybe<SubCategoryWhereInput>;
};


export type MutationUpdateTopicArgs = {
  data: TopicUpdateInput;
  where: TopicWhereUniqueInput;
};


export type MutationUpdateManyTopicsArgs = {
  data: TopicUpdateManyMutationInput;
  where?: Maybe<TopicWhereInput>;
};


export type MutationUpsertTopicArgs = {
  where: TopicWhereUniqueInput;
  create: TopicCreateInput;
  update: TopicUpdateInput;
};


export type MutationDeleteTopicArgs = {
  where: TopicWhereUniqueInput;
};


export type MutationDeleteManyTopicsArgs = {
  where?: Maybe<TopicWhereInput>;
};


export type MutationCreateUserArgs = {
  data: UserCreateInput;
};


export type MutationUpdateUserArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};


export type MutationUpdateManyUsersArgs = {
  data: UserUpdateManyMutationInput;
  where?: Maybe<UserWhereInput>;
};


export type MutationUpsertUserArgs = {
  where: UserWhereUniqueInput;
  create: UserCreateInput;
  update: UserUpdateInput;
};


export type MutationDeleteUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationDeleteManyUsersArgs = {
  where?: Maybe<UserWhereInput>;
};

export type AuthPayload = {
  __typename?: 'AuthPayload';
  token: Scalars['String'];
  user: User;
};

export type RegistrationData = {
  forename: Scalars['String'];
  lastname: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
  role?: Maybe<Role>;
};

export type UserSettings = {
  email?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  favoriteServices?: Maybe<ServiceUpdateManyWithoutFavoredUsersInput>;
};

export type ServiceInput = {
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  subCategoryId: Scalars['ID'];
};

export type TopicInput = {
  name: Scalars['String'];
  serviceId: Scalars['ID'];
  userId: Scalars['ID'];
};

export type Category = {
  __typename?: 'Category';
  id: Scalars['ID'];
  name: Scalars['String'];
  subCategories?: Maybe<Array<SubCategory>>;
};


export type CategorySubCategoriesArgs = {
  where?: Maybe<SubCategoryWhereInput>;
  orderBy?: Maybe<SubCategoryOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type CategoryWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_lt?: Maybe<Scalars['String']>;
  name_lte?: Maybe<Scalars['String']>;
  name_gt?: Maybe<Scalars['String']>;
  name_gte?: Maybe<Scalars['String']>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  subCategories_every?: Maybe<SubCategoryWhereInput>;
  subCategories_some?: Maybe<SubCategoryWhereInput>;
  subCategories_none?: Maybe<SubCategoryWhereInput>;
  AND?: Maybe<Array<CategoryWhereInput>>;
  OR?: Maybe<Array<CategoryWhereInput>>;
  NOT?: Maybe<Array<CategoryWhereInput>>;
};

export enum CategoryOrderByInput {
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  NameAsc = 'name_ASC',
  NameDesc = 'name_DESC'
}

export type Service = {
  __typename?: 'Service';
  id: Scalars['ID'];
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  subCategory?: Maybe<SubCategory>;
  topics?: Maybe<Array<Topic>>;
  favoredUsers?: Maybe<Array<User>>;
  owner: User;
};


export type ServiceTopicsArgs = {
  where?: Maybe<TopicWhereInput>;
  orderBy?: Maybe<TopicOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ServiceFavoredUsersArgs = {
  where?: Maybe<UserWhereInput>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type ServiceWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  title?: Maybe<Scalars['String']>;
  title_not?: Maybe<Scalars['String']>;
  title_in?: Maybe<Array<Scalars['String']>>;
  title_not_in?: Maybe<Array<Scalars['String']>>;
  title_lt?: Maybe<Scalars['String']>;
  title_lte?: Maybe<Scalars['String']>;
  title_gt?: Maybe<Scalars['String']>;
  title_gte?: Maybe<Scalars['String']>;
  title_contains?: Maybe<Scalars['String']>;
  title_not_contains?: Maybe<Scalars['String']>;
  title_starts_with?: Maybe<Scalars['String']>;
  title_not_starts_with?: Maybe<Scalars['String']>;
  title_ends_with?: Maybe<Scalars['String']>;
  title_not_ends_with?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  description_not?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Scalars['String']>>;
  description_not_in?: Maybe<Array<Scalars['String']>>;
  description_lt?: Maybe<Scalars['String']>;
  description_lte?: Maybe<Scalars['String']>;
  description_gt?: Maybe<Scalars['String']>;
  description_gte?: Maybe<Scalars['String']>;
  description_contains?: Maybe<Scalars['String']>;
  description_not_contains?: Maybe<Scalars['String']>;
  description_starts_with?: Maybe<Scalars['String']>;
  description_not_starts_with?: Maybe<Scalars['String']>;
  description_ends_with?: Maybe<Scalars['String']>;
  description_not_ends_with?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  picture_not?: Maybe<Scalars['String']>;
  picture_in?: Maybe<Array<Scalars['String']>>;
  picture_not_in?: Maybe<Array<Scalars['String']>>;
  picture_lt?: Maybe<Scalars['String']>;
  picture_lte?: Maybe<Scalars['String']>;
  picture_gt?: Maybe<Scalars['String']>;
  picture_gte?: Maybe<Scalars['String']>;
  picture_contains?: Maybe<Scalars['String']>;
  picture_not_contains?: Maybe<Scalars['String']>;
  picture_starts_with?: Maybe<Scalars['String']>;
  picture_not_starts_with?: Maybe<Scalars['String']>;
  picture_ends_with?: Maybe<Scalars['String']>;
  picture_not_ends_with?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  video_not?: Maybe<Scalars['String']>;
  video_in?: Maybe<Array<Scalars['String']>>;
  video_not_in?: Maybe<Array<Scalars['String']>>;
  video_lt?: Maybe<Scalars['String']>;
  video_lte?: Maybe<Scalars['String']>;
  video_gt?: Maybe<Scalars['String']>;
  video_gte?: Maybe<Scalars['String']>;
  video_contains?: Maybe<Scalars['String']>;
  video_not_contains?: Maybe<Scalars['String']>;
  video_starts_with?: Maybe<Scalars['String']>;
  video_not_starts_with?: Maybe<Scalars['String']>;
  video_ends_with?: Maybe<Scalars['String']>;
  video_not_ends_with?: Maybe<Scalars['String']>;
  subCategory?: Maybe<SubCategoryWhereInput>;
  topics_every?: Maybe<TopicWhereInput>;
  topics_some?: Maybe<TopicWhereInput>;
  topics_none?: Maybe<TopicWhereInput>;
  favoredUsers_every?: Maybe<UserWhereInput>;
  favoredUsers_some?: Maybe<UserWhereInput>;
  favoredUsers_none?: Maybe<UserWhereInput>;
  owner?: Maybe<UserWhereInput>;
  AND?: Maybe<Array<ServiceWhereInput>>;
  OR?: Maybe<Array<ServiceWhereInput>>;
  NOT?: Maybe<Array<ServiceWhereInput>>;
};

export enum ServiceOrderByInput {
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  TitleAsc = 'title_ASC',
  TitleDesc = 'title_DESC',
  DescriptionAsc = 'description_ASC',
  DescriptionDesc = 'description_DESC',
  PictureAsc = 'picture_ASC',
  PictureDesc = 'picture_DESC',
  VideoAsc = 'video_ASC',
  VideoDesc = 'video_DESC'
}

export type SubCategory = {
  __typename?: 'SubCategory';
  id: Scalars['ID'];
  name: Scalars['String'];
  category: Category;
  services?: Maybe<Array<Service>>;
};


export type SubCategoryServicesArgs = {
  where?: Maybe<ServiceWhereInput>;
  orderBy?: Maybe<ServiceOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type SubCategoryWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_lt?: Maybe<Scalars['String']>;
  name_lte?: Maybe<Scalars['String']>;
  name_gt?: Maybe<Scalars['String']>;
  name_gte?: Maybe<Scalars['String']>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  category?: Maybe<CategoryWhereInput>;
  services_every?: Maybe<ServiceWhereInput>;
  services_some?: Maybe<ServiceWhereInput>;
  services_none?: Maybe<ServiceWhereInput>;
  AND?: Maybe<Array<SubCategoryWhereInput>>;
  OR?: Maybe<Array<SubCategoryWhereInput>>;
  NOT?: Maybe<Array<SubCategoryWhereInput>>;
};

export enum SubCategoryOrderByInput {
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  NameAsc = 'name_ASC',
  NameDesc = 'name_DESC'
}

export type Topic = {
  __typename?: 'Topic';
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  services?: Maybe<Array<Service>>;
  users?: Maybe<Array<User>>;
};


export type TopicServicesArgs = {
  where?: Maybe<ServiceWhereInput>;
  orderBy?: Maybe<ServiceOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type TopicUsersArgs = {
  where?: Maybe<UserWhereInput>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type TopicWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_lt?: Maybe<Scalars['String']>;
  name_lte?: Maybe<Scalars['String']>;
  name_gt?: Maybe<Scalars['String']>;
  name_gte?: Maybe<Scalars['String']>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  services_every?: Maybe<ServiceWhereInput>;
  services_some?: Maybe<ServiceWhereInput>;
  services_none?: Maybe<ServiceWhereInput>;
  users_every?: Maybe<UserWhereInput>;
  users_some?: Maybe<UserWhereInput>;
  users_none?: Maybe<UserWhereInput>;
  AND?: Maybe<Array<TopicWhereInput>>;
  OR?: Maybe<Array<TopicWhereInput>>;
  NOT?: Maybe<Array<TopicWhereInput>>;
};

export enum TopicOrderByInput {
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  NameAsc = 'name_ASC',
  NameDesc = 'name_DESC'
}

export type User = {
  __typename?: 'User';
  id: Scalars['ID'];
  email: Scalars['String'];
  password: Scalars['String'];
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<Array<Service>>;
  topics?: Maybe<Array<Topic>>;
  writtenReviews?: Maybe<Array<Review>>;
  ratedReviews?: Maybe<Array<Review>>;
  messages?: Maybe<Array<Message>>;
  favoriteConsultants?: Maybe<Array<User>>;
  favoriteServices?: Maybe<Array<Service>>;
  consultants?: Maybe<Array<User>>;
};


export type UserServicesArgs = {
  where?: Maybe<ServiceWhereInput>;
  orderBy?: Maybe<ServiceOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type UserTopicsArgs = {
  where?: Maybe<TopicWhereInput>;
  orderBy?: Maybe<TopicOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type UserWrittenReviewsArgs = {
  where?: Maybe<ReviewWhereInput>;
  orderBy?: Maybe<ReviewOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type UserRatedReviewsArgs = {
  where?: Maybe<ReviewWhereInput>;
  orderBy?: Maybe<ReviewOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type UserMessagesArgs = {
  where?: Maybe<MessageWhereInput>;
  orderBy?: Maybe<MessageOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type UserFavoriteConsultantsArgs = {
  where?: Maybe<UserWhereInput>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type UserFavoriteServicesArgs = {
  where?: Maybe<ServiceWhereInput>;
  orderBy?: Maybe<ServiceOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type UserConsultantsArgs = {
  where?: Maybe<UserWhereInput>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type CategoryConnection = {
  __typename?: 'CategoryConnection';
  pageInfo: PageInfo;
  edges: Array<Maybe<CategoryEdge>>;
  aggregate: AggregateCategory;
};

export type Chat = {
  __typename?: 'Chat';
  id: Scalars['ID'];
  messages?: Maybe<Array<Message>>;
};


export type ChatMessagesArgs = {
  where?: Maybe<MessageWhereInput>;
  orderBy?: Maybe<MessageOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type ChatWhereUniqueInput = {
  id?: Maybe<Scalars['ID']>;
};

export type ChatWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  messages_every?: Maybe<MessageWhereInput>;
  messages_some?: Maybe<MessageWhereInput>;
  messages_none?: Maybe<MessageWhereInput>;
  AND?: Maybe<Array<ChatWhereInput>>;
  OR?: Maybe<Array<ChatWhereInput>>;
  NOT?: Maybe<Array<ChatWhereInput>>;
};

export enum ChatOrderByInput {
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC'
}

export type ChatConnection = {
  __typename?: 'ChatConnection';
  pageInfo: PageInfo;
  edges: Array<Maybe<ChatEdge>>;
  aggregate: AggregateChat;
};

export type Message = {
  __typename?: 'Message';
  id: Scalars['ID'];
  text: Scalars['String'];
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  chat: Chat;
  author: User;
};

export type MessageWhereUniqueInput = {
  id?: Maybe<Scalars['ID']>;
};

export type MessageWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  text?: Maybe<Scalars['String']>;
  text_not?: Maybe<Scalars['String']>;
  text_in?: Maybe<Array<Scalars['String']>>;
  text_not_in?: Maybe<Array<Scalars['String']>>;
  text_lt?: Maybe<Scalars['String']>;
  text_lte?: Maybe<Scalars['String']>;
  text_gt?: Maybe<Scalars['String']>;
  text_gte?: Maybe<Scalars['String']>;
  text_contains?: Maybe<Scalars['String']>;
  text_not_contains?: Maybe<Scalars['String']>;
  text_starts_with?: Maybe<Scalars['String']>;
  text_not_starts_with?: Maybe<Scalars['String']>;
  text_ends_with?: Maybe<Scalars['String']>;
  text_not_ends_with?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdAt_not?: Maybe<Scalars['DateTime']>;
  createdAt_in?: Maybe<Array<Scalars['DateTime']>>;
  createdAt_not_in?: Maybe<Array<Scalars['DateTime']>>;
  createdAt_lt?: Maybe<Scalars['DateTime']>;
  createdAt_lte?: Maybe<Scalars['DateTime']>;
  createdAt_gt?: Maybe<Scalars['DateTime']>;
  createdAt_gte?: Maybe<Scalars['DateTime']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  updatedAt_not?: Maybe<Scalars['DateTime']>;
  updatedAt_in?: Maybe<Array<Scalars['DateTime']>>;
  updatedAt_not_in?: Maybe<Array<Scalars['DateTime']>>;
  updatedAt_lt?: Maybe<Scalars['DateTime']>;
  updatedAt_lte?: Maybe<Scalars['DateTime']>;
  updatedAt_gt?: Maybe<Scalars['DateTime']>;
  updatedAt_gte?: Maybe<Scalars['DateTime']>;
  chat?: Maybe<ChatWhereInput>;
  author?: Maybe<UserWhereInput>;
  AND?: Maybe<Array<MessageWhereInput>>;
  OR?: Maybe<Array<MessageWhereInput>>;
  NOT?: Maybe<Array<MessageWhereInput>>;
};

export enum MessageOrderByInput {
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  TextAsc = 'text_ASC',
  TextDesc = 'text_DESC',
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC'
}

export type MessageConnection = {
  __typename?: 'MessageConnection';
  pageInfo: PageInfo;
  edges: Array<Maybe<MessageEdge>>;
  aggregate: AggregateMessage;
};

export type Review = {
  __typename?: 'Review';
  id: Scalars['ID'];
  text: Scalars['String'];
  rating: Scalars['Int'];
  author: User;
  ratedConsultant: User;
};

export type ReviewWhereUniqueInput = {
  id?: Maybe<Scalars['ID']>;
};

export type ReviewWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  text?: Maybe<Scalars['String']>;
  text_not?: Maybe<Scalars['String']>;
  text_in?: Maybe<Array<Scalars['String']>>;
  text_not_in?: Maybe<Array<Scalars['String']>>;
  text_lt?: Maybe<Scalars['String']>;
  text_lte?: Maybe<Scalars['String']>;
  text_gt?: Maybe<Scalars['String']>;
  text_gte?: Maybe<Scalars['String']>;
  text_contains?: Maybe<Scalars['String']>;
  text_not_contains?: Maybe<Scalars['String']>;
  text_starts_with?: Maybe<Scalars['String']>;
  text_not_starts_with?: Maybe<Scalars['String']>;
  text_ends_with?: Maybe<Scalars['String']>;
  text_not_ends_with?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['Int']>;
  rating_not?: Maybe<Scalars['Int']>;
  rating_in?: Maybe<Array<Scalars['Int']>>;
  rating_not_in?: Maybe<Array<Scalars['Int']>>;
  rating_lt?: Maybe<Scalars['Int']>;
  rating_lte?: Maybe<Scalars['Int']>;
  rating_gt?: Maybe<Scalars['Int']>;
  rating_gte?: Maybe<Scalars['Int']>;
  author?: Maybe<UserWhereInput>;
  ratedConsultant?: Maybe<UserWhereInput>;
  AND?: Maybe<Array<ReviewWhereInput>>;
  OR?: Maybe<Array<ReviewWhereInput>>;
  NOT?: Maybe<Array<ReviewWhereInput>>;
};

export enum ReviewOrderByInput {
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  TextAsc = 'text_ASC',
  TextDesc = 'text_DESC',
  RatingAsc = 'rating_ASC',
  RatingDesc = 'rating_DESC'
}

export type ReviewConnection = {
  __typename?: 'ReviewConnection';
  pageInfo: PageInfo;
  edges: Array<Maybe<ReviewEdge>>;
  aggregate: AggregateReview;
};

export type ServiceConnection = {
  __typename?: 'ServiceConnection';
  pageInfo: PageInfo;
  edges: Array<Maybe<ServiceEdge>>;
  aggregate: AggregateService;
};

export type SubCategoryConnection = {
  __typename?: 'SubCategoryConnection';
  pageInfo: PageInfo;
  edges: Array<Maybe<SubCategoryEdge>>;
  aggregate: AggregateSubCategory;
};

export type TopicConnection = {
  __typename?: 'TopicConnection';
  pageInfo: PageInfo;
  edges: Array<Maybe<TopicEdge>>;
  aggregate: AggregateTopic;
};

export type UserWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  email?: Maybe<Scalars['String']>;
  email_not?: Maybe<Scalars['String']>;
  email_in?: Maybe<Array<Scalars['String']>>;
  email_not_in?: Maybe<Array<Scalars['String']>>;
  email_lt?: Maybe<Scalars['String']>;
  email_lte?: Maybe<Scalars['String']>;
  email_gt?: Maybe<Scalars['String']>;
  email_gte?: Maybe<Scalars['String']>;
  email_contains?: Maybe<Scalars['String']>;
  email_not_contains?: Maybe<Scalars['String']>;
  email_starts_with?: Maybe<Scalars['String']>;
  email_not_starts_with?: Maybe<Scalars['String']>;
  email_ends_with?: Maybe<Scalars['String']>;
  email_not_ends_with?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  password_not?: Maybe<Scalars['String']>;
  password_in?: Maybe<Array<Scalars['String']>>;
  password_not_in?: Maybe<Array<Scalars['String']>>;
  password_lt?: Maybe<Scalars['String']>;
  password_lte?: Maybe<Scalars['String']>;
  password_gt?: Maybe<Scalars['String']>;
  password_gte?: Maybe<Scalars['String']>;
  password_contains?: Maybe<Scalars['String']>;
  password_not_contains?: Maybe<Scalars['String']>;
  password_starts_with?: Maybe<Scalars['String']>;
  password_not_starts_with?: Maybe<Scalars['String']>;
  password_ends_with?: Maybe<Scalars['String']>;
  password_not_ends_with?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  forename_not?: Maybe<Scalars['String']>;
  forename_in?: Maybe<Array<Scalars['String']>>;
  forename_not_in?: Maybe<Array<Scalars['String']>>;
  forename_lt?: Maybe<Scalars['String']>;
  forename_lte?: Maybe<Scalars['String']>;
  forename_gt?: Maybe<Scalars['String']>;
  forename_gte?: Maybe<Scalars['String']>;
  forename_contains?: Maybe<Scalars['String']>;
  forename_not_contains?: Maybe<Scalars['String']>;
  forename_starts_with?: Maybe<Scalars['String']>;
  forename_not_starts_with?: Maybe<Scalars['String']>;
  forename_ends_with?: Maybe<Scalars['String']>;
  forename_not_ends_with?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  lastname_not?: Maybe<Scalars['String']>;
  lastname_in?: Maybe<Array<Scalars['String']>>;
  lastname_not_in?: Maybe<Array<Scalars['String']>>;
  lastname_lt?: Maybe<Scalars['String']>;
  lastname_lte?: Maybe<Scalars['String']>;
  lastname_gt?: Maybe<Scalars['String']>;
  lastname_gte?: Maybe<Scalars['String']>;
  lastname_contains?: Maybe<Scalars['String']>;
  lastname_not_contains?: Maybe<Scalars['String']>;
  lastname_starts_with?: Maybe<Scalars['String']>;
  lastname_not_starts_with?: Maybe<Scalars['String']>;
  lastname_ends_with?: Maybe<Scalars['String']>;
  lastname_not_ends_with?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  picture_not?: Maybe<Scalars['String']>;
  picture_in?: Maybe<Array<Scalars['String']>>;
  picture_not_in?: Maybe<Array<Scalars['String']>>;
  picture_lt?: Maybe<Scalars['String']>;
  picture_lte?: Maybe<Scalars['String']>;
  picture_gt?: Maybe<Scalars['String']>;
  picture_gte?: Maybe<Scalars['String']>;
  picture_contains?: Maybe<Scalars['String']>;
  picture_not_contains?: Maybe<Scalars['String']>;
  picture_starts_with?: Maybe<Scalars['String']>;
  picture_not_starts_with?: Maybe<Scalars['String']>;
  picture_ends_with?: Maybe<Scalars['String']>;
  picture_not_ends_with?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  price_not?: Maybe<Scalars['Float']>;
  price_in?: Maybe<Array<Scalars['Float']>>;
  price_not_in?: Maybe<Array<Scalars['Float']>>;
  price_lt?: Maybe<Scalars['Float']>;
  price_lte?: Maybe<Scalars['Float']>;
  price_gt?: Maybe<Scalars['Float']>;
  price_gte?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  phoneNumber_not?: Maybe<Scalars['String']>;
  phoneNumber_in?: Maybe<Array<Scalars['String']>>;
  phoneNumber_not_in?: Maybe<Array<Scalars['String']>>;
  phoneNumber_lt?: Maybe<Scalars['String']>;
  phoneNumber_lte?: Maybe<Scalars['String']>;
  phoneNumber_gt?: Maybe<Scalars['String']>;
  phoneNumber_gte?: Maybe<Scalars['String']>;
  phoneNumber_contains?: Maybe<Scalars['String']>;
  phoneNumber_not_contains?: Maybe<Scalars['String']>;
  phoneNumber_starts_with?: Maybe<Scalars['String']>;
  phoneNumber_not_starts_with?: Maybe<Scalars['String']>;
  phoneNumber_ends_with?: Maybe<Scalars['String']>;
  phoneNumber_not_ends_with?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  role_not?: Maybe<Role>;
  role_in?: Maybe<Array<Role>>;
  role_not_in?: Maybe<Array<Role>>;
  description?: Maybe<Scalars['String']>;
  description_not?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Scalars['String']>>;
  description_not_in?: Maybe<Array<Scalars['String']>>;
  description_lt?: Maybe<Scalars['String']>;
  description_lte?: Maybe<Scalars['String']>;
  description_gt?: Maybe<Scalars['String']>;
  description_gte?: Maybe<Scalars['String']>;
  description_contains?: Maybe<Scalars['String']>;
  description_not_contains?: Maybe<Scalars['String']>;
  description_starts_with?: Maybe<Scalars['String']>;
  description_not_starts_with?: Maybe<Scalars['String']>;
  description_ends_with?: Maybe<Scalars['String']>;
  description_not_ends_with?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  verificationCode_not?: Maybe<Scalars['String']>;
  verificationCode_in?: Maybe<Array<Scalars['String']>>;
  verificationCode_not_in?: Maybe<Array<Scalars['String']>>;
  verificationCode_lt?: Maybe<Scalars['String']>;
  verificationCode_lte?: Maybe<Scalars['String']>;
  verificationCode_gt?: Maybe<Scalars['String']>;
  verificationCode_gte?: Maybe<Scalars['String']>;
  verificationCode_contains?: Maybe<Scalars['String']>;
  verificationCode_not_contains?: Maybe<Scalars['String']>;
  verificationCode_starts_with?: Maybe<Scalars['String']>;
  verificationCode_not_starts_with?: Maybe<Scalars['String']>;
  verificationCode_ends_with?: Maybe<Scalars['String']>;
  verificationCode_not_ends_with?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  active_not?: Maybe<Scalars['Boolean']>;
  services_every?: Maybe<ServiceWhereInput>;
  services_some?: Maybe<ServiceWhereInput>;
  services_none?: Maybe<ServiceWhereInput>;
  topics_every?: Maybe<TopicWhereInput>;
  topics_some?: Maybe<TopicWhereInput>;
  topics_none?: Maybe<TopicWhereInput>;
  writtenReviews_every?: Maybe<ReviewWhereInput>;
  writtenReviews_some?: Maybe<ReviewWhereInput>;
  writtenReviews_none?: Maybe<ReviewWhereInput>;
  ratedReviews_every?: Maybe<ReviewWhereInput>;
  ratedReviews_some?: Maybe<ReviewWhereInput>;
  ratedReviews_none?: Maybe<ReviewWhereInput>;
  messages_every?: Maybe<MessageWhereInput>;
  messages_some?: Maybe<MessageWhereInput>;
  messages_none?: Maybe<MessageWhereInput>;
  favoriteConsultants_every?: Maybe<UserWhereInput>;
  favoriteConsultants_some?: Maybe<UserWhereInput>;
  favoriteConsultants_none?: Maybe<UserWhereInput>;
  favoriteServices_every?: Maybe<ServiceWhereInput>;
  favoriteServices_some?: Maybe<ServiceWhereInput>;
  favoriteServices_none?: Maybe<ServiceWhereInput>;
  consultants_every?: Maybe<UserWhereInput>;
  consultants_some?: Maybe<UserWhereInput>;
  consultants_none?: Maybe<UserWhereInput>;
  AND?: Maybe<Array<UserWhereInput>>;
  OR?: Maybe<Array<UserWhereInput>>;
  NOT?: Maybe<Array<UserWhereInput>>;
};

export enum UserOrderByInput {
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  EmailAsc = 'email_ASC',
  EmailDesc = 'email_DESC',
  PasswordAsc = 'password_ASC',
  PasswordDesc = 'password_DESC',
  ForenameAsc = 'forename_ASC',
  ForenameDesc = 'forename_DESC',
  LastnameAsc = 'lastname_ASC',
  LastnameDesc = 'lastname_DESC',
  PictureAsc = 'picture_ASC',
  PictureDesc = 'picture_DESC',
  PriceAsc = 'price_ASC',
  PriceDesc = 'price_DESC',
  PhoneNumberAsc = 'phoneNumber_ASC',
  PhoneNumberDesc = 'phoneNumber_DESC',
  RoleAsc = 'role_ASC',
  RoleDesc = 'role_DESC',
  DescriptionAsc = 'description_ASC',
  DescriptionDesc = 'description_DESC',
  VerificationCodeAsc = 'verificationCode_ASC',
  VerificationCodeDesc = 'verificationCode_DESC',
  ActiveAsc = 'active_ASC',
  ActiveDesc = 'active_DESC'
}

export type UserConnection = {
  __typename?: 'UserConnection';
  pageInfo: PageInfo;
  edges: Array<Maybe<UserEdge>>;
  aggregate: AggregateUser;
};

export type UserUpdateInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceUpdateManyWithoutOwnerInput>;
  topics?: Maybe<TopicUpdateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewUpdateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewUpdateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageUpdateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserUpdateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceUpdateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserUpdateManyWithoutConsultantsInput>;
};

export type ServiceUpdateInput = {
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  subCategory?: Maybe<SubCategoryUpdateOneWithoutServicesInput>;
  topics?: Maybe<TopicUpdateManyWithoutServicesInput>;
  favoredUsers?: Maybe<UserUpdateManyWithoutFavoriteServicesInput>;
  owner?: Maybe<UserUpdateOneRequiredWithoutServicesInput>;
};

export type CategoryUpdateInput = {
  name?: Maybe<Scalars['String']>;
  subCategories?: Maybe<SubCategoryUpdateManyWithoutCategoryInput>;
};

export type CategoryWhereUniqueInput = {
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
};

export type BatchPayload = {
  __typename?: 'BatchPayload';
  count: Scalars['Long'];
};

export type CategoryUpdateManyMutationInput = {
  name?: Maybe<Scalars['String']>;
};

export type CategoryCreateInput = {
  id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  subCategories?: Maybe<SubCategoryCreateManyWithoutCategoryInput>;
};

export type ChatCreateInput = {
  id?: Maybe<Scalars['ID']>;
  messages?: Maybe<MessageCreateManyWithoutChatInput>;
};

export type ChatUpdateInput = {
  messages?: Maybe<MessageUpdateManyWithoutChatInput>;
};

export type MessageCreateInput = {
  id?: Maybe<Scalars['ID']>;
  text: Scalars['String'];
  chat: ChatCreateOneWithoutMessagesInput;
  author: UserCreateOneWithoutMessagesInput;
};

export type MessageUpdateInput = {
  text?: Maybe<Scalars['String']>;
  chat?: Maybe<ChatUpdateOneRequiredWithoutMessagesInput>;
  author?: Maybe<UserUpdateOneRequiredWithoutMessagesInput>;
};

export type MessageUpdateManyMutationInput = {
  text?: Maybe<Scalars['String']>;
};

export type ReviewCreateInput = {
  id?: Maybe<Scalars['ID']>;
  text: Scalars['String'];
  rating: Scalars['Int'];
  author: UserCreateOneWithoutWrittenReviewsInput;
  ratedConsultant: UserCreateOneWithoutRatedReviewsInput;
};

export type ReviewUpdateInput = {
  text?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['Int']>;
  author?: Maybe<UserUpdateOneRequiredWithoutWrittenReviewsInput>;
  ratedConsultant?: Maybe<UserUpdateOneRequiredWithoutRatedReviewsInput>;
};

export type ReviewUpdateManyMutationInput = {
  text?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['Int']>;
};

export type ServiceUpdateManyMutationInput = {
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
};

export type ServiceWhereUniqueInput = {
  id?: Maybe<Scalars['ID']>;
};

export type ServiceCreateInput = {
  id?: Maybe<Scalars['ID']>;
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  subCategory?: Maybe<SubCategoryCreateOneWithoutServicesInput>;
  topics?: Maybe<TopicCreateManyWithoutServicesInput>;
  favoredUsers?: Maybe<UserCreateManyWithoutFavoriteServicesInput>;
  owner: UserCreateOneWithoutServicesInput;
};

export type SubCategoryUpdateInput = {
  name?: Maybe<Scalars['String']>;
  category?: Maybe<CategoryUpdateOneRequiredWithoutSubCategoriesInput>;
  services?: Maybe<ServiceUpdateManyWithoutSubCategoryInput>;
};

export type SubCategoryWhereUniqueInput = {
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
};

export type SubCategoryUpdateManyMutationInput = {
  name?: Maybe<Scalars['String']>;
};

export type SubCategoryCreateInput = {
  id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  category: CategoryCreateOneWithoutSubCategoriesInput;
  services?: Maybe<ServiceCreateManyWithoutSubCategoryInput>;
};

export type TopicUpdateInput = {
  name?: Maybe<Scalars['String']>;
  services?: Maybe<ServiceUpdateManyWithoutTopicsInput>;
  users?: Maybe<UserUpdateManyWithoutTopicsInput>;
};

export type TopicWhereUniqueInput = {
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
};

export type TopicUpdateManyMutationInput = {
  name?: Maybe<Scalars['String']>;
};

export type TopicCreateInput = {
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  services?: Maybe<ServiceCreateManyWithoutTopicsInput>;
  users?: Maybe<UserCreateManyWithoutTopicsInput>;
};

export type UserCreateInput = {
  id?: Maybe<Scalars['ID']>;
  email: Scalars['String'];
  password: Scalars['String'];
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceCreateManyWithoutOwnerInput>;
  topics?: Maybe<TopicCreateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewCreateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewCreateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageCreateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserCreateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceCreateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserCreateManyWithoutConsultantsInput>;
};

export type UserWhereUniqueInput = {
  id?: Maybe<Scalars['ID']>;
  email?: Maybe<Scalars['String']>;
};

export type UserUpdateManyMutationInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
};

export enum Role {
  Admin = 'ADMIN',
  Consultant = 'CONSULTANT',
  User = 'USER'
}

export type ServiceUpdateManyWithoutFavoredUsersInput = {
  create?: Maybe<Array<ServiceCreateWithoutFavoredUsersInput>>;
  delete?: Maybe<Array<ServiceWhereUniqueInput>>;
  connect?: Maybe<Array<ServiceWhereUniqueInput>>;
  set?: Maybe<Array<ServiceWhereUniqueInput>>;
  disconnect?: Maybe<Array<ServiceWhereUniqueInput>>;
  update?: Maybe<Array<ServiceUpdateWithWhereUniqueWithoutFavoredUsersInput>>;
  upsert?: Maybe<Array<ServiceUpsertWithWhereUniqueWithoutFavoredUsersInput>>;
  deleteMany?: Maybe<Array<ServiceScalarWhereInput>>;
  updateMany?: Maybe<Array<ServiceUpdateManyWithWhereNestedInput>>;
};

export type PageInfo = {
  __typename?: 'PageInfo';
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
  endCursor?: Maybe<Scalars['String']>;
};

export type CategoryEdge = {
  __typename?: 'CategoryEdge';
  node: Category;
  cursor: Scalars['String'];
};

export type AggregateCategory = {
  __typename?: 'AggregateCategory';
  count: Scalars['Int'];
};

export type ChatEdge = {
  __typename?: 'ChatEdge';
  node: Chat;
  cursor: Scalars['String'];
};

export type AggregateChat = {
  __typename?: 'AggregateChat';
  count: Scalars['Int'];
};


export type MessageEdge = {
  __typename?: 'MessageEdge';
  node: Message;
  cursor: Scalars['String'];
};

export type AggregateMessage = {
  __typename?: 'AggregateMessage';
  count: Scalars['Int'];
};

export type ReviewEdge = {
  __typename?: 'ReviewEdge';
  node: Review;
  cursor: Scalars['String'];
};

export type AggregateReview = {
  __typename?: 'AggregateReview';
  count: Scalars['Int'];
};

export type ServiceEdge = {
  __typename?: 'ServiceEdge';
  node: Service;
  cursor: Scalars['String'];
};

export type AggregateService = {
  __typename?: 'AggregateService';
  count: Scalars['Int'];
};

export type SubCategoryEdge = {
  __typename?: 'SubCategoryEdge';
  node: SubCategory;
  cursor: Scalars['String'];
};

export type AggregateSubCategory = {
  __typename?: 'AggregateSubCategory';
  count: Scalars['Int'];
};

export type TopicEdge = {
  __typename?: 'TopicEdge';
  node: Topic;
  cursor: Scalars['String'];
};

export type AggregateTopic = {
  __typename?: 'AggregateTopic';
  count: Scalars['Int'];
};

export type UserEdge = {
  __typename?: 'UserEdge';
  node: User;
  cursor: Scalars['String'];
};

export type AggregateUser = {
  __typename?: 'AggregateUser';
  count: Scalars['Int'];
};

export type ServiceUpdateManyWithoutOwnerInput = {
  create?: Maybe<Array<ServiceCreateWithoutOwnerInput>>;
  delete?: Maybe<Array<ServiceWhereUniqueInput>>;
  connect?: Maybe<Array<ServiceWhereUniqueInput>>;
  set?: Maybe<Array<ServiceWhereUniqueInput>>;
  disconnect?: Maybe<Array<ServiceWhereUniqueInput>>;
  update?: Maybe<Array<ServiceUpdateWithWhereUniqueWithoutOwnerInput>>;
  upsert?: Maybe<Array<ServiceUpsertWithWhereUniqueWithoutOwnerInput>>;
  deleteMany?: Maybe<Array<ServiceScalarWhereInput>>;
  updateMany?: Maybe<Array<ServiceUpdateManyWithWhereNestedInput>>;
};

export type TopicUpdateManyWithoutUsersInput = {
  create?: Maybe<Array<TopicCreateWithoutUsersInput>>;
  delete?: Maybe<Array<TopicWhereUniqueInput>>;
  connect?: Maybe<Array<TopicWhereUniqueInput>>;
  set?: Maybe<Array<TopicWhereUniqueInput>>;
  disconnect?: Maybe<Array<TopicWhereUniqueInput>>;
  update?: Maybe<Array<TopicUpdateWithWhereUniqueWithoutUsersInput>>;
  upsert?: Maybe<Array<TopicUpsertWithWhereUniqueWithoutUsersInput>>;
  deleteMany?: Maybe<Array<TopicScalarWhereInput>>;
  updateMany?: Maybe<Array<TopicUpdateManyWithWhereNestedInput>>;
};

export type ReviewUpdateManyWithoutAuthorInput = {
  create?: Maybe<Array<ReviewCreateWithoutAuthorInput>>;
  delete?: Maybe<Array<ReviewWhereUniqueInput>>;
  connect?: Maybe<Array<ReviewWhereUniqueInput>>;
  set?: Maybe<Array<ReviewWhereUniqueInput>>;
  disconnect?: Maybe<Array<ReviewWhereUniqueInput>>;
  update?: Maybe<Array<ReviewUpdateWithWhereUniqueWithoutAuthorInput>>;
  upsert?: Maybe<Array<ReviewUpsertWithWhereUniqueWithoutAuthorInput>>;
  deleteMany?: Maybe<Array<ReviewScalarWhereInput>>;
  updateMany?: Maybe<Array<ReviewUpdateManyWithWhereNestedInput>>;
};

export type ReviewUpdateManyWithoutRatedConsultantInput = {
  create?: Maybe<Array<ReviewCreateWithoutRatedConsultantInput>>;
  delete?: Maybe<Array<ReviewWhereUniqueInput>>;
  connect?: Maybe<Array<ReviewWhereUniqueInput>>;
  set?: Maybe<Array<ReviewWhereUniqueInput>>;
  disconnect?: Maybe<Array<ReviewWhereUniqueInput>>;
  update?: Maybe<Array<ReviewUpdateWithWhereUniqueWithoutRatedConsultantInput>>;
  upsert?: Maybe<Array<ReviewUpsertWithWhereUniqueWithoutRatedConsultantInput>>;
  deleteMany?: Maybe<Array<ReviewScalarWhereInput>>;
  updateMany?: Maybe<Array<ReviewUpdateManyWithWhereNestedInput>>;
};

export type MessageUpdateManyWithoutAuthorInput = {
  create?: Maybe<Array<MessageCreateWithoutAuthorInput>>;
  delete?: Maybe<Array<MessageWhereUniqueInput>>;
  connect?: Maybe<Array<MessageWhereUniqueInput>>;
  set?: Maybe<Array<MessageWhereUniqueInput>>;
  disconnect?: Maybe<Array<MessageWhereUniqueInput>>;
  update?: Maybe<Array<MessageUpdateWithWhereUniqueWithoutAuthorInput>>;
  upsert?: Maybe<Array<MessageUpsertWithWhereUniqueWithoutAuthorInput>>;
  deleteMany?: Maybe<Array<MessageScalarWhereInput>>;
  updateMany?: Maybe<Array<MessageUpdateManyWithWhereNestedInput>>;
};

export type UserUpdateManyWithoutFavoriteConsultantsInput = {
  create?: Maybe<Array<UserCreateWithoutFavoriteConsultantsInput>>;
  delete?: Maybe<Array<UserWhereUniqueInput>>;
  connect?: Maybe<Array<UserWhereUniqueInput>>;
  set?: Maybe<Array<UserWhereUniqueInput>>;
  disconnect?: Maybe<Array<UserWhereUniqueInput>>;
  update?: Maybe<Array<UserUpdateWithWhereUniqueWithoutFavoriteConsultantsInput>>;
  upsert?: Maybe<Array<UserUpsertWithWhereUniqueWithoutFavoriteConsultantsInput>>;
  deleteMany?: Maybe<Array<UserScalarWhereInput>>;
  updateMany?: Maybe<Array<UserUpdateManyWithWhereNestedInput>>;
};

export type UserUpdateManyWithoutConsultantsInput = {
  create?: Maybe<Array<UserCreateWithoutConsultantsInput>>;
  delete?: Maybe<Array<UserWhereUniqueInput>>;
  connect?: Maybe<Array<UserWhereUniqueInput>>;
  set?: Maybe<Array<UserWhereUniqueInput>>;
  disconnect?: Maybe<Array<UserWhereUniqueInput>>;
  update?: Maybe<Array<UserUpdateWithWhereUniqueWithoutConsultantsInput>>;
  upsert?: Maybe<Array<UserUpsertWithWhereUniqueWithoutConsultantsInput>>;
  deleteMany?: Maybe<Array<UserScalarWhereInput>>;
  updateMany?: Maybe<Array<UserUpdateManyWithWhereNestedInput>>;
};

export type SubCategoryUpdateOneWithoutServicesInput = {
  create?: Maybe<SubCategoryCreateWithoutServicesInput>;
  update?: Maybe<SubCategoryUpdateWithoutServicesDataInput>;
  upsert?: Maybe<SubCategoryUpsertWithoutServicesInput>;
  delete?: Maybe<Scalars['Boolean']>;
  disconnect?: Maybe<Scalars['Boolean']>;
  connect?: Maybe<SubCategoryWhereUniqueInput>;
};

export type TopicUpdateManyWithoutServicesInput = {
  create?: Maybe<Array<TopicCreateWithoutServicesInput>>;
  delete?: Maybe<Array<TopicWhereUniqueInput>>;
  connect?: Maybe<Array<TopicWhereUniqueInput>>;
  set?: Maybe<Array<TopicWhereUniqueInput>>;
  disconnect?: Maybe<Array<TopicWhereUniqueInput>>;
  update?: Maybe<Array<TopicUpdateWithWhereUniqueWithoutServicesInput>>;
  upsert?: Maybe<Array<TopicUpsertWithWhereUniqueWithoutServicesInput>>;
  deleteMany?: Maybe<Array<TopicScalarWhereInput>>;
  updateMany?: Maybe<Array<TopicUpdateManyWithWhereNestedInput>>;
};

export type UserUpdateManyWithoutFavoriteServicesInput = {
  create?: Maybe<Array<UserCreateWithoutFavoriteServicesInput>>;
  delete?: Maybe<Array<UserWhereUniqueInput>>;
  connect?: Maybe<Array<UserWhereUniqueInput>>;
  set?: Maybe<Array<UserWhereUniqueInput>>;
  disconnect?: Maybe<Array<UserWhereUniqueInput>>;
  update?: Maybe<Array<UserUpdateWithWhereUniqueWithoutFavoriteServicesInput>>;
  upsert?: Maybe<Array<UserUpsertWithWhereUniqueWithoutFavoriteServicesInput>>;
  deleteMany?: Maybe<Array<UserScalarWhereInput>>;
  updateMany?: Maybe<Array<UserUpdateManyWithWhereNestedInput>>;
};

export type UserUpdateOneRequiredWithoutServicesInput = {
  create?: Maybe<UserCreateWithoutServicesInput>;
  update?: Maybe<UserUpdateWithoutServicesDataInput>;
  upsert?: Maybe<UserUpsertWithoutServicesInput>;
  connect?: Maybe<UserWhereUniqueInput>;
};

export type SubCategoryUpdateManyWithoutCategoryInput = {
  create?: Maybe<Array<SubCategoryCreateWithoutCategoryInput>>;
  delete?: Maybe<Array<SubCategoryWhereUniqueInput>>;
  connect?: Maybe<Array<SubCategoryWhereUniqueInput>>;
  set?: Maybe<Array<SubCategoryWhereUniqueInput>>;
  disconnect?: Maybe<Array<SubCategoryWhereUniqueInput>>;
  update?: Maybe<Array<SubCategoryUpdateWithWhereUniqueWithoutCategoryInput>>;
  upsert?: Maybe<Array<SubCategoryUpsertWithWhereUniqueWithoutCategoryInput>>;
  deleteMany?: Maybe<Array<SubCategoryScalarWhereInput>>;
  updateMany?: Maybe<Array<SubCategoryUpdateManyWithWhereNestedInput>>;
};


export type SubCategoryCreateManyWithoutCategoryInput = {
  create?: Maybe<Array<SubCategoryCreateWithoutCategoryInput>>;
  connect?: Maybe<Array<SubCategoryWhereUniqueInput>>;
};

export type MessageCreateManyWithoutChatInput = {
  create?: Maybe<Array<MessageCreateWithoutChatInput>>;
  connect?: Maybe<Array<MessageWhereUniqueInput>>;
};

export type MessageUpdateManyWithoutChatInput = {
  create?: Maybe<Array<MessageCreateWithoutChatInput>>;
  delete?: Maybe<Array<MessageWhereUniqueInput>>;
  connect?: Maybe<Array<MessageWhereUniqueInput>>;
  set?: Maybe<Array<MessageWhereUniqueInput>>;
  disconnect?: Maybe<Array<MessageWhereUniqueInput>>;
  update?: Maybe<Array<MessageUpdateWithWhereUniqueWithoutChatInput>>;
  upsert?: Maybe<Array<MessageUpsertWithWhereUniqueWithoutChatInput>>;
  deleteMany?: Maybe<Array<MessageScalarWhereInput>>;
  updateMany?: Maybe<Array<MessageUpdateManyWithWhereNestedInput>>;
};

export type ChatCreateOneWithoutMessagesInput = {
  create?: Maybe<ChatCreateWithoutMessagesInput>;
  connect?: Maybe<ChatWhereUniqueInput>;
};

export type UserCreateOneWithoutMessagesInput = {
  create?: Maybe<UserCreateWithoutMessagesInput>;
  connect?: Maybe<UserWhereUniqueInput>;
};

export type ChatUpdateOneRequiredWithoutMessagesInput = {
  create?: Maybe<ChatCreateWithoutMessagesInput>;
  connect?: Maybe<ChatWhereUniqueInput>;
};

export type UserUpdateOneRequiredWithoutMessagesInput = {
  create?: Maybe<UserCreateWithoutMessagesInput>;
  update?: Maybe<UserUpdateWithoutMessagesDataInput>;
  upsert?: Maybe<UserUpsertWithoutMessagesInput>;
  connect?: Maybe<UserWhereUniqueInput>;
};

export type UserCreateOneWithoutWrittenReviewsInput = {
  create?: Maybe<UserCreateWithoutWrittenReviewsInput>;
  connect?: Maybe<UserWhereUniqueInput>;
};

export type UserCreateOneWithoutRatedReviewsInput = {
  create?: Maybe<UserCreateWithoutRatedReviewsInput>;
  connect?: Maybe<UserWhereUniqueInput>;
};

export type UserUpdateOneRequiredWithoutWrittenReviewsInput = {
  create?: Maybe<UserCreateWithoutWrittenReviewsInput>;
  update?: Maybe<UserUpdateWithoutWrittenReviewsDataInput>;
  upsert?: Maybe<UserUpsertWithoutWrittenReviewsInput>;
  connect?: Maybe<UserWhereUniqueInput>;
};

export type UserUpdateOneRequiredWithoutRatedReviewsInput = {
  create?: Maybe<UserCreateWithoutRatedReviewsInput>;
  update?: Maybe<UserUpdateWithoutRatedReviewsDataInput>;
  upsert?: Maybe<UserUpsertWithoutRatedReviewsInput>;
  connect?: Maybe<UserWhereUniqueInput>;
};

export type SubCategoryCreateOneWithoutServicesInput = {
  create?: Maybe<SubCategoryCreateWithoutServicesInput>;
  connect?: Maybe<SubCategoryWhereUniqueInput>;
};

export type TopicCreateManyWithoutServicesInput = {
  create?: Maybe<Array<TopicCreateWithoutServicesInput>>;
  connect?: Maybe<Array<TopicWhereUniqueInput>>;
};

export type UserCreateManyWithoutFavoriteServicesInput = {
  create?: Maybe<Array<UserCreateWithoutFavoriteServicesInput>>;
  connect?: Maybe<Array<UserWhereUniqueInput>>;
};

export type UserCreateOneWithoutServicesInput = {
  create?: Maybe<UserCreateWithoutServicesInput>;
  connect?: Maybe<UserWhereUniqueInput>;
};

export type CategoryUpdateOneRequiredWithoutSubCategoriesInput = {
  create?: Maybe<CategoryCreateWithoutSubCategoriesInput>;
  update?: Maybe<CategoryUpdateWithoutSubCategoriesDataInput>;
  upsert?: Maybe<CategoryUpsertWithoutSubCategoriesInput>;
  connect?: Maybe<CategoryWhereUniqueInput>;
};

export type ServiceUpdateManyWithoutSubCategoryInput = {
  create?: Maybe<Array<ServiceCreateWithoutSubCategoryInput>>;
  delete?: Maybe<Array<ServiceWhereUniqueInput>>;
  connect?: Maybe<Array<ServiceWhereUniqueInput>>;
  set?: Maybe<Array<ServiceWhereUniqueInput>>;
  disconnect?: Maybe<Array<ServiceWhereUniqueInput>>;
  update?: Maybe<Array<ServiceUpdateWithWhereUniqueWithoutSubCategoryInput>>;
  upsert?: Maybe<Array<ServiceUpsertWithWhereUniqueWithoutSubCategoryInput>>;
  deleteMany?: Maybe<Array<ServiceScalarWhereInput>>;
  updateMany?: Maybe<Array<ServiceUpdateManyWithWhereNestedInput>>;
};

export type CategoryCreateOneWithoutSubCategoriesInput = {
  create?: Maybe<CategoryCreateWithoutSubCategoriesInput>;
  connect?: Maybe<CategoryWhereUniqueInput>;
};

export type ServiceCreateManyWithoutSubCategoryInput = {
  create?: Maybe<Array<ServiceCreateWithoutSubCategoryInput>>;
  connect?: Maybe<Array<ServiceWhereUniqueInput>>;
};

export type ServiceUpdateManyWithoutTopicsInput = {
  create?: Maybe<Array<ServiceCreateWithoutTopicsInput>>;
  delete?: Maybe<Array<ServiceWhereUniqueInput>>;
  connect?: Maybe<Array<ServiceWhereUniqueInput>>;
  set?: Maybe<Array<ServiceWhereUniqueInput>>;
  disconnect?: Maybe<Array<ServiceWhereUniqueInput>>;
  update?: Maybe<Array<ServiceUpdateWithWhereUniqueWithoutTopicsInput>>;
  upsert?: Maybe<Array<ServiceUpsertWithWhereUniqueWithoutTopicsInput>>;
  deleteMany?: Maybe<Array<ServiceScalarWhereInput>>;
  updateMany?: Maybe<Array<ServiceUpdateManyWithWhereNestedInput>>;
};

export type UserUpdateManyWithoutTopicsInput = {
  create?: Maybe<Array<UserCreateWithoutTopicsInput>>;
  delete?: Maybe<Array<UserWhereUniqueInput>>;
  connect?: Maybe<Array<UserWhereUniqueInput>>;
  set?: Maybe<Array<UserWhereUniqueInput>>;
  disconnect?: Maybe<Array<UserWhereUniqueInput>>;
  update?: Maybe<Array<UserUpdateWithWhereUniqueWithoutTopicsInput>>;
  upsert?: Maybe<Array<UserUpsertWithWhereUniqueWithoutTopicsInput>>;
  deleteMany?: Maybe<Array<UserScalarWhereInput>>;
  updateMany?: Maybe<Array<UserUpdateManyWithWhereNestedInput>>;
};

export type ServiceCreateManyWithoutTopicsInput = {
  create?: Maybe<Array<ServiceCreateWithoutTopicsInput>>;
  connect?: Maybe<Array<ServiceWhereUniqueInput>>;
};

export type UserCreateManyWithoutTopicsInput = {
  create?: Maybe<Array<UserCreateWithoutTopicsInput>>;
  connect?: Maybe<Array<UserWhereUniqueInput>>;
};

export type ServiceCreateManyWithoutOwnerInput = {
  create?: Maybe<Array<ServiceCreateWithoutOwnerInput>>;
  connect?: Maybe<Array<ServiceWhereUniqueInput>>;
};

export type TopicCreateManyWithoutUsersInput = {
  create?: Maybe<Array<TopicCreateWithoutUsersInput>>;
  connect?: Maybe<Array<TopicWhereUniqueInput>>;
};

export type ReviewCreateManyWithoutAuthorInput = {
  create?: Maybe<Array<ReviewCreateWithoutAuthorInput>>;
  connect?: Maybe<Array<ReviewWhereUniqueInput>>;
};

export type ReviewCreateManyWithoutRatedConsultantInput = {
  create?: Maybe<Array<ReviewCreateWithoutRatedConsultantInput>>;
  connect?: Maybe<Array<ReviewWhereUniqueInput>>;
};

export type MessageCreateManyWithoutAuthorInput = {
  create?: Maybe<Array<MessageCreateWithoutAuthorInput>>;
  connect?: Maybe<Array<MessageWhereUniqueInput>>;
};

export type UserCreateManyWithoutFavoriteConsultantsInput = {
  create?: Maybe<Array<UserCreateWithoutFavoriteConsultantsInput>>;
  connect?: Maybe<Array<UserWhereUniqueInput>>;
};

export type ServiceCreateManyWithoutFavoredUsersInput = {
  create?: Maybe<Array<ServiceCreateWithoutFavoredUsersInput>>;
  connect?: Maybe<Array<ServiceWhereUniqueInput>>;
};

export type UserCreateManyWithoutConsultantsInput = {
  create?: Maybe<Array<UserCreateWithoutConsultantsInput>>;
  connect?: Maybe<Array<UserWhereUniqueInput>>;
};

export type ServiceCreateWithoutFavoredUsersInput = {
  id?: Maybe<Scalars['ID']>;
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  subCategory?: Maybe<SubCategoryCreateOneWithoutServicesInput>;
  topics?: Maybe<TopicCreateManyWithoutServicesInput>;
  owner: UserCreateOneWithoutServicesInput;
};

export type ServiceUpdateWithWhereUniqueWithoutFavoredUsersInput = {
  where: ServiceWhereUniqueInput;
  data: ServiceUpdateWithoutFavoredUsersDataInput;
};

export type ServiceUpsertWithWhereUniqueWithoutFavoredUsersInput = {
  where: ServiceWhereUniqueInput;
  update: ServiceUpdateWithoutFavoredUsersDataInput;
  create: ServiceCreateWithoutFavoredUsersInput;
};

export type ServiceScalarWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  title?: Maybe<Scalars['String']>;
  title_not?: Maybe<Scalars['String']>;
  title_in?: Maybe<Array<Scalars['String']>>;
  title_not_in?: Maybe<Array<Scalars['String']>>;
  title_lt?: Maybe<Scalars['String']>;
  title_lte?: Maybe<Scalars['String']>;
  title_gt?: Maybe<Scalars['String']>;
  title_gte?: Maybe<Scalars['String']>;
  title_contains?: Maybe<Scalars['String']>;
  title_not_contains?: Maybe<Scalars['String']>;
  title_starts_with?: Maybe<Scalars['String']>;
  title_not_starts_with?: Maybe<Scalars['String']>;
  title_ends_with?: Maybe<Scalars['String']>;
  title_not_ends_with?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  description_not?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Scalars['String']>>;
  description_not_in?: Maybe<Array<Scalars['String']>>;
  description_lt?: Maybe<Scalars['String']>;
  description_lte?: Maybe<Scalars['String']>;
  description_gt?: Maybe<Scalars['String']>;
  description_gte?: Maybe<Scalars['String']>;
  description_contains?: Maybe<Scalars['String']>;
  description_not_contains?: Maybe<Scalars['String']>;
  description_starts_with?: Maybe<Scalars['String']>;
  description_not_starts_with?: Maybe<Scalars['String']>;
  description_ends_with?: Maybe<Scalars['String']>;
  description_not_ends_with?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  picture_not?: Maybe<Scalars['String']>;
  picture_in?: Maybe<Array<Scalars['String']>>;
  picture_not_in?: Maybe<Array<Scalars['String']>>;
  picture_lt?: Maybe<Scalars['String']>;
  picture_lte?: Maybe<Scalars['String']>;
  picture_gt?: Maybe<Scalars['String']>;
  picture_gte?: Maybe<Scalars['String']>;
  picture_contains?: Maybe<Scalars['String']>;
  picture_not_contains?: Maybe<Scalars['String']>;
  picture_starts_with?: Maybe<Scalars['String']>;
  picture_not_starts_with?: Maybe<Scalars['String']>;
  picture_ends_with?: Maybe<Scalars['String']>;
  picture_not_ends_with?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  video_not?: Maybe<Scalars['String']>;
  video_in?: Maybe<Array<Scalars['String']>>;
  video_not_in?: Maybe<Array<Scalars['String']>>;
  video_lt?: Maybe<Scalars['String']>;
  video_lte?: Maybe<Scalars['String']>;
  video_gt?: Maybe<Scalars['String']>;
  video_gte?: Maybe<Scalars['String']>;
  video_contains?: Maybe<Scalars['String']>;
  video_not_contains?: Maybe<Scalars['String']>;
  video_starts_with?: Maybe<Scalars['String']>;
  video_not_starts_with?: Maybe<Scalars['String']>;
  video_ends_with?: Maybe<Scalars['String']>;
  video_not_ends_with?: Maybe<Scalars['String']>;
  AND?: Maybe<Array<ServiceScalarWhereInput>>;
  OR?: Maybe<Array<ServiceScalarWhereInput>>;
  NOT?: Maybe<Array<ServiceScalarWhereInput>>;
};

export type ServiceUpdateManyWithWhereNestedInput = {
  where: ServiceScalarWhereInput;
  data: ServiceUpdateManyDataInput;
};

export type ServiceCreateWithoutOwnerInput = {
  id?: Maybe<Scalars['ID']>;
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  subCategory?: Maybe<SubCategoryCreateOneWithoutServicesInput>;
  topics?: Maybe<TopicCreateManyWithoutServicesInput>;
  favoredUsers?: Maybe<UserCreateManyWithoutFavoriteServicesInput>;
};

export type ServiceUpdateWithWhereUniqueWithoutOwnerInput = {
  where: ServiceWhereUniqueInput;
  data: ServiceUpdateWithoutOwnerDataInput;
};

export type ServiceUpsertWithWhereUniqueWithoutOwnerInput = {
  where: ServiceWhereUniqueInput;
  update: ServiceUpdateWithoutOwnerDataInput;
  create: ServiceCreateWithoutOwnerInput;
};

export type TopicCreateWithoutUsersInput = {
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  services?: Maybe<ServiceCreateManyWithoutTopicsInput>;
};

export type TopicUpdateWithWhereUniqueWithoutUsersInput = {
  where: TopicWhereUniqueInput;
  data: TopicUpdateWithoutUsersDataInput;
};

export type TopicUpsertWithWhereUniqueWithoutUsersInput = {
  where: TopicWhereUniqueInput;
  update: TopicUpdateWithoutUsersDataInput;
  create: TopicCreateWithoutUsersInput;
};

export type TopicScalarWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_lt?: Maybe<Scalars['String']>;
  name_lte?: Maybe<Scalars['String']>;
  name_gt?: Maybe<Scalars['String']>;
  name_gte?: Maybe<Scalars['String']>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  AND?: Maybe<Array<TopicScalarWhereInput>>;
  OR?: Maybe<Array<TopicScalarWhereInput>>;
  NOT?: Maybe<Array<TopicScalarWhereInput>>;
};

export type TopicUpdateManyWithWhereNestedInput = {
  where: TopicScalarWhereInput;
  data: TopicUpdateManyDataInput;
};

export type ReviewCreateWithoutAuthorInput = {
  id?: Maybe<Scalars['ID']>;
  text: Scalars['String'];
  rating: Scalars['Int'];
  ratedConsultant: UserCreateOneWithoutRatedReviewsInput;
};

export type ReviewUpdateWithWhereUniqueWithoutAuthorInput = {
  where: ReviewWhereUniqueInput;
  data: ReviewUpdateWithoutAuthorDataInput;
};

export type ReviewUpsertWithWhereUniqueWithoutAuthorInput = {
  where: ReviewWhereUniqueInput;
  update: ReviewUpdateWithoutAuthorDataInput;
  create: ReviewCreateWithoutAuthorInput;
};

export type ReviewScalarWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  text?: Maybe<Scalars['String']>;
  text_not?: Maybe<Scalars['String']>;
  text_in?: Maybe<Array<Scalars['String']>>;
  text_not_in?: Maybe<Array<Scalars['String']>>;
  text_lt?: Maybe<Scalars['String']>;
  text_lte?: Maybe<Scalars['String']>;
  text_gt?: Maybe<Scalars['String']>;
  text_gte?: Maybe<Scalars['String']>;
  text_contains?: Maybe<Scalars['String']>;
  text_not_contains?: Maybe<Scalars['String']>;
  text_starts_with?: Maybe<Scalars['String']>;
  text_not_starts_with?: Maybe<Scalars['String']>;
  text_ends_with?: Maybe<Scalars['String']>;
  text_not_ends_with?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['Int']>;
  rating_not?: Maybe<Scalars['Int']>;
  rating_in?: Maybe<Array<Scalars['Int']>>;
  rating_not_in?: Maybe<Array<Scalars['Int']>>;
  rating_lt?: Maybe<Scalars['Int']>;
  rating_lte?: Maybe<Scalars['Int']>;
  rating_gt?: Maybe<Scalars['Int']>;
  rating_gte?: Maybe<Scalars['Int']>;
  AND?: Maybe<Array<ReviewScalarWhereInput>>;
  OR?: Maybe<Array<ReviewScalarWhereInput>>;
  NOT?: Maybe<Array<ReviewScalarWhereInput>>;
};

export type ReviewUpdateManyWithWhereNestedInput = {
  where: ReviewScalarWhereInput;
  data: ReviewUpdateManyDataInput;
};

export type ReviewCreateWithoutRatedConsultantInput = {
  id?: Maybe<Scalars['ID']>;
  text: Scalars['String'];
  rating: Scalars['Int'];
  author: UserCreateOneWithoutWrittenReviewsInput;
};

export type ReviewUpdateWithWhereUniqueWithoutRatedConsultantInput = {
  where: ReviewWhereUniqueInput;
  data: ReviewUpdateWithoutRatedConsultantDataInput;
};

export type ReviewUpsertWithWhereUniqueWithoutRatedConsultantInput = {
  where: ReviewWhereUniqueInput;
  update: ReviewUpdateWithoutRatedConsultantDataInput;
  create: ReviewCreateWithoutRatedConsultantInput;
};

export type MessageCreateWithoutAuthorInput = {
  id?: Maybe<Scalars['ID']>;
  text: Scalars['String'];
  chat: ChatCreateOneWithoutMessagesInput;
};

export type MessageUpdateWithWhereUniqueWithoutAuthorInput = {
  where: MessageWhereUniqueInput;
  data: MessageUpdateWithoutAuthorDataInput;
};

export type MessageUpsertWithWhereUniqueWithoutAuthorInput = {
  where: MessageWhereUniqueInput;
  update: MessageUpdateWithoutAuthorDataInput;
  create: MessageCreateWithoutAuthorInput;
};

export type MessageScalarWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  text?: Maybe<Scalars['String']>;
  text_not?: Maybe<Scalars['String']>;
  text_in?: Maybe<Array<Scalars['String']>>;
  text_not_in?: Maybe<Array<Scalars['String']>>;
  text_lt?: Maybe<Scalars['String']>;
  text_lte?: Maybe<Scalars['String']>;
  text_gt?: Maybe<Scalars['String']>;
  text_gte?: Maybe<Scalars['String']>;
  text_contains?: Maybe<Scalars['String']>;
  text_not_contains?: Maybe<Scalars['String']>;
  text_starts_with?: Maybe<Scalars['String']>;
  text_not_starts_with?: Maybe<Scalars['String']>;
  text_ends_with?: Maybe<Scalars['String']>;
  text_not_ends_with?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdAt_not?: Maybe<Scalars['DateTime']>;
  createdAt_in?: Maybe<Array<Scalars['DateTime']>>;
  createdAt_not_in?: Maybe<Array<Scalars['DateTime']>>;
  createdAt_lt?: Maybe<Scalars['DateTime']>;
  createdAt_lte?: Maybe<Scalars['DateTime']>;
  createdAt_gt?: Maybe<Scalars['DateTime']>;
  createdAt_gte?: Maybe<Scalars['DateTime']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  updatedAt_not?: Maybe<Scalars['DateTime']>;
  updatedAt_in?: Maybe<Array<Scalars['DateTime']>>;
  updatedAt_not_in?: Maybe<Array<Scalars['DateTime']>>;
  updatedAt_lt?: Maybe<Scalars['DateTime']>;
  updatedAt_lte?: Maybe<Scalars['DateTime']>;
  updatedAt_gt?: Maybe<Scalars['DateTime']>;
  updatedAt_gte?: Maybe<Scalars['DateTime']>;
  AND?: Maybe<Array<MessageScalarWhereInput>>;
  OR?: Maybe<Array<MessageScalarWhereInput>>;
  NOT?: Maybe<Array<MessageScalarWhereInput>>;
};

export type MessageUpdateManyWithWhereNestedInput = {
  where: MessageScalarWhereInput;
  data: MessageUpdateManyDataInput;
};

export type UserCreateWithoutFavoriteConsultantsInput = {
  id?: Maybe<Scalars['ID']>;
  email: Scalars['String'];
  password: Scalars['String'];
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceCreateManyWithoutOwnerInput>;
  topics?: Maybe<TopicCreateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewCreateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewCreateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageCreateManyWithoutAuthorInput>;
  favoriteServices?: Maybe<ServiceCreateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserCreateManyWithoutConsultantsInput>;
};

export type UserUpdateWithWhereUniqueWithoutFavoriteConsultantsInput = {
  where: UserWhereUniqueInput;
  data: UserUpdateWithoutFavoriteConsultantsDataInput;
};

export type UserUpsertWithWhereUniqueWithoutFavoriteConsultantsInput = {
  where: UserWhereUniqueInput;
  update: UserUpdateWithoutFavoriteConsultantsDataInput;
  create: UserCreateWithoutFavoriteConsultantsInput;
};

export type UserScalarWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  email?: Maybe<Scalars['String']>;
  email_not?: Maybe<Scalars['String']>;
  email_in?: Maybe<Array<Scalars['String']>>;
  email_not_in?: Maybe<Array<Scalars['String']>>;
  email_lt?: Maybe<Scalars['String']>;
  email_lte?: Maybe<Scalars['String']>;
  email_gt?: Maybe<Scalars['String']>;
  email_gte?: Maybe<Scalars['String']>;
  email_contains?: Maybe<Scalars['String']>;
  email_not_contains?: Maybe<Scalars['String']>;
  email_starts_with?: Maybe<Scalars['String']>;
  email_not_starts_with?: Maybe<Scalars['String']>;
  email_ends_with?: Maybe<Scalars['String']>;
  email_not_ends_with?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  password_not?: Maybe<Scalars['String']>;
  password_in?: Maybe<Array<Scalars['String']>>;
  password_not_in?: Maybe<Array<Scalars['String']>>;
  password_lt?: Maybe<Scalars['String']>;
  password_lte?: Maybe<Scalars['String']>;
  password_gt?: Maybe<Scalars['String']>;
  password_gte?: Maybe<Scalars['String']>;
  password_contains?: Maybe<Scalars['String']>;
  password_not_contains?: Maybe<Scalars['String']>;
  password_starts_with?: Maybe<Scalars['String']>;
  password_not_starts_with?: Maybe<Scalars['String']>;
  password_ends_with?: Maybe<Scalars['String']>;
  password_not_ends_with?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  forename_not?: Maybe<Scalars['String']>;
  forename_in?: Maybe<Array<Scalars['String']>>;
  forename_not_in?: Maybe<Array<Scalars['String']>>;
  forename_lt?: Maybe<Scalars['String']>;
  forename_lte?: Maybe<Scalars['String']>;
  forename_gt?: Maybe<Scalars['String']>;
  forename_gte?: Maybe<Scalars['String']>;
  forename_contains?: Maybe<Scalars['String']>;
  forename_not_contains?: Maybe<Scalars['String']>;
  forename_starts_with?: Maybe<Scalars['String']>;
  forename_not_starts_with?: Maybe<Scalars['String']>;
  forename_ends_with?: Maybe<Scalars['String']>;
  forename_not_ends_with?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  lastname_not?: Maybe<Scalars['String']>;
  lastname_in?: Maybe<Array<Scalars['String']>>;
  lastname_not_in?: Maybe<Array<Scalars['String']>>;
  lastname_lt?: Maybe<Scalars['String']>;
  lastname_lte?: Maybe<Scalars['String']>;
  lastname_gt?: Maybe<Scalars['String']>;
  lastname_gte?: Maybe<Scalars['String']>;
  lastname_contains?: Maybe<Scalars['String']>;
  lastname_not_contains?: Maybe<Scalars['String']>;
  lastname_starts_with?: Maybe<Scalars['String']>;
  lastname_not_starts_with?: Maybe<Scalars['String']>;
  lastname_ends_with?: Maybe<Scalars['String']>;
  lastname_not_ends_with?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  picture_not?: Maybe<Scalars['String']>;
  picture_in?: Maybe<Array<Scalars['String']>>;
  picture_not_in?: Maybe<Array<Scalars['String']>>;
  picture_lt?: Maybe<Scalars['String']>;
  picture_lte?: Maybe<Scalars['String']>;
  picture_gt?: Maybe<Scalars['String']>;
  picture_gte?: Maybe<Scalars['String']>;
  picture_contains?: Maybe<Scalars['String']>;
  picture_not_contains?: Maybe<Scalars['String']>;
  picture_starts_with?: Maybe<Scalars['String']>;
  picture_not_starts_with?: Maybe<Scalars['String']>;
  picture_ends_with?: Maybe<Scalars['String']>;
  picture_not_ends_with?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  price_not?: Maybe<Scalars['Float']>;
  price_in?: Maybe<Array<Scalars['Float']>>;
  price_not_in?: Maybe<Array<Scalars['Float']>>;
  price_lt?: Maybe<Scalars['Float']>;
  price_lte?: Maybe<Scalars['Float']>;
  price_gt?: Maybe<Scalars['Float']>;
  price_gte?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  phoneNumber_not?: Maybe<Scalars['String']>;
  phoneNumber_in?: Maybe<Array<Scalars['String']>>;
  phoneNumber_not_in?: Maybe<Array<Scalars['String']>>;
  phoneNumber_lt?: Maybe<Scalars['String']>;
  phoneNumber_lte?: Maybe<Scalars['String']>;
  phoneNumber_gt?: Maybe<Scalars['String']>;
  phoneNumber_gte?: Maybe<Scalars['String']>;
  phoneNumber_contains?: Maybe<Scalars['String']>;
  phoneNumber_not_contains?: Maybe<Scalars['String']>;
  phoneNumber_starts_with?: Maybe<Scalars['String']>;
  phoneNumber_not_starts_with?: Maybe<Scalars['String']>;
  phoneNumber_ends_with?: Maybe<Scalars['String']>;
  phoneNumber_not_ends_with?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  role_not?: Maybe<Role>;
  role_in?: Maybe<Array<Role>>;
  role_not_in?: Maybe<Array<Role>>;
  description?: Maybe<Scalars['String']>;
  description_not?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Scalars['String']>>;
  description_not_in?: Maybe<Array<Scalars['String']>>;
  description_lt?: Maybe<Scalars['String']>;
  description_lte?: Maybe<Scalars['String']>;
  description_gt?: Maybe<Scalars['String']>;
  description_gte?: Maybe<Scalars['String']>;
  description_contains?: Maybe<Scalars['String']>;
  description_not_contains?: Maybe<Scalars['String']>;
  description_starts_with?: Maybe<Scalars['String']>;
  description_not_starts_with?: Maybe<Scalars['String']>;
  description_ends_with?: Maybe<Scalars['String']>;
  description_not_ends_with?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  verificationCode_not?: Maybe<Scalars['String']>;
  verificationCode_in?: Maybe<Array<Scalars['String']>>;
  verificationCode_not_in?: Maybe<Array<Scalars['String']>>;
  verificationCode_lt?: Maybe<Scalars['String']>;
  verificationCode_lte?: Maybe<Scalars['String']>;
  verificationCode_gt?: Maybe<Scalars['String']>;
  verificationCode_gte?: Maybe<Scalars['String']>;
  verificationCode_contains?: Maybe<Scalars['String']>;
  verificationCode_not_contains?: Maybe<Scalars['String']>;
  verificationCode_starts_with?: Maybe<Scalars['String']>;
  verificationCode_not_starts_with?: Maybe<Scalars['String']>;
  verificationCode_ends_with?: Maybe<Scalars['String']>;
  verificationCode_not_ends_with?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  active_not?: Maybe<Scalars['Boolean']>;
  AND?: Maybe<Array<UserScalarWhereInput>>;
  OR?: Maybe<Array<UserScalarWhereInput>>;
  NOT?: Maybe<Array<UserScalarWhereInput>>;
};

export type UserUpdateManyWithWhereNestedInput = {
  where: UserScalarWhereInput;
  data: UserUpdateManyDataInput;
};

export type UserCreateWithoutConsultantsInput = {
  id?: Maybe<Scalars['ID']>;
  email: Scalars['String'];
  password: Scalars['String'];
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceCreateManyWithoutOwnerInput>;
  topics?: Maybe<TopicCreateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewCreateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewCreateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageCreateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserCreateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceCreateManyWithoutFavoredUsersInput>;
};

export type UserUpdateWithWhereUniqueWithoutConsultantsInput = {
  where: UserWhereUniqueInput;
  data: UserUpdateWithoutConsultantsDataInput;
};

export type UserUpsertWithWhereUniqueWithoutConsultantsInput = {
  where: UserWhereUniqueInput;
  update: UserUpdateWithoutConsultantsDataInput;
  create: UserCreateWithoutConsultantsInput;
};

export type SubCategoryCreateWithoutServicesInput = {
  id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  category: CategoryCreateOneWithoutSubCategoriesInput;
};

export type SubCategoryUpdateWithoutServicesDataInput = {
  name?: Maybe<Scalars['String']>;
  category?: Maybe<CategoryUpdateOneRequiredWithoutSubCategoriesInput>;
};

export type SubCategoryUpsertWithoutServicesInput = {
  update: SubCategoryUpdateWithoutServicesDataInput;
  create: SubCategoryCreateWithoutServicesInput;
};

export type TopicCreateWithoutServicesInput = {
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  users?: Maybe<UserCreateManyWithoutTopicsInput>;
};

export type TopicUpdateWithWhereUniqueWithoutServicesInput = {
  where: TopicWhereUniqueInput;
  data: TopicUpdateWithoutServicesDataInput;
};

export type TopicUpsertWithWhereUniqueWithoutServicesInput = {
  where: TopicWhereUniqueInput;
  update: TopicUpdateWithoutServicesDataInput;
  create: TopicCreateWithoutServicesInput;
};

export type UserCreateWithoutFavoriteServicesInput = {
  id?: Maybe<Scalars['ID']>;
  email: Scalars['String'];
  password: Scalars['String'];
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceCreateManyWithoutOwnerInput>;
  topics?: Maybe<TopicCreateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewCreateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewCreateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageCreateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserCreateManyWithoutFavoriteConsultantsInput>;
  consultants?: Maybe<UserCreateManyWithoutConsultantsInput>;
};

export type UserUpdateWithWhereUniqueWithoutFavoriteServicesInput = {
  where: UserWhereUniqueInput;
  data: UserUpdateWithoutFavoriteServicesDataInput;
};

export type UserUpsertWithWhereUniqueWithoutFavoriteServicesInput = {
  where: UserWhereUniqueInput;
  update: UserUpdateWithoutFavoriteServicesDataInput;
  create: UserCreateWithoutFavoriteServicesInput;
};

export type UserCreateWithoutServicesInput = {
  id?: Maybe<Scalars['ID']>;
  email: Scalars['String'];
  password: Scalars['String'];
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  topics?: Maybe<TopicCreateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewCreateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewCreateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageCreateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserCreateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceCreateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserCreateManyWithoutConsultantsInput>;
};

export type UserUpdateWithoutServicesDataInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  topics?: Maybe<TopicUpdateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewUpdateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewUpdateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageUpdateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserUpdateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceUpdateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserUpdateManyWithoutConsultantsInput>;
};

export type UserUpsertWithoutServicesInput = {
  update: UserUpdateWithoutServicesDataInput;
  create: UserCreateWithoutServicesInput;
};

export type SubCategoryCreateWithoutCategoryInput = {
  id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  services?: Maybe<ServiceCreateManyWithoutSubCategoryInput>;
};

export type SubCategoryUpdateWithWhereUniqueWithoutCategoryInput = {
  where: SubCategoryWhereUniqueInput;
  data: SubCategoryUpdateWithoutCategoryDataInput;
};

export type SubCategoryUpsertWithWhereUniqueWithoutCategoryInput = {
  where: SubCategoryWhereUniqueInput;
  update: SubCategoryUpdateWithoutCategoryDataInput;
  create: SubCategoryCreateWithoutCategoryInput;
};

export type SubCategoryScalarWhereInput = {
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_lt?: Maybe<Scalars['ID']>;
  id_lte?: Maybe<Scalars['ID']>;
  id_gt?: Maybe<Scalars['ID']>;
  id_gte?: Maybe<Scalars['ID']>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_lt?: Maybe<Scalars['String']>;
  name_lte?: Maybe<Scalars['String']>;
  name_gt?: Maybe<Scalars['String']>;
  name_gte?: Maybe<Scalars['String']>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  AND?: Maybe<Array<SubCategoryScalarWhereInput>>;
  OR?: Maybe<Array<SubCategoryScalarWhereInput>>;
  NOT?: Maybe<Array<SubCategoryScalarWhereInput>>;
};

export type SubCategoryUpdateManyWithWhereNestedInput = {
  where: SubCategoryScalarWhereInput;
  data: SubCategoryUpdateManyDataInput;
};

export type MessageCreateWithoutChatInput = {
  id?: Maybe<Scalars['ID']>;
  text: Scalars['String'];
  author: UserCreateOneWithoutMessagesInput;
};

export type MessageUpdateWithWhereUniqueWithoutChatInput = {
  where: MessageWhereUniqueInput;
  data: MessageUpdateWithoutChatDataInput;
};

export type MessageUpsertWithWhereUniqueWithoutChatInput = {
  where: MessageWhereUniqueInput;
  update: MessageUpdateWithoutChatDataInput;
  create: MessageCreateWithoutChatInput;
};

export type ChatCreateWithoutMessagesInput = {
  id?: Maybe<Scalars['ID']>;
};

export type UserCreateWithoutMessagesInput = {
  id?: Maybe<Scalars['ID']>;
  email: Scalars['String'];
  password: Scalars['String'];
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceCreateManyWithoutOwnerInput>;
  topics?: Maybe<TopicCreateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewCreateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewCreateManyWithoutRatedConsultantInput>;
  favoriteConsultants?: Maybe<UserCreateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceCreateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserCreateManyWithoutConsultantsInput>;
};

export type UserUpdateWithoutMessagesDataInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceUpdateManyWithoutOwnerInput>;
  topics?: Maybe<TopicUpdateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewUpdateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewUpdateManyWithoutRatedConsultantInput>;
  favoriteConsultants?: Maybe<UserUpdateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceUpdateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserUpdateManyWithoutConsultantsInput>;
};

export type UserUpsertWithoutMessagesInput = {
  update: UserUpdateWithoutMessagesDataInput;
  create: UserCreateWithoutMessagesInput;
};

export type UserCreateWithoutWrittenReviewsInput = {
  id?: Maybe<Scalars['ID']>;
  email: Scalars['String'];
  password: Scalars['String'];
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceCreateManyWithoutOwnerInput>;
  topics?: Maybe<TopicCreateManyWithoutUsersInput>;
  ratedReviews?: Maybe<ReviewCreateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageCreateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserCreateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceCreateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserCreateManyWithoutConsultantsInput>;
};

export type UserCreateWithoutRatedReviewsInput = {
  id?: Maybe<Scalars['ID']>;
  email: Scalars['String'];
  password: Scalars['String'];
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceCreateManyWithoutOwnerInput>;
  topics?: Maybe<TopicCreateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewCreateManyWithoutAuthorInput>;
  messages?: Maybe<MessageCreateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserCreateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceCreateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserCreateManyWithoutConsultantsInput>;
};

export type UserUpdateWithoutWrittenReviewsDataInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceUpdateManyWithoutOwnerInput>;
  topics?: Maybe<TopicUpdateManyWithoutUsersInput>;
  ratedReviews?: Maybe<ReviewUpdateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageUpdateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserUpdateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceUpdateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserUpdateManyWithoutConsultantsInput>;
};

export type UserUpsertWithoutWrittenReviewsInput = {
  update: UserUpdateWithoutWrittenReviewsDataInput;
  create: UserCreateWithoutWrittenReviewsInput;
};

export type UserUpdateWithoutRatedReviewsDataInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceUpdateManyWithoutOwnerInput>;
  topics?: Maybe<TopicUpdateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewUpdateManyWithoutAuthorInput>;
  messages?: Maybe<MessageUpdateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserUpdateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceUpdateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserUpdateManyWithoutConsultantsInput>;
};

export type UserUpsertWithoutRatedReviewsInput = {
  update: UserUpdateWithoutRatedReviewsDataInput;
  create: UserCreateWithoutRatedReviewsInput;
};

export type CategoryCreateWithoutSubCategoriesInput = {
  id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
};

export type CategoryUpdateWithoutSubCategoriesDataInput = {
  name?: Maybe<Scalars['String']>;
};

export type CategoryUpsertWithoutSubCategoriesInput = {
  update: CategoryUpdateWithoutSubCategoriesDataInput;
  create: CategoryCreateWithoutSubCategoriesInput;
};

export type ServiceCreateWithoutSubCategoryInput = {
  id?: Maybe<Scalars['ID']>;
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  topics?: Maybe<TopicCreateManyWithoutServicesInput>;
  favoredUsers?: Maybe<UserCreateManyWithoutFavoriteServicesInput>;
  owner: UserCreateOneWithoutServicesInput;
};

export type ServiceUpdateWithWhereUniqueWithoutSubCategoryInput = {
  where: ServiceWhereUniqueInput;
  data: ServiceUpdateWithoutSubCategoryDataInput;
};

export type ServiceUpsertWithWhereUniqueWithoutSubCategoryInput = {
  where: ServiceWhereUniqueInput;
  update: ServiceUpdateWithoutSubCategoryDataInput;
  create: ServiceCreateWithoutSubCategoryInput;
};

export type ServiceCreateWithoutTopicsInput = {
  id?: Maybe<Scalars['ID']>;
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  subCategory?: Maybe<SubCategoryCreateOneWithoutServicesInput>;
  favoredUsers?: Maybe<UserCreateManyWithoutFavoriteServicesInput>;
  owner: UserCreateOneWithoutServicesInput;
};

export type ServiceUpdateWithWhereUniqueWithoutTopicsInput = {
  where: ServiceWhereUniqueInput;
  data: ServiceUpdateWithoutTopicsDataInput;
};

export type ServiceUpsertWithWhereUniqueWithoutTopicsInput = {
  where: ServiceWhereUniqueInput;
  update: ServiceUpdateWithoutTopicsDataInput;
  create: ServiceCreateWithoutTopicsInput;
};

export type UserCreateWithoutTopicsInput = {
  id?: Maybe<Scalars['ID']>;
  email: Scalars['String'];
  password: Scalars['String'];
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceCreateManyWithoutOwnerInput>;
  writtenReviews?: Maybe<ReviewCreateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewCreateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageCreateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserCreateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceCreateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserCreateManyWithoutConsultantsInput>;
};

export type UserUpdateWithWhereUniqueWithoutTopicsInput = {
  where: UserWhereUniqueInput;
  data: UserUpdateWithoutTopicsDataInput;
};

export type UserUpsertWithWhereUniqueWithoutTopicsInput = {
  where: UserWhereUniqueInput;
  update: UserUpdateWithoutTopicsDataInput;
  create: UserCreateWithoutTopicsInput;
};

export type ServiceUpdateWithoutFavoredUsersDataInput = {
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  subCategory?: Maybe<SubCategoryUpdateOneWithoutServicesInput>;
  topics?: Maybe<TopicUpdateManyWithoutServicesInput>;
  owner?: Maybe<UserUpdateOneRequiredWithoutServicesInput>;
};

export type ServiceUpdateManyDataInput = {
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
};

export type ServiceUpdateWithoutOwnerDataInput = {
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  subCategory?: Maybe<SubCategoryUpdateOneWithoutServicesInput>;
  topics?: Maybe<TopicUpdateManyWithoutServicesInput>;
  favoredUsers?: Maybe<UserUpdateManyWithoutFavoriteServicesInput>;
};

export type TopicUpdateWithoutUsersDataInput = {
  name?: Maybe<Scalars['String']>;
  services?: Maybe<ServiceUpdateManyWithoutTopicsInput>;
};

export type TopicUpdateManyDataInput = {
  name?: Maybe<Scalars['String']>;
};

export type ReviewUpdateWithoutAuthorDataInput = {
  text?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['Int']>;
  ratedConsultant?: Maybe<UserUpdateOneRequiredWithoutRatedReviewsInput>;
};

export type ReviewUpdateManyDataInput = {
  text?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['Int']>;
};

export type ReviewUpdateWithoutRatedConsultantDataInput = {
  text?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['Int']>;
  author?: Maybe<UserUpdateOneRequiredWithoutWrittenReviewsInput>;
};

export type MessageUpdateWithoutAuthorDataInput = {
  text?: Maybe<Scalars['String']>;
  chat?: Maybe<ChatUpdateOneRequiredWithoutMessagesInput>;
};

export type MessageUpdateManyDataInput = {
  text?: Maybe<Scalars['String']>;
};

export type UserUpdateWithoutFavoriteConsultantsDataInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceUpdateManyWithoutOwnerInput>;
  topics?: Maybe<TopicUpdateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewUpdateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewUpdateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageUpdateManyWithoutAuthorInput>;
  favoriteServices?: Maybe<ServiceUpdateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserUpdateManyWithoutConsultantsInput>;
};

export type UserUpdateManyDataInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
};

export type UserUpdateWithoutConsultantsDataInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceUpdateManyWithoutOwnerInput>;
  topics?: Maybe<TopicUpdateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewUpdateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewUpdateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageUpdateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserUpdateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceUpdateManyWithoutFavoredUsersInput>;
};

export type TopicUpdateWithoutServicesDataInput = {
  name?: Maybe<Scalars['String']>;
  users?: Maybe<UserUpdateManyWithoutTopicsInput>;
};

export type UserUpdateWithoutFavoriteServicesDataInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceUpdateManyWithoutOwnerInput>;
  topics?: Maybe<TopicUpdateManyWithoutUsersInput>;
  writtenReviews?: Maybe<ReviewUpdateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewUpdateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageUpdateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserUpdateManyWithoutFavoriteConsultantsInput>;
  consultants?: Maybe<UserUpdateManyWithoutConsultantsInput>;
};

export type SubCategoryUpdateWithoutCategoryDataInput = {
  name?: Maybe<Scalars['String']>;
  services?: Maybe<ServiceUpdateManyWithoutSubCategoryInput>;
};

export type SubCategoryUpdateManyDataInput = {
  name?: Maybe<Scalars['String']>;
};

export type MessageUpdateWithoutChatDataInput = {
  text?: Maybe<Scalars['String']>;
  author?: Maybe<UserUpdateOneRequiredWithoutMessagesInput>;
};

export type ServiceUpdateWithoutSubCategoryDataInput = {
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  topics?: Maybe<TopicUpdateManyWithoutServicesInput>;
  favoredUsers?: Maybe<UserUpdateManyWithoutFavoriteServicesInput>;
  owner?: Maybe<UserUpdateOneRequiredWithoutServicesInput>;
};

export type ServiceUpdateWithoutTopicsDataInput = {
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  subCategory?: Maybe<SubCategoryUpdateOneWithoutServicesInput>;
  favoredUsers?: Maybe<UserUpdateManyWithoutFavoriteServicesInput>;
  owner?: Maybe<UserUpdateOneRequiredWithoutServicesInput>;
};

export type UserUpdateWithoutTopicsDataInput = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  forename?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  description?: Maybe<Scalars['String']>;
  verificationCode?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  services?: Maybe<ServiceUpdateManyWithoutOwnerInput>;
  writtenReviews?: Maybe<ReviewUpdateManyWithoutAuthorInput>;
  ratedReviews?: Maybe<ReviewUpdateManyWithoutRatedConsultantInput>;
  messages?: Maybe<MessageUpdateManyWithoutAuthorInput>;
  favoriteConsultants?: Maybe<UserUpdateManyWithoutFavoriteConsultantsInput>;
  favoriteServices?: Maybe<ServiceUpdateManyWithoutFavoredUsersInput>;
  consultants?: Maybe<UserUpdateManyWithoutConsultantsInput>;
};

export enum CacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}


export type Node = {
  id: Scalars['ID'];
};

export type GetAllCategoriesQueryVariables = Exact<{
  where?: Maybe<CategoryWhereInput>;
  orderBy?: Maybe<CategoryOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
}>;


export type GetAllCategoriesQuery = (
  { __typename?: 'Query' }
  & { categories: Array<(
    { __typename?: 'Category' }
    & Pick<Category, 'id' | 'name'>
    & { subCategories?: Maybe<Array<(
      { __typename?: 'SubCategory' }
      & Pick<SubCategory, 'id' | 'name'>
    )>> }
  )> }
);

export type GetCategoryQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetCategoryQuery = (
  { __typename?: 'Query' }
  & { category: (
    { __typename?: 'Category' }
    & Pick<Category, 'id' | 'name'>
    & { subCategories?: Maybe<Array<(
      { __typename?: 'SubCategory' }
      & Pick<SubCategory, 'id' | 'name'>
    )>> }
  ) }
);

export type CreateCategoryMutationVariables = Exact<{
  name: Scalars['String'];
}>;


export type CreateCategoryMutation = (
  { __typename?: 'Mutation' }
  & { createCategory: (
    { __typename?: 'Category' }
    & Pick<Category, 'id' | 'name'>
    & { subCategories?: Maybe<Array<(
      { __typename?: 'SubCategory' }
      & Pick<SubCategory, 'id' | 'name'>
    )>> }
  ) }
);

export type GetOneServiceQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetOneServiceQuery = (
  { __typename?: 'Query' }
  & { service: (
    { __typename?: 'Service' }
    & Pick<Service, 'id' | 'title' | 'description' | 'picture' | 'video'>
    & { owner: (
      { __typename?: 'User' }
      & Pick<User, 'id' | 'email' | 'forename' | 'lastname' | 'picture' | 'price' | 'phoneNumber' | 'description'>
    ), subCategory?: Maybe<(
      { __typename?: 'SubCategory' }
      & Pick<SubCategory, 'id'>
      & { category: (
        { __typename?: 'Category' }
        & Pick<Category, 'id'>
      ) }
    )> }
  ) }
);

export type GetAllServicesQueryVariables = Exact<{
  where?: Maybe<ServiceWhereInput>;
  orderBy?: Maybe<ServiceOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
}>;


export type GetAllServicesQuery = (
  { __typename?: 'Query' }
  & { services: Array<(
    { __typename?: 'Service' }
    & Pick<Service, 'id' | 'title' | 'description' | 'picture' | 'video'>
    & { owner: (
      { __typename?: 'User' }
      & Pick<User, 'id' | 'price'>
    ) }
  )> }
);

export type CreateServiceMutationVariables = Exact<{
  data: ServiceInput;
}>;


export type CreateServiceMutation = (
  { __typename?: 'Mutation' }
  & { createService: (
    { __typename?: 'Service' }
    & Pick<Service, 'id' | 'title' | 'description' | 'picture' | 'video'>
    & { owner: (
      { __typename?: 'User' }
      & Pick<User, 'id' | 'price'>
    ) }
  ) }
);

export type UpdateServiceMutationVariables = Exact<{
  data: ServiceUpdateInput;
  serviceId: Scalars['ID'];
}>;


export type UpdateServiceMutation = (
  { __typename?: 'Mutation' }
  & { updateService: (
    { __typename?: 'Service' }
    & Pick<Service, 'id' | 'title' | 'description' | 'picture' | 'video'>
    & { owner: (
      { __typename?: 'User' }
      & Pick<User, 'id' | 'price'>
    ) }
  ) }
);

export type GetSubCategoryQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetSubCategoryQuery = (
  { __typename?: 'Query' }
  & { subCategory: (
    { __typename?: 'SubCategory' }
    & Pick<SubCategory, 'id' | 'name'>
  ) }
);

export type GetAllSubCategoriesQueryVariables = Exact<{
  where?: Maybe<SubCategoryWhereInput>;
  orderBy?: Maybe<SubCategoryOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
}>;


export type GetAllSubCategoriesQuery = (
  { __typename?: 'Query' }
  & { subCategories: Array<(
    { __typename?: 'SubCategory' }
    & Pick<SubCategory, 'id' | 'name'>
  )> }
);

export type CreateSubCategoryMutationVariables = Exact<{
  name: Scalars['String'];
  categoryId: Scalars['ID'];
}>;


export type CreateSubCategoryMutation = (
  { __typename?: 'Mutation' }
  & { createSubCategory: (
    { __typename?: 'SubCategory' }
    & Pick<SubCategory, 'id' | 'name'>
    & { category: (
      { __typename?: 'Category' }
      & Pick<Category, 'id'>
    ) }
  ) }
);

export type GetAllTopicsQueryVariables = Exact<{
  where?: Maybe<TopicWhereInput>;
  orderBy?: Maybe<TopicOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
}>;


export type GetAllTopicsQuery = (
  { __typename?: 'Query' }
  & { topics: Array<(
    { __typename?: 'Topic' }
    & Pick<Topic, 'id' | 'name'>
  )> }
);

export type GetTopicQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetTopicQuery = (
  { __typename?: 'Query' }
  & { topic: (
    { __typename?: 'Topic' }
    & Pick<Topic, 'id' | 'name'>
  ) }
);

export type CreateTopicMutationVariables = Exact<{
  data: TopicInput;
}>;


export type CreateTopicMutation = (
  { __typename?: 'Mutation' }
  & { createTopic: (
    { __typename?: 'Topic' }
    & Pick<Topic, 'id'>
  ) }
);

export type LoginUserQueryVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginUserQuery = (
  { __typename?: 'Query' }
  & { login: (
    { __typename?: 'AuthPayload' }
    & Pick<AuthPayload, 'token'>
    & { user: (
      { __typename?: 'User' }
      & Pick<User, 'id' | 'email' | 'forename' | 'lastname' | 'picture' | 'price' | 'phoneNumber' | 'role' | 'description'>
      & { favoriteServices?: Maybe<Array<(
        { __typename?: 'Service' }
        & Pick<Service, 'id' | 'title' | 'description' | 'picture'>
        & { owner: (
          { __typename?: 'User' }
          & Pick<User, 'id' | 'price'>
        ) }
      )>> }
    ) }
  ) }
);

export type CheckEmailQueryVariables = Exact<{
  email: Scalars['String'];
}>;


export type CheckEmailQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'checkEmailAddress'>
);

export type GetFavoriteServicesQueryVariables = Exact<{
  where?: Maybe<ServiceWhereInput>;
  orderBy?: Maybe<ServiceOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
}>;


export type GetFavoriteServicesQuery = (
  { __typename?: 'Query' }
  & { user: (
    { __typename?: 'User' }
    & { favoriteServices?: Maybe<Array<(
      { __typename?: 'Service' }
      & Pick<Service, 'id' | 'title' | 'description' | 'picture'>
      & { owner: (
        { __typename?: 'User' }
        & Pick<User, 'id' | 'price'>
      ) }
    )>> }
  ) }
);

export type GetFavoriteConsultantsQueryVariables = Exact<{
  where?: Maybe<UserWhereInput>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
}>;


export type GetFavoriteConsultantsQuery = (
  { __typename?: 'Query' }
  & { user: (
    { __typename?: 'User' }
    & { favoriteConsultants?: Maybe<Array<(
      { __typename?: 'User' }
      & Pick<User, 'id' | 'email'>
    )>> }
  ) }
);

export type GetMyServicesQueryVariables = Exact<{
  where?: Maybe<ServiceWhereInput>;
  orderBy?: Maybe<ServiceOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
}>;


export type GetMyServicesQuery = (
  { __typename?: 'Query' }
  & { user: (
    { __typename?: 'User' }
    & { services?: Maybe<Array<(
      { __typename?: 'Service' }
      & Pick<Service, 'id' | 'title' | 'description' | 'picture'>
      & { owner: (
        { __typename?: 'User' }
        & Pick<User, 'id' | 'price'>
      ) }
    )>> }
  ) }
);

export type RegistrateUserMutationVariables = Exact<{
  data: RegistrationData;
}>;


export type RegistrateUserMutation = (
  { __typename?: 'Mutation' }
  & { registrate: (
    { __typename?: 'User' }
    & Pick<User, 'id'>
  ) }
);

export type UpdateUserSettingsMutationVariables = Exact<{
  data: UserUpdateInput;
}>;


export type UpdateUserSettingsMutation = (
  { __typename?: 'Mutation' }
  & { updateUserSettings: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'email' | 'forename' | 'lastname' | 'picture' | 'price' | 'phoneNumber' | 'role' | 'description'>
    & { favoriteServices?: Maybe<Array<(
      { __typename?: 'Service' }
      & Pick<Service, 'id' | 'title' | 'description' | 'picture'>
      & { owner: (
        { __typename?: 'User' }
        & Pick<User, 'id' | 'price'>
      ) }
    )>>, services?: Maybe<Array<(
      { __typename?: 'Service' }
      & Pick<Service, 'id' | 'title' | 'description' | 'picture'>
      & { owner: (
        { __typename?: 'User' }
        & Pick<User, 'id' | 'price'>
      ) }
    )>> }
  ) }
);

export const GetAllCategoriesDocument = gql`
    query getAllCategories($where: CategoryWhereInput, $orderBy: CategoryOrderByInput, $skip: Int, $after: String, $before: String, $first: Int, $last: Int) {
  categories(where: $where, orderBy: $orderBy, skip: $skip, after: $after, before: $before, first: $first, last: $last) {
    id
    name
    subCategories {
      id
      name
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetAllCategoriesGQL extends Apollo.Query<GetAllCategoriesQuery, GetAllCategoriesQueryVariables> {
    document = GetAllCategoriesDocument;
    
  }
export const GetCategoryDocument = gql`
    query getCategory($id: ID!) {
  category(id: $id) {
    id
    name
    subCategories {
      id
      name
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetCategoryGQL extends Apollo.Query<GetCategoryQuery, GetCategoryQueryVariables> {
    document = GetCategoryDocument;
    
  }
export const CreateCategoryDocument = gql`
    mutation createCategory($name: String!) {
  createCategory(name: $name) {
    id
    name
    subCategories {
      id
      name
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateCategoryGQL extends Apollo.Mutation<CreateCategoryMutation, CreateCategoryMutationVariables> {
    document = CreateCategoryDocument;
    
  }
export const GetOneServiceDocument = gql`
    query getOneService($id: ID!) {
  service(id: $id) {
    id
    title
    description
    picture
    video
    owner {
      id
      email
      forename
      lastname
      picture
      price
      phoneNumber
      description
    }
    subCategory {
      id
      category {
        id
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetOneServiceGQL extends Apollo.Query<GetOneServiceQuery, GetOneServiceQueryVariables> {
    document = GetOneServiceDocument;
    
  }
export const GetAllServicesDocument = gql`
    query getAllServices($where: ServiceWhereInput, $orderBy: ServiceOrderByInput, $skip: Int, $after: String, $before: String, $first: Int, $last: Int) {
  services(where: $where, orderBy: $orderBy, skip: $skip, after: $after, before: $before, first: $first, last: $last) {
    id
    title
    description
    picture
    video
    owner {
      id
      price
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetAllServicesGQL extends Apollo.Query<GetAllServicesQuery, GetAllServicesQueryVariables> {
    document = GetAllServicesDocument;
    
  }
export const CreateServiceDocument = gql`
    mutation createService($data: ServiceInput!) {
  createService(data: $data) {
    id
    title
    description
    picture
    video
    owner {
      id
      price
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateServiceGQL extends Apollo.Mutation<CreateServiceMutation, CreateServiceMutationVariables> {
    document = CreateServiceDocument;
    
  }
export const UpdateServiceDocument = gql`
    mutation updateService($data: ServiceUpdateInput!, $serviceId: ID!) {
  updateService(data: $data, serviceId: $serviceId) {
    id
    title
    description
    picture
    video
    owner {
      id
      price
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdateServiceGQL extends Apollo.Mutation<UpdateServiceMutation, UpdateServiceMutationVariables> {
    document = UpdateServiceDocument;
    
  }
export const GetSubCategoryDocument = gql`
    query getSubCategory($id: ID!) {
  subCategory(id: $id) {
    id
    name
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetSubCategoryGQL extends Apollo.Query<GetSubCategoryQuery, GetSubCategoryQueryVariables> {
    document = GetSubCategoryDocument;
    
  }
export const GetAllSubCategoriesDocument = gql`
    query getAllSubCategories($where: SubCategoryWhereInput, $orderBy: SubCategoryOrderByInput, $skip: Int, $after: String, $before: String, $first: Int, $last: Int) {
  subCategories(where: $where, orderBy: $orderBy, skip: $skip, after: $after, before: $before, first: $first, last: $last) {
    id
    name
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetAllSubCategoriesGQL extends Apollo.Query<GetAllSubCategoriesQuery, GetAllSubCategoriesQueryVariables> {
    document = GetAllSubCategoriesDocument;
    
  }
export const CreateSubCategoryDocument = gql`
    mutation createSubCategory($name: String!, $categoryId: ID!) {
  createSubCategory(name: $name, categoryId: $categoryId) {
    id
    name
    category {
      id
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateSubCategoryGQL extends Apollo.Mutation<CreateSubCategoryMutation, CreateSubCategoryMutationVariables> {
    document = CreateSubCategoryDocument;
    
  }
export const GetAllTopicsDocument = gql`
    query getAllTopics($where: TopicWhereInput, $orderBy: TopicOrderByInput, $skip: Int, $after: String, $before: String, $first: Int, $last: Int) {
  topics(where: $where, orderBy: $orderBy, skip: $skip, after: $after, before: $before, first: $first, last: $last) {
    id
    name
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetAllTopicsGQL extends Apollo.Query<GetAllTopicsQuery, GetAllTopicsQueryVariables> {
    document = GetAllTopicsDocument;
    
  }
export const GetTopicDocument = gql`
    query getTopic($id: ID!) {
  topic(id: $id) {
    id
    name
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetTopicGQL extends Apollo.Query<GetTopicQuery, GetTopicQueryVariables> {
    document = GetTopicDocument;
    
  }
export const CreateTopicDocument = gql`
    mutation createTopic($data: TopicInput!) {
  createTopic(data: $data) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateTopicGQL extends Apollo.Mutation<CreateTopicMutation, CreateTopicMutationVariables> {
    document = CreateTopicDocument;
    
  }
export const LoginUserDocument = gql`
    query loginUser($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    token
    user {
      id
      email
      forename
      lastname
      picture
      price
      phoneNumber
      role
      description
      favoriteServices {
        id
        title
        description
        picture
        owner {
          id
          price
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LoginUserGQL extends Apollo.Query<LoginUserQuery, LoginUserQueryVariables> {
    document = LoginUserDocument;
    
  }
export const CheckEmailDocument = gql`
    query checkEmail($email: String!) {
  checkEmailAddress(email: $email)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CheckEmailGQL extends Apollo.Query<CheckEmailQuery, CheckEmailQueryVariables> {
    document = CheckEmailDocument;
    
  }
export const GetFavoriteServicesDocument = gql`
    query getFavoriteServices($where: ServiceWhereInput, $orderBy: ServiceOrderByInput, $skip: Int, $after: String, $before: String, $first: Int, $last: Int) {
  user {
    favoriteServices(where: $where, orderBy: $orderBy, skip: $skip, after: $after, before: $before, first: $first, last: $last) {
      id
      title
      description
      picture
      owner {
        id
        price
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetFavoriteServicesGQL extends Apollo.Query<GetFavoriteServicesQuery, GetFavoriteServicesQueryVariables> {
    document = GetFavoriteServicesDocument;
    
  }
export const GetFavoriteConsultantsDocument = gql`
    query getFavoriteConsultants($where: UserWhereInput, $orderBy: UserOrderByInput, $skip: Int, $after: String, $before: String, $first: Int, $last: Int) {
  user {
    favoriteConsultants(where: $where, orderBy: $orderBy, skip: $skip, after: $after, before: $before, first: $first, last: $last) {
      id
      email
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetFavoriteConsultantsGQL extends Apollo.Query<GetFavoriteConsultantsQuery, GetFavoriteConsultantsQueryVariables> {
    document = GetFavoriteConsultantsDocument;
    
  }
export const GetMyServicesDocument = gql`
    query getMyServices($where: ServiceWhereInput, $orderBy: ServiceOrderByInput, $skip: Int, $after: String, $before: String, $first: Int, $last: Int) {
  user {
    services(where: $where, orderBy: $orderBy, skip: $skip, after: $after, before: $before, first: $first, last: $last) {
      id
      title
      description
      picture
      owner {
        id
        price
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetMyServicesGQL extends Apollo.Query<GetMyServicesQuery, GetMyServicesQueryVariables> {
    document = GetMyServicesDocument;
    
  }
export const RegistrateUserDocument = gql`
    mutation registrateUser($data: RegistrationData!) {
  registrate(data: $data) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RegistrateUserGQL extends Apollo.Mutation<RegistrateUserMutation, RegistrateUserMutationVariables> {
    document = RegistrateUserDocument;
    
  }
export const UpdateUserSettingsDocument = gql`
    mutation updateUserSettings($data: UserUpdateInput!) {
  updateUserSettings(data: $data) {
    id
    email
    forename
    lastname
    picture
    price
    phoneNumber
    role
    description
    favoriteServices {
      id
      title
      description
      picture
      owner {
        id
        price
      }
    }
    services {
      id
      title
      description
      picture
      owner {
        id
        price
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdateUserSettingsGQL extends Apollo.Mutation<UpdateUserSettingsMutation, UpdateUserSettingsMutationVariables> {
    document = UpdateUserSettingsDocument;
    
  }