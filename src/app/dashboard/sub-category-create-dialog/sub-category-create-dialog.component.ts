import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubCategoryService } from '@app-graphql/services/sub-category.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { State } from '@app-store/index';
import {CreateSubCategoryAction} from '@app-store/category/actions/category.actions';

@Component({
  selector: 'app-sub-category-create-dialog',
  templateUrl: './sub-category-create-dialog.component.html',
  styleUrls: ['./sub-category-create-dialog.component.scss']
})
export class SubCategoryCreateDialogComponent implements OnInit {
  subCategoryForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<State>,
    private dataRef: MatDialogRef<SubCategoryCreateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: string
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.subCategoryForm = this.fb.group({
      name: ['', Validators.required]
    });
  }

  submitSubCategory(): void {
    const name = this.subCategoryForm.get('name').value;
    this.store.dispatch(new CreateSubCategoryAction(name, this.data));
    this.subCategoryForm.reset();
    this.dataRef.close();
  }
}
