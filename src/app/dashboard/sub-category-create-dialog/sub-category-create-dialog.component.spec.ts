import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubCategoryCreateDialogComponent } from './sub-category-create-dialog.component';

describe('SubCategoryCreateDialogComponent', () => {
  let component: SubCategoryCreateDialogComponent;
  let fixture: ComponentFixture<SubCategoryCreateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubCategoryCreateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubCategoryCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
