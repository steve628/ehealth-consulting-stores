import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Category, Service, User } from '@app-graphql/generated/graphql';
import { ActivatedRoute, Router } from '@angular/router';
import { map, take } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { getAllServices, getCategory, getUser, State } from '@app-store/index';
import { GetCategoryAction } from '@app-store/category/actions/category.actions';
import { GetAllServicesAction } from '@app-store/service/actions/service.actions';
import {UpdateUserSettingsAction} from '@app-store/user/actions/user.actions';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  category$: Observable<Category>;
  services$: Observable<Service[]>;
  user$: Observable<User>;

  constructor(
    private store: Store<State>,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.category$ = this.store.pipe(select(getCategory));
    this.services$ = this.store.pipe(select(getAllServices));
    this.user$ = this.store.pipe(select(getUser));
    this.route.paramMap.pipe(
      map(params => params.get('id')),
      take(1)
    ).subscribe((id: string) => {
      this.store.dispatch(new GetCategoryAction(id));
      this.store.dispatch(new GetAllServicesAction({
        where: { subCategory: { category: { id } } }
      }));
    });
  }

  addFavorite(serviceId: string): void {
    this.store.dispatch(new UpdateUserSettingsAction({
      favoriteServices: { connect: [{ id: serviceId }] }
    }));
  }

  removeFavorite(serviceId: string): void {
    this.store.dispatch(new UpdateUserSettingsAction({
      favoriteServices: { disconnect: [{ id: serviceId }] }
    }));
  }

  routeToService(id: string): void {
    this.router.navigate(['../../', 'service', id]);
  }
}
