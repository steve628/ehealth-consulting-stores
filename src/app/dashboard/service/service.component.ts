import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Service, User } from '@app-graphql/generated/graphql';
import { ActivatedRoute } from '@angular/router';
import { map, take } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { getService, getUser, State } from '@app-store/index';
import { GetServiceAction } from '@app-store/service/actions/service.actions';
import { UpdateUserSettingsAction } from '@app-store/user/actions/user.actions';
import { MatTabGroup } from '@angular/material/tabs';
import { Gesture } from '@app-core/classes/gesture';
import {display} from 'html2canvas/dist/types/css/property-descriptors/display';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit {
  service$: Observable<Service>;
  user$: Observable<User>;
  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;
  private readonly playerStandardWidth = 640;
  private readonly playerStandardHeight = 480;
  private readonly playerAspectRatio = 1.333;

  constructor(
    private store: Store<State>,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.user$ = this.store.pipe(select(getUser));
    this.service$ = this.store.pipe(select(getService));
    this.route.paramMap.pipe(
      map(params => params.get('id')),
      take(1)
    ).subscribe((id: string) => this.store.dispatch(new GetServiceAction(id)));
    this.loadYoutube();
  }

  private loadYoutube(): void {
    const tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    document.body.appendChild(tag);
  }

  loadPlayerWidth(): number {
    return screen.availWidth > this.playerStandardWidth ? this.playerStandardWidth : screen.availWidth * 0.7;
  }

  loadPlayerHeight(): number {
    return screen.availWidth > this.playerStandardWidth ? this.playerStandardHeight : (screen.availWidth * 0.7) / this.playerAspectRatio;
  }

  addFavorite(serviceId: string): void {
    this.store.dispatch(new UpdateUserSettingsAction({
      favoriteServices: { connect: [{ id: serviceId }] }
    }));
  }

  removeFavorite(serviceId: string): void {
    this.store.dispatch(new UpdateUserSettingsAction({
      favoriteServices: { disconnect: [{ id: serviceId }] }
    }));
  }

  onSwipe(event): void {
    this.tabGroup.selectedIndex = Gesture.onTabGroupSwipe(this.tabGroup, event);
  }
}
