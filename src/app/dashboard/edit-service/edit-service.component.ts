import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Category, ServiceUpdateInput } from '@app-graphql/generated/graphql';
import { select, Store } from '@ngrx/store';
import { getAllCategories, getService, State } from '@app-store/index';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { map, take } from 'rxjs/operators';
import { CategoryCreateDialogComponent } from '../category-create-dialog/category-create-dialog.component';
import { SubCategoryCreateDialogComponent } from '../sub-category-create-dialog/sub-category-create-dialog.component';
import { GetServiceAction, UpdateServiceAction } from '@app-store/service/actions/service.actions';
import { GetAllCategoriesAction } from '@app-store/category/actions/category.actions';
import {InfoDialogComponent} from '@app-core/components/info-dialog/info-dialog.component';

@Component({
  selector: 'app-edit-service',
  templateUrl: './edit-service.component.html',
  styleUrls: ['./edit-service.component.scss']
})
export class EditServiceComponent implements OnInit, OnDestroy {
  editForm: FormGroup;
  categories$: Observable<Category[]>;
  selectedFile: string | ArrayBuffer;
  selectedCategory: string;
  loadImage = false;
  private sub: Subscription;

  constructor(
    private fb: FormBuilder,
    private store: Store<State>,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private ng2ImgMax: Ng2ImgMaxService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.initObservables();
    this.route.paramMap.pipe(
      map(params => params.get('id')),
      take(1)
    ).subscribe((id: string) => {
      this.store.dispatch(new GetServiceAction(id));
      this.store.dispatch(new GetAllCategoriesAction());
    });
  }

  ngOnDestroy(): void {
    if (this.sub && !this.sub.closed) {
      this.sub.unsubscribe();
    }
  }

  private initForm(): void {
    this.editForm = this.fb.group({
      id: [''],
      title: ['', Validators.required],
      description: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      video: ['']
    });
  }

  private initObservables(): void {
    this.categories$ = this.store.pipe(select(getAllCategories));
    this.sub = this.store.pipe(select(getService)).subscribe(res => {
      if (res) {
        this.editForm.patchValue(res);
        this.selectedFile = res.picture;
        this.selectedCategory = res.subCategory.category.id;
        this.editForm.get('subCategoryId').patchValue(res.subCategory.id);
      }
    });
  }

  private readFile(file: File): void {
    this.loadImage = true;
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.loadImage = false;
      this.selectedFile = reader.result;
    });
    this.ng2ImgMax.resizeImage(file, 300, 10000)
      .pipe(take(1))
      .subscribe(result => {
        reader.readAsDataURL(result);
      });
  }

  onFileChanged(event): void {
    const file = event.target.files[0];
    this.readFile(file);
  }

  createCategory(): void {
    const dialogRef = this.dialog.open(CategoryCreateDialogComponent);
    dialogRef.afterClosed().pipe(take(1)).subscribe();
  }

  createSubCategory(): void {
    const dialogRef = this.dialog.open(SubCategoryCreateDialogComponent, { data: this.selectedCategory });
    dialogRef.afterClosed().pipe(take(1)).subscribe();
  }

  openInfoDialog(infoType: string): void {
    const dialogRef = this.dialog.open(InfoDialogComponent, { data: infoType });
    dialogRef.afterClosed().pipe(take(1)).subscribe();
  }

  submitService(): void {
    const video = this.editForm.get('video').value && this.editForm.get('video').value.split('=')
      ? this.editForm.get('video').value.split('=')[1] : this.editForm.get('video').value;
    const data: ServiceUpdateInput = {
      title: this.editForm.get('title').value,
      description: this.editForm.get('description').value,
      picture: this.selectedFile as string,
      video,
      subCategory: { connect: { id: this.editForm.get('subCategoryId').value } }
    };
    const serviceId = this.editForm.get('id').value;
    this.editForm.reset();
    this.store.dispatch(new UpdateServiceAction(data, serviceId));
    this.router.navigate(['../', 'my-services']);
  }
}
