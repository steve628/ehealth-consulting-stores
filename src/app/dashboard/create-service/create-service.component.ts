import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Category, ServiceInput } from '@app-graphql/generated/graphql';
import { MatDialog } from '@angular/material/dialog';
import { CategoryCreateDialogComponent } from '../category-create-dialog/category-create-dialog.component';
import { take } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { getAllCategories, State } from '@app-store/index';
import { GetAllCategoriesAction } from '@app-store/category/actions/category.actions';
import { SubCategoryCreateDialogComponent } from '../sub-category-create-dialog/sub-category-create-dialog.component';
import { Router } from '@angular/router';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { CreateServiceAction } from '@app-store/service/actions/service.actions';
import { InfoDialogComponent } from '@app-core/components/info-dialog/info-dialog.component';

@Component({
  selector: 'app-create-service',
  templateUrl: './create-service.component.html',
  styleUrls: ['./create-service.component.scss']
})
export class CreateServiceComponent implements OnInit {
  createGroup: FormGroup;
  categories$: Observable<Category[]>;
  selectedCategory: string;
  selectedFile: string | ArrayBuffer;
  loadImage = false;

  constructor(
    private fb: FormBuilder,
    private store: Store<State>,
    private dialog: MatDialog,
    private router: Router,
    private ng2ImgMax: Ng2ImgMaxService
  ) { }

  ngOnInit(): void {
    this.categories$ = this.store.pipe(select(getAllCategories));
    this.store.dispatch(new GetAllCategoriesAction());
    this.initForm();
  }

  private initForm(): void {
    this.createGroup = this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      video: ['']
    });
  }

  private readFile(file: File): void {
    this.loadImage = true;
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.loadImage = false;
      this.selectedFile = reader.result;
    });
    this.ng2ImgMax.resizeImage(file, 300, 10000)
      .pipe(take(1))
      .subscribe(result => {
        reader.readAsDataURL(result);
      });
  }

  onFileChanged(event): void {
    const file = event.target.files[0];
    this.readFile(file);
  }

  openInfoDialog(infoType: string): void {
    const dialogRef = this.dialog.open(InfoDialogComponent, { data: infoType });
    dialogRef.afterClosed().pipe(take(1)).subscribe();
  }

  createCategory(): void {
    const dialogRef = this.dialog.open(CategoryCreateDialogComponent);
    dialogRef.afterClosed().pipe(take(1)).subscribe();
  }

  createSubCategory(): void {
    const dialogRef = this.dialog.open(SubCategoryCreateDialogComponent, { data: this.selectedCategory });
    dialogRef.afterClosed().pipe(take(1)).subscribe();
  }

  submitService(): void {
    const data: ServiceInput = {
      ...this.createGroup.value,
      video: this.createGroup.get('video').value.split('=')[1],
      picture: this.selectedFile
    };
    this.createGroup.reset();
    this.store.dispatch(new CreateServiceAction(data));
    this.router.navigate(['../', 'dashboard']);
  }

  reset(): void {
    this.createGroup.reset();
  }
}
