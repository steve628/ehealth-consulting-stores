import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Service } from '@app-graphql/generated/graphql';
import { ActivatedRoute, Router } from '@angular/router';
import { map, take } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { getAllServices, State } from '@app-store/index';
import { GetAllServicesAction } from '@app-store/service/actions/service.actions';

@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.scss']
})
export class SubCategoryComponent implements OnInit {
  services$: Observable<Service[]>;

  constructor(
    private store: Store<State>,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.services$ = this.store.pipe(select(getAllServices));
    this.route.paramMap.pipe(
      map(params => params.get('id')),
      take(1)
    ).subscribe((id: string) => this.store.dispatch(new GetAllServicesAction({
      where: { subCategory: {id} }
    })));
  }

  routeToService(id: string): void {
    this.router.navigate(['../../', 'service', id]);
  }
}
