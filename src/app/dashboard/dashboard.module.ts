import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { HomeComponent } from './home/home.component';
import { CategoryComponent } from './category/category.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { ServiceComponent } from './service/service.component';
import { TopicComponent } from './topic/topic.component';
import { CategoryCardComponent } from './category-card/category-card.component';
import { MaterialModule } from '@app-material/material.module';
import { SubCategoryCardComponent } from './sub-category-card/sub-category-card.component';
import { ServiceCardComponent } from './service-card/service-card.component';
import { CreateServiceComponent } from './create-service/create-service.component';
import { EditServiceComponent } from './edit-service/edit-service.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CategoryCreateDialogComponent } from './category-create-dialog/category-create-dialog.component';
import { SubCategoryCreateDialogComponent } from './sub-category-create-dialog/sub-category-create-dialog.component';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { OwnerCardComponent } from './owner-card/owner-card.component';
import { CallFormComponent } from './call-form/call-form.component';
import { CoreModule } from '@app-core/core.module';


@NgModule({
  declarations: [
    HomeComponent,
    CategoryComponent,
    SubCategoryComponent,
    ServiceComponent,
    TopicComponent,
    CategoryCardComponent,
    SubCategoryCardComponent,
    ServiceCardComponent,
    CreateServiceComponent,
    EditServiceComponent,
    CategoryCreateDialogComponent,
    SubCategoryCreateDialogComponent,
    OwnerCardComponent,
    CallFormComponent
  ],
  exports: [
    ServiceCardComponent
  ],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        MaterialModule,
        ReactiveFormsModule,
        YouTubePlayerModule,
        CoreModule
    ]
})
export class DashboardModule { }
