import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Category, Service, User } from '@app-graphql/generated/graphql';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { getAllCategories, getAllServices, getUser, State } from '@app-store/index';
import { GetAllCategoriesAction } from '@app-store/category/actions/category.actions';
import { GetAllServicesAction } from '@app-store/service/actions/service.actions';
import {UpdateUserSettingsAction} from '@app-store/user/actions/user.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  categories$: Observable<Category[]>;
  services$: Observable<Service[]>;
  user$: Observable<User>;

  constructor(
    private store: Store<State>,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.categories$ = this.store.pipe(select(getAllCategories));
    this.services$ = this.store.pipe(select(getAllServices));
    this.user$ = this.store.pipe(select(getUser));
    this.store.dispatch(new GetAllCategoriesAction());
    this.store.dispatch(new GetAllServicesAction());
  }

  addFavorite(serviceId: string): void {
    this.store.dispatch(new UpdateUserSettingsAction({
      favoriteServices: { connect: [{ id: serviceId }] }
    }));
  }

  removeFavorite(serviceId: string): void {
    this.store.dispatch(new UpdateUserSettingsAction({
      favoriteServices: { disconnect: [{ id: serviceId }] }
    }));
  }

  routeToService(id: string): void {
    this.router.navigate(['../', 'service', id]);
  }
}
