import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CategoryComponent } from './category/category.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { ServiceComponent } from './service/service.component';
import { CreateServiceComponent } from './create-service/create-service.component';
import { EditServiceComponent } from './edit-service/edit-service.component';
import { CallFormComponent } from './call-form/call-form.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: HomeComponent
  },
  {
    path: 'category/:id',
    component: CategoryComponent
  },
  {
    path: 'subCategory/:id',
    component: SubCategoryComponent
  },
  {
    path: 'service/:id',
    component: ServiceComponent
  },
  {
    path: 'create-service',
    component: CreateServiceComponent
  },
  {
    path: 'edit-service/:id',
    component: EditServiceComponent
  },
  {
    path: 'call-form/:id',
    component: CallFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
