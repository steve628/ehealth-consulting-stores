import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Service, User } from '@app-graphql/generated/graphql';

@Component({
  selector: 'app-service-card',
  templateUrl: './service-card.component.html',
  styleUrls: ['./service-card.component.scss']
})
export class ServiceCardComponent implements OnInit {
  @Input() service: Service;
  @Input() user: User;
  // @Input() owner: boolean;
  @Output() selectFavorite = new EventEmitter<string>();
  @Output() deSelectFavorite = new EventEmitter<string>();
  @Output() editService = new EventEmitter<string>();
  @Output() deleteService = new EventEmitter<string>();
  @Output() routeTo = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  addFavorite(): void {
    this.selectFavorite.emit(this.service.id);
  }

  removeFavorite(): void {
    this.deSelectFavorite.emit(this.service.id);
  }

  isFavorite(): Service | undefined {
    return this.user.favoriteServices ? this.user.favoriteServices.find(e => e.id === this.service.id) : undefined;
  }

  edit(): void {
    this.editService.emit(this.service.id);
  }

  delete(): void {
    this.deleteService.emit(this.service.id);
  }

  routeToId(): void {
    this.routeTo.emit(this.service.id);
  }

  isOwner(): boolean {
    return this.user ? this.user.id === this.service.owner.id : false;
  }
}
