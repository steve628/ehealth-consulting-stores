import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Service } from '@app-graphql/generated/graphql';
import { select, Store } from '@ngrx/store';
import { getService, getUser, State } from '@app-store/index';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GetServiceAction } from '@app-store/service/actions/service.actions';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-call-form',
  templateUrl: './call-form.component.html',
  styleUrls: ['./call-form.component.scss']
})
export class CallFormComponent implements OnInit, OnDestroy {
  service$: Observable<Service>;
  callForm: FormGroup;
  private sub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private store: Store<State>,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.service$ = this.route.paramMap.pipe(
      map(params => params.get('id')),
      switchMap((id: string) => {
        this.store.dispatch(new GetServiceAction(id));
        return this.store.pipe(select(getService));
      })
    );
    this.initForm();
    this.pathForm();
  }

  ngOnDestroy(): void {
    if (this.sub && !this.sub.closed) {
      this.sub.unsubscribe();
    }
  }

  private initForm(): void {
    this.callForm = this.fb.group({
      message: ['', Validators.required],
      callTime: ['', Validators.required],
      phoneNumber: ['', Validators.required]
      // to do -> add suggest times (maybe as an array)
    });
  }

  private pathForm(): void {
    this.sub = this.store.pipe(select(getUser)).subscribe(res => {
      // to do -> patch form with user data (like phoneNumber, forename, etc.)
    });
  }
}
