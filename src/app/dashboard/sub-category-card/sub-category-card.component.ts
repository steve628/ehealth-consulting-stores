import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { SubCategory } from '@app-graphql/generated/graphql';

@Component({
  selector: 'app-sub-category-card',
  templateUrl: './sub-category-card.component.html',
  styleUrls: ['./sub-category-card.component.scss']
})
export class SubCategoryCardComponent implements OnInit {
  @Input() subCategory: SubCategory;

  constructor() { }

  ngOnInit(): void {
  }
}
