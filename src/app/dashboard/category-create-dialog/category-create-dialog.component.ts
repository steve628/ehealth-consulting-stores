import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { State } from '@app-store/index';
import { CreateCategoryAction } from '@app-store/category/actions/category.actions';

@Component({
  selector: 'app-category-create-dialog',
  templateUrl: './category-create-dialog.component.html',
  styleUrls: ['./category-create-dialog.component.scss']
})
export class CategoryCreateDialogComponent implements OnInit {
  categoryForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<State>,
    private dataRef: MatDialogRef<CategoryCreateDialogComponent>
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.categoryForm = this.fb.group({
      name: ['', Validators.required]
    });
  }

  submitForm(): void {
    const name = this.categoryForm.get('name').value;
    this.store.dispatch(new CreateCategoryAction(name));
    this.categoryForm.reset();
    this.dataRef.close();
  }
}
