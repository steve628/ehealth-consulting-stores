import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Service, User } from '@app-graphql/generated/graphql';

@Component({
  selector: 'app-owner-card',
  templateUrl: './owner-card.component.html',
  styleUrls: ['./owner-card.component.scss']
})
export class OwnerCardComponent implements OnInit {
  @Input() service: Service;
  @Input() user: User;
  @Output() selectFavorite = new EventEmitter<string>();
  @Output() deSelectFavorite = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  addFavorite(): void {
    this.selectFavorite.emit(this.service.id);
  }

  removeFavorite(): void {
    this.deSelectFavorite.emit(this.service.id);
  }

  isFavorite(): Service | undefined {
    return this.user.favoriteServices ? this.user.favoriteServices.find(e => e.id === this.service.id) : undefined;
  }

  isOwner(): boolean {
    return this.user ? this.user.id === this.service.owner.id : false;
  }
}
