import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-help-navigation',
  templateUrl: './help-navigation.component.html',
  styleUrls: ['./help-navigation.component.scss']
})
export class HelpNavigationComponent implements OnInit {
  @Output() itemClicked = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  clickItem(): void {
    this.itemClicked.emit();
  }
}
