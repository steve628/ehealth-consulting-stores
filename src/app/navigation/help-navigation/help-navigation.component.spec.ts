import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpNavigationComponent } from './help-navigation.component';

describe('HelpNavigationComponent', () => {
  let component: HelpNavigationComponent;
  let fixture: ComponentFixture<HelpNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
