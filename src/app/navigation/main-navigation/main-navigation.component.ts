import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ThemeService, ThemeStyle } from '@app-core/services/theme.service';
import { User } from '@app-graphql/generated/graphql';
import { select, Store } from '@ngrx/store';
import { getUser, State } from '@app-store/index';
import { UserGqlService } from '@app-graphql/services/user-gql.service';
import { LogoutUserAction } from '@app-store/user/actions/user.actions';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.css']
})
export class MainNavigationComponent implements OnInit {
  user$: Observable<User>;

  constructor(
    private themeService: ThemeService,
    private store: Store<State>,
    private userService: UserGqlService
  ) { }

  ngOnInit(): void {
    this.user$ = this.store.pipe(select(getUser));
  }

  setTheme(theme: ThemeStyle): void {
    this.themeService.setTheme(theme);
  }

  logout(): void {
    this.store.dispatch(new LogoutUserAction());
    this.userService.logoutUser();
  }
}
