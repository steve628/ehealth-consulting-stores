import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '@app-graphql/generated/graphql';

@Component({
  selector: 'app-dashboard-navigation',
  templateUrl: './dashboard-navigation.component.html',
  styleUrls: ['./dashboard-navigation.component.scss']
})
export class DashboardNavigationComponent implements OnInit {
  @Input() user: User;
  @Output() itemClicked = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  clickItem(): void {
    this.itemClicked.emit();
  }
}
