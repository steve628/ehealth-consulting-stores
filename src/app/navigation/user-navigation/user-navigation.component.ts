import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '@app-graphql/generated/graphql';

@Component({
  selector: 'app-user-navigation',
  templateUrl: './user-navigation.component.html',
  styleUrls: ['./user-navigation.component.scss']
})
export class UserNavigationComponent implements OnInit {
  @Input() user: User;
  @Output() itemClicked = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  clickItem(): void {
    this.itemClicked.emit();
  }
}
