import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainNavigationComponent } from './main-navigation/main-navigation.component';
import { MaterialModule } from '../material/material.module';
import { ExtendedModule, FlexModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { UserNavigationComponent } from './user-navigation/user-navigation.component';
import { HelpNavigationComponent } from './help-navigation/help-navigation.component';
import { DashboardNavigationComponent } from './dashboard-navigation/dashboard-navigation.component';



@NgModule({
  declarations: [
    MainNavigationComponent,
    UserNavigationComponent,
    HelpNavigationComponent,
    DashboardNavigationComponent
  ],
  exports: [
    MainNavigationComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ExtendedModule,
    FlexModule,
    RouterModule
  ]
})
export class NavigationModule { }
