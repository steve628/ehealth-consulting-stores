import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { MaterialModule } from '../material/material.module';
import { LoadingComponent } from '@app-core/components/loading/loading.component';


@NgModule({
  declarations: [InfoDialogComponent, LoadingComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    InfoDialogComponent,
    LoadingComponent
  ]
})
export class CoreModule { }
