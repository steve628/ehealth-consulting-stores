import { FormGroup, ValidationErrors } from '@angular/forms';

export class PasswordValidator {
  static passwordEquality(controlGroup: FormGroup): ValidationErrors | null {
    const pwd1 = controlGroup.get('password').value;
    const pwd2 = controlGroup.get('passwordRepeat').value;

    return pwd1 === pwd2 ? null : { passwordEquality: { valid: false } };
  }
}
