import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export enum ThemeStyle {
  LIGHT,
  DARK
}

// Code is based on: https://medium.com/grensesnittet/dynamic-themes-in-angular-material-b6dc0c88dfd7
@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private darkTheme = new Subject<ThemeStyle>();
  currentTheme = this.darkTheme.asObservable();

  constructor() { }

  setTheme(theme: ThemeStyle): void {
    this.darkTheme.next(theme);
  }
}
