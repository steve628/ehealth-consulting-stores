import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

export interface Question {
  questionTitle: string;
  answerTitle: string;
  answerText: string;
}

@Injectable({
  providedIn: 'root'
})
export class FaqService {
  private questions: Question[] = [
    {
      questionTitle: 'Frage 1',
      answerTitle: 'Antwort 1',
      answerText: 'Anworttext auf Frage 1'
    },
    {
      questionTitle: 'Frage 2',
      answerTitle: 'Antwort 2',
      answerText: 'Antworttext auf Frage 2'
    },
    {
      questionTitle: 'Frage 3',
      answerTitle: 'Antwort 3',
      answerText: 'Antworttext auf Frage 3'
    }
  ];

  constructor() { }

  get Faq(): Observable<Question[]> {
    return of(this.questions);
  }
}
