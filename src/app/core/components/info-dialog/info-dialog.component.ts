import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-info-dialog',
  templateUrl: './info-dialog.component.html',
  styleUrls: ['./info-dialog.component.scss']
})
export class InfoDialogComponent implements OnInit {
  infoMessages = {
    'service-title': `Geben Sie Ihrer Dienstleistung einen aussagefähigen Titel, um das Interesse potentieller KundInnen zu wecken.`,
    'service-description': `Beschreiben Sie Ihre Dienstleistung ausführlich, um potentiellen KundInnen ein besseres Verständnis führ
     diese zu geben. Zusätzlich kann eine Beschreibung die Entscheidung zur Auswahl einer Dienstleistung, bei InteressentInnen,
     entscheidend verbessern.`,
    'service-photo': `Fügen Sie ein Titelbild für Ihre Dienstleistung hinzu, um die Aufmerksamkeit potentiellen KundInnen zu erlangen.`,
    'service-video': `Erstellen Sie ein YouTube-Video und beschreiben Sie sowohl Ihre Dienstleistung, als auch Ihre eigenen Kompetenzen
     und Erfahrungen in jenem Bereich. Fügen Sie die vollständige URL zu Ihrem YouTube-Video in das vorgesehene Feld ein,
      z.B. https://www.youtube.com/watch?v=kopoLzvh5jY`,
    'service-category': `Wählen Sie eine passende Oberkategorie aus, die Ihrer Dienstleistung entspricht. Anschließend wählen Sie
     eine Unterkategorie. Sollte keine passende Oberkategorie vorhanden sein, die Ihrer Dienstleistung zugeordnet werden kann, klicken
      Sie auf “Neue Kategorie erstellen“ und fügen Sie eine neue Oberkategorie, anhand eines Titels, hinzu.`,
    'service-subcategory': `Wählen Sie eine passende Unterkategorie aus, die Ihrer Dienstleistung entspricht. Sollte keine passende
     Unterkategorie vorhanden sein, die Ihrer Dienstleistung zugeordnet werden kann, klicken Sie auf “Neue Kategorie erstellen“ und
      fügen Sie eine neue Unterkategorie, anhand eines Titels, hinzu.`,
    'settings-email': `Geben Sie eine E-Mail Adresse an, unter der Ihre KundInnen mit Ihnen über Ihre Dienstleistung kommunizieren
     können.`,
    'settings-forename': 'Geben Sie Ihren Vornamen an.',
    'settings-lastname': 'Geben Sie Ihren Nachnamen an.',
    'settings-photo': `Fügen Sie Ihrem Profil ein Foto hinzu. Für einen besseren Eindruck bei KundInnen sollten Sie ein seriöses Foto
     von sich selbst verwenden.`,
    'settings-price': `Geben Sie einen Basispreis führ Ihre Dienstleistungen an. Dieser wird mit dem Mittelwert Ihrer Bewertungen
     verrechnet, zur Generierung des Endpreises.`,
    'settings-phoneNumber': `Geben Sie eine dienstliche Telefonnummer an, unter der Ihre KundInnen sie für Beratungen, Fragen und
     andere dienstleistungsorientierte Telefonate erreichen können.`,
    'settings-description': `Geben Sie ein kurzes Exposé über Ihre bisher erworbenen Kompetenzen, sowie Ihre Erfahrungen in
     beruflichen Bereichen.`
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public infoType: string
  ) { }

  ngOnInit(): void {
  }

}
