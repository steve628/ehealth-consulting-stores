import { MatTabGroup } from '@angular/material/tabs';

export class Gesture {
  static onTabGroupSwipe(tabGroup: MatTabGroup, event): number {
    const direction = event ? (Math.abs(event.deltaX) > 40 ? (event.deltaX > 0 ? 'right' : 'left') : '') : '';

    switch (direction) {
      case 'left':
        return  tabGroup.selectedIndex < tabGroup._tabs.length - 1 ? tabGroup.selectedIndex + 1 : 0;
      case 'right':
        return  tabGroup.selectedIndex >= 1 ? tabGroup.selectedIndex - 1 : tabGroup._tabs.length - 1;
      default:
        return tabGroup.selectedIndex;
    }
  }
}
