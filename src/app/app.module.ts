import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig, HammerModule } from '@angular/platform-browser';
import * as Hammer from 'hammerjs';
import {Injectable, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphqlModule } from './graphql/graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '@app-env/environment';
import { HttpLinkModule } from 'apollo-angular-link-http';
import { LoginModule } from './login/login.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { NavigationModule } from './navigation/navigation.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserAreaModule } from './user-area/user-area.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { UserModule } from '@app-store/user/user.module';
import { IosInstallComponent } from './ios-install/ios-install.component';
import { MaterialModule } from '@app-material/material.module';
import { CategoryModule } from '@app-store/category/category.module';
import { ServiceModule } from '@app-store/service/service.module';
import { HelpModule } from './help/help.module';
import { CoreModule } from '@app-core/core.module';

@Injectable()
export class MyHammerConfig extends HammerGestureConfig {
  overrides = {
    swipe: { direction: Hammer.DIRECTION_HORIZONTAL },
  } as any;
}

@NgModule({
    declarations: [
        AppComponent,
        IosInstallComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        GraphqlModule,
        HttpClientModule,
        HttpLinkModule,
        BrowserAnimationsModule,
        HammerModule,
        CoreModule,
        LoginModule,
        DashboardModule,
        NavigationModule,
        UserAreaModule,
        UserModule,
        CategoryModule,
        ServiceModule,
        MaterialModule,
        HelpModule,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        StoreModule.forRoot({}, {}),
        EffectsModule.forRoot([]),
        StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production})
    ],
    providers: [
        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: HammerGestureConfig
        }
    ],
    bootstrap: [AppComponent],
    entryComponents: [IosInstallComponent]
})
export class AppModule { }
