import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserAreaRoutingModule } from './user-area-routing.module';
import { SettingComponent } from './setting/setting.component';
import { MaterialModule } from '@app-material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ErrorModule } from '@app-error/error.module';
import { ConsultantSettingsComponent } from './consultant-settings/consultant-settings.component';
import { CustomerSettingsComponent } from './customer-settings/customer-settings.component';
import { MyServicesComponent } from './my-services/my-services.component';
import { MyFavoritesComponent } from './my-favorites/my-favorites.component';

import { Ng2ImgMaxModule } from 'ng2-img-max';
import { DashboardModule } from '../dashboard/dashboard.module';
import { CoreModule } from '@app-core/core.module';


@NgModule({
  declarations: [
    SettingComponent,
    ConsultantSettingsComponent,
    CustomerSettingsComponent,
    MyServicesComponent,
    MyFavoritesComponent
  ],
    imports: [
        CommonModule,
        UserAreaRoutingModule,
        MaterialModule,
        ReactiveFormsModule,
        ErrorModule,
        Ng2ImgMaxModule,
        DashboardModule,
        CoreModule
    ]
})
export class UserAreaModule { }
