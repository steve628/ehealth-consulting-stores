import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { getUser, State } from '@app-store/index';
import { Observable } from 'rxjs';
import { User } from '@app-graphql/generated/graphql';
import { UpdateUserSettingsAction } from '@app-store/user/actions/user.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-favorites',
  templateUrl: './my-favorites.component.html',
  styleUrls: ['./my-favorites.component.scss']
})
export class MyFavoritesComponent implements OnInit {
  user$: Observable<User>;

  constructor(
    private store: Store<State>,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.user$ = this.store.pipe(select(getUser));
  }

  removeFavorite(serviceId: string): void {
    this.store.dispatch(new UpdateUserSettingsAction({
      favoriteServices: { disconnect: [{ id: serviceId }] }
    }));
  }

  routeToService(id: string): void {
    this.router.navigate(['../', 'service', id]);
  }
}
