import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidatorService } from '@app-graphql/services/email-validator.service';
import { select, Store } from '@ngrx/store';
import { getUser, State } from '@app-store/index';
import { Subscription } from 'rxjs';
import { UserUpdateInput } from '@app-graphql/generated/graphql';
import { UpdateUserSettingsAction } from '@app-store/user/actions/user.actions';
import { PasswordValidator } from '@app-core/validators/password-validator';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { take } from 'rxjs/operators';
import { MatTabGroup } from '@angular/material/tabs';
import { Gesture } from '@app-core/classes/gesture';
import { MatDialog } from '@angular/material/dialog';
import { InfoDialogComponent } from '@app-core/components/info-dialog/info-dialog.component';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit, OnDestroy {
  basicSettingsForm: FormGroup;
  extendedSettingsForm: FormGroup;
  passwordForm: FormGroup;
  isCustomer = true;
  profileImage: string;
  loadImage = false;
  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;
  private sub: Subscription;
  private selectedFile: string | ArrayBuffer;

  constructor(
    private fb: FormBuilder,
    private evs: EmailValidatorService,
    private store: Store<State>,
    private ng2ImgMax: Ng2ImgMaxService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.initBasicForm();
    this.initExtendedForm();
    this.initPasswordForm();
    this.sub = this.store.pipe(select(getUser)).subscribe(res => {
      if (res) {
        this.basicSettingsForm.patchValue(res);
        this.extendedSettingsForm.patchValue(res);
        this.isCustomer = res.role === 'USER';
        this.profileImage = res.picture;
      }
    });
  }

  ngOnDestroy(): void {
    if (this.sub && !this.sub.closed) {
      this.sub.unsubscribe();
    }
    this.basicSettingsForm.reset();
    this.extendedSettingsForm.reset();
    this.passwordForm.reset();
  }

  private initBasicForm(): void {
    this.basicSettingsForm = this.fb.group({
      email: ['', [Validators.email], [this.evs]],
      forename: [''],
      lastname: ['']
    });
  }

  private initExtendedForm(): void {
    this.extendedSettingsForm = this.fb.group({
      role: [''],
      picture: [''],
      price: [''],
      phoneNumber: [''],
      description: ['']
    });
  }

  private initPasswordForm(): void {
    this.passwordForm = this.fb.group({
      passwords: this.fb.group({
        password: ['', Validators.minLength(5)],
        passwordRepeat: ''
      }, { validators: PasswordValidator.passwordEquality })
    });
  }

  readFile(file: File): void {
    this.loadImage = true;
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.loadImage = false;
      this.selectedFile = reader.result;
      this.submitSettings({ picture: this.selectedFile as string });
    });
    this.ng2ImgMax.resizeImage(file, 150, 10000)
      .pipe(take(1))
      .subscribe(result => {
        reader.readAsDataURL(result);
      });
  }

  // just used to check image dimension
  /* private readImage(result): void {
    const image = new Image();
    image.addEventListener('load', () => {
      console.log(`Width: ${image.width}, Height: ${image.height}`);
    });
    image.src = result;
  } */

  openInfoDialog(infoType: string): void {
    const dialogRef = this.dialog.open(InfoDialogComponent, { data: infoType });
    dialogRef.afterClosed().pipe(take(1)).subscribe();
  }

  onSwipe(event?): void {
    this.tabGroup.selectedIndex = Gesture.onTabGroupSwipe(this.tabGroup, event);
  }

  submitBasicSettings(): void {
    this.submitSettings({ ...this.basicSettingsForm.value });
  }

  submitExtendedSettings(extended: FormGroup): void {
    this.submitSettings({ ...extended.value });
  }

  submitPasswordSettings(): void {
    this.submitSettings({ password: this.passwordForm.get('passwords').get('password').value });
  }

  private submitSettings(update: UserUpdateInput): void {
    this.store.dispatch(new UpdateUserSettingsAction(update));
  }
}
