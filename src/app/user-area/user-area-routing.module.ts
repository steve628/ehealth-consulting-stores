import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingComponent } from './setting/setting.component';
import { MyFavoritesComponent } from './my-favorites/my-favorites.component';
import { MyServicesComponent } from './my-services/my-services.component';

const routes: Routes = [
  {
    path: 'settings',
    component: SettingComponent
  },
  {
    path: 'my-favorites',
    component: MyFavoritesComponent
  },
  {
    path: 'my-services',
    component: MyServicesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserAreaRoutingModule { }
