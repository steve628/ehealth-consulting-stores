import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '@app-graphql/generated/graphql';
import { select, Store } from '@ngrx/store';
import { getUser, State } from '@app-store/index';
import { GetMyServicesAction, UpdateUserSettingsAction } from '@app-store/user/actions/user.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-services',
  templateUrl: './my-services.component.html',
  styleUrls: ['./my-services.component.scss']
})
export class MyServicesComponent implements OnInit {
  user$: Observable<User>;

  constructor(
    private store: Store<State>,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.user$ = this.store.pipe(select(getUser));
    this.store.dispatch(new GetMyServicesAction());
  }

  editService(id: string): void {
    this.router.navigate(['../', 'edit-service', id]);
  }

  deleteService(id: string): void {
    this.store.dispatch(new UpdateUserSettingsAction(
      { services: { delete: [{ id }] } }
    ));
  }

  routeToService(id: string): void {
    this.router.navigate(['../', 'service', id]);
  }
}
