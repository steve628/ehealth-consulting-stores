import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-customer-settings',
  templateUrl: './customer-settings.component.html',
  styleUrls: ['./customer-settings.component.scss']
})
export class CustomerSettingsComponent implements OnInit {
  @Input() extendedForm: FormGroup;
  @Input() profileImage: string;
  @Input() loading = false;
  @Output() setConsultant = new EventEmitter<boolean>();
  @Output() submitForm = new EventEmitter<FormGroup>();
  @Output() setImage = new EventEmitter<File>();
  @Output() swipeCard = new EventEmitter();
  @Output() info = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  submitExtendedSettings(): void {
    this.extendedForm.patchValue({ role: 'USER' });
    this.submitForm.emit(this.extendedForm);
  }

  linkToConsultant(): void {
    this.setConsultant.emit(false);
  }

  onFileChanged(event): void {
    const file = event.target.files[0];
    this.setImage.emit(file);
  }

  onSwipe(): void {
    this.swipeCard.emit();
  }

  onInfoClicked(infoType: string): void {
    this.info.emit(infoType);
  }
}
