import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultantSettingsComponent } from './consultant-settings.component';

describe('ConsultantSettingsComponent', () => {
  let component: ConsultantSettingsComponent;
  let fixture: ComponentFixture<ConsultantSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultantSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultantSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
