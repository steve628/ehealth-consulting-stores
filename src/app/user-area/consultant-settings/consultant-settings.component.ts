import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-consultant-settings',
  templateUrl: './consultant-settings.component.html',
  styleUrls: ['./consultant-settings.component.scss']
})
export class ConsultantSettingsComponent implements OnInit {
  @Input() extendedForm: FormGroup;
  @Input() profileImage: string;
  @Input() loading = false;
  @Output() submitForm = new EventEmitter<FormGroup>();
  @Output() setImage = new EventEmitter<File>();
  @Output() swipeCard = new EventEmitter();
  @Output() info = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  submitExtendedSettings(): void {
    this.extendedForm.patchValue({ role: 'CONSULTANT' });
    this.submitForm.emit(this.extendedForm);
  }

  onFileChanged(event): void {
    const file = event.target.files[0];
    this.setImage.emit(file);
  }

  onSwipe(): void {
    this.swipeCard.emit();
  }

  onInfoClicked(infoType: string): void {
    this.info.emit(infoType);
  }
}
