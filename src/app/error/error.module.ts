import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormErrorMessagesComponent } from './form-error-messages/form-error-messages.component';



@NgModule({
    declarations: [FormErrorMessagesComponent],
    exports: [
        FormErrorMessagesComponent
    ],
    imports: [
        CommonModule
    ]
})
export class ErrorModule { }
