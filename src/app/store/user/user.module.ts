import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './effects/user.effects';
import { StoreModule } from '@ngrx/store';
import { userReducer } from './reducers/user.reducer';
import { userFeatureStateName } from '../index';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(userFeatureStateName, userReducer),
    EffectsModule.forFeature([UserEffects])
  ]
})
export class UserModule { }
