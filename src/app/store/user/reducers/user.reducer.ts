import {AuthPayload, Service, User} from '@app-graphql/generated/graphql';
import {UserActions, UserActionTypes} from '../actions/user.actions';

export interface State extends AuthPayload {
  error;
  loading;
}

export const initialState: State = {
  token: null,
  user: null,
  error: null,
  loading: false
};

export function userReducer(
  state = initialState,
  action: UserActions
): State {
  switch (action.type) {
    case UserActionTypes.LoginUser: {
      return {
        ...state,
        error: null,
        loading: true
      };
    }

    case UserActionTypes.LoginUserFinished: {
      return {
        ...state,
        token: action.payload.data.login.token,
        user: action.payload.data.login.user as User,
        error: action.payload.errors,
        loading: false
      };
    }

    case UserActionTypes.LoginUserFailure: {
      return {
        ...state,
        error: action.payload.error,
        loading: false
      };
    }

    case UserActionTypes.UpdateUserSettings: {
      return {
        ...state,
        error: null,
        loading: true
      };
    }

    case UserActionTypes.UpdateUserSettingsFinished: {
      return {
        ...state,
        user: action.payload.data.updateUserSettings as User,
        error: action.payload.errors,
        loading: false
      };
    }

    case UserActionTypes.UpdateUserSettingsFailure: {
      return {
        ...state,
        error: action.payload.error,
        loading: false
      };
    }

    case UserActionTypes.LogoutUser: {
      return {
        ...state,
        error: null,
        loading: true
      };
    }

    case UserActionTypes.LogoutUserFinished: {
      return {
        ...state,
        token: null,
        user: null,
        error: null,
        loading: false
      };
    }

    case UserActionTypes.LogoutUserFailure: {
      return {
        ...state,
        error: action.payload.error,
        loading: false
      };
    }

    case UserActionTypes.GetFavoriteServices: {
      return {
        ...state,
        loading: true
      };
    }

    case UserActionTypes.GetFavoriteServicesFinished: {
      const myUser = {...state.user};
      myUser.favoriteServices = action.payload.data.user.favoriteServices as Service[];
      return {
        ...state,
        user: myUser,
        error: action.payload.errors,
        loading: false
      };
    }

    case UserActionTypes.GetFavoriteServicesFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    case UserActionTypes.GetMyServices: {
      return {
        ...state,
        loading: true
      };
    }

    case UserActionTypes.GetMyServicesFinished: {
      const myUser = {...state.user};
      myUser.services = action.payload.data.user.services as Service[];
      return {
        ...state,
        user: myUser,
        error: action.payload.errors,
        loading: false
      };
    }

    case UserActionTypes.GetMyServicesFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    default: return state;
  }
}
