import { Action } from '@ngrx/store';
import {
  AuthPayload,
  GetFavoriteServicesQuery,
  GetMyServicesQuery,
  LoginUserQuery,
  UpdateUserSettingsMutation,
  User,
  UserUpdateInput
} from '@app-graphql/generated/graphql';
import { HttpErrorResponse } from '@angular/common/http';
import { ServiceSearch } from '@app-graphql/services/service.service';
import { ApolloQueryResult } from 'apollo-client';
import { FetchResult } from 'apollo-link';

export enum UserActionTypes {
  LoginUser = '[User] Login User',
  LoginUserFinished = '[User] Login User Finished',
  LoginUserFailure = '[User] Login User Failure',
  UpdateUserSettings = '[User] Update User Settings',
  UpdateUserSettingsFinished = '[User] Update User Settings Finished',
  UpdateUserSettingsFailure = '[User] Update User Settings Failure',
  LogoutUser = '[User] Logout User',
  LogoutUserFinished = '[User] Logout User Finished',
  LogoutUserFailure = '[User] Logout User Failure',
  GetFavoriteServices = '[User] Get Favorite Services',
  GetFavoriteServicesFinished = '[User] Get Favorite Services Finished',
  GetFavoriteServicesFailure = '[User] Get Favorite Services Failure',
  GetMyServices = '[User] Get My Services',
  GetMyServicesFinished = '[User] Get My Services Finished',
  GetMyServicesFailure = '[User] Get My Services Failure'
}

export class LoginUserAction implements Action {
  readonly type = UserActionTypes.LoginUser;
  constructor(public email: string, public password: string) {}
}

export class LoginUserActionFinished implements Action {
  readonly type = UserActionTypes.LoginUserFinished;
  constructor(public payload: ApolloQueryResult<LoginUserQuery>) {}
  // constructor(public payload: AuthPayload) {}
}

export class LoginUserActionFailure implements Action {
  readonly type = UserActionTypes.LoginUserFailure;
  constructor(public payload: { error: HttpErrorResponse }) {}
}

export class UpdateUserSettingsAction implements Action {
  readonly type = UserActionTypes.UpdateUserSettings;
  constructor(public data: UserUpdateInput) {}
}

export class UpdateUserSettingsActionFinished implements Action {
  readonly type = UserActionTypes.UpdateUserSettingsFinished;
  constructor(public payload: FetchResult<UpdateUserSettingsMutation>) {}
  // constructor(public payload: User) {}
}

export class UpdateUserSettingsActionFailure implements Action {
  readonly type = UserActionTypes.UpdateUserSettingsFailure;
  constructor(public payload: { error: HttpErrorResponse }) {}
}

export class LogoutUserAction implements Action {
  readonly type = UserActionTypes.LogoutUser;
}

export class LogoutUserActionFinished implements Action {
  readonly type = UserActionTypes.LogoutUserFinished;
  constructor(public payload: AuthPayload) {}
}

export class LogoutUserActionFailure implements Action {
  readonly type = UserActionTypes.LogoutUserFailure;
  constructor(public payload: { error: HttpErrorResponse }) {}
}

export class GetFavoriteServiceAction implements Action  {
  readonly type = UserActionTypes.GetFavoriteServices;
  constructor(public search?: ServiceSearch) {}
}

export class GetFavoriteServiceActionFinished implements Action {
  readonly type = UserActionTypes.GetFavoriteServicesFinished;
  constructor(public payload: ApolloQueryResult<GetFavoriteServicesQuery>) {}
  // constructor(public payload: User) {}
}

export class GetFavoriteServiceActionFailure implements Action {
  readonly type = UserActionTypes.GetFavoriteServicesFailure;
  constructor(public payload: { error: HttpErrorResponse }) {}
}

export class GetMyServicesAction implements Action {
  readonly type = UserActionTypes.GetMyServices;
  constructor(public search?: ServiceSearch) {}
}

export class GetMyServicesActionFinished implements Action {
  readonly type = UserActionTypes.GetMyServicesFinished;
  constructor(public payload: ApolloQueryResult<GetMyServicesQuery>) {}
  // constructor(public payload: User) {}
}

export class GetMyServicesActionFailure implements Action {
  readonly type = UserActionTypes.GetMyServicesFailure;
  constructor(public payload: { error: HttpErrorResponse }) {}
}

export type UserActions =
  | LoginUserAction
  | LoginUserActionFinished
  | LoginUserActionFailure
  | UpdateUserSettingsAction
  | UpdateUserSettingsActionFinished
  | UpdateUserSettingsActionFailure
  | LogoutUserAction
  | LogoutUserActionFinished
  | LogoutUserActionFailure
  | GetFavoriteServiceAction
  | GetFavoriteServiceActionFinished
  | GetFavoriteServiceActionFailure
  | GetMyServicesAction
  | GetMyServicesActionFinished
  | GetMyServicesActionFailure;
