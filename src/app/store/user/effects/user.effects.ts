import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { UserGqlService } from '@app-graphql/services/user-gql.service';
import {
  GetFavoriteServiceAction,
  GetFavoriteServiceActionFailure,
  GetFavoriteServiceActionFinished,
  GetMyServicesAction, GetMyServicesActionFailure,
  GetMyServicesActionFinished,
  LoginUserAction,
  LoginUserActionFailure,
  LoginUserActionFinished,
  LogoutUserActionFailure,
  LogoutUserActionFinished,
  UpdateUserSettingsAction,
  UpdateUserSettingsActionFailure,
  UpdateUserSettingsActionFinished,
  UserActionTypes
} from '../actions/user.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import {
  AuthPayload,
  GetFavoriteServicesQuery,
  GetMyServicesQuery,
  LoginUserQuery,
  UpdateUserSettingsMutation,
  User
} from '@app-graphql/generated/graphql';
import { of } from 'rxjs';
import {ApolloQueryResult} from 'apollo-client';
import {FetchResult} from 'apollo-link';

@Injectable()
export class UserEffects {
  constructor(private actions$: Actions, private userService: UserGqlService) {}

  @Effect()
  loginUser$ = this.actions$.pipe(
    ofType(UserActionTypes.LoginUser),
    switchMap((action: LoginUserAction) =>
    this.userService
      .loginUser(action.email, action.password)
      .pipe(
        // map((payload: AuthPayload) => new LoginUserActionFinished(payload)),
        map((payload: ApolloQueryResult<LoginUserQuery>) => new LoginUserActionFinished(payload)),
        catchError(error => of(new LoginUserActionFailure(error)))
      )
    )
  );

  @Effect()
  updateSettings$ = this.actions$.pipe(
    ofType(UserActionTypes.UpdateUserSettings),
    switchMap((action: UpdateUserSettingsAction) =>
    this.userService
      .updateUserSettings(action.data)
      .pipe(
        // map((payload: User) => new UpdateUserSettingsActionFinished((payload))),
        map((payload: FetchResult<UpdateUserSettingsMutation>) => new UpdateUserSettingsActionFinished(payload)),
        catchError(error => of(new UpdateUserSettingsActionFailure(error)))
      )
    )
  );

  @Effect()
  getFavoriteServices$ = this.actions$.pipe(
    ofType(UserActionTypes.GetFavoriteServices),
    switchMap((action: GetFavoriteServiceAction) =>
      this.userService.getFavoriteServices(action.search)
        .pipe(
          // map((payload: User) => new GetFavoriteServiceActionFinished(payload))
          map((payload: ApolloQueryResult<GetFavoriteServicesQuery>) => new GetFavoriteServiceActionFinished(payload)),
          catchError(err => of(new GetFavoriteServiceActionFailure(err)))
        )
    )
  );

  @Effect()
  getMyServices$ = this.actions$.pipe(
    ofType(UserActionTypes.GetMyServices),
    switchMap((action: GetMyServicesAction) =>
      this.userService.getMyServices(action.search)
        .pipe(
          // map((payload: User) => new GetMyServicesActionFinished(payload))
          map((payload: ApolloQueryResult<GetMyServicesQuery>) => new GetMyServicesActionFinished(payload)),
          catchError(err => of(new GetMyServicesActionFailure(err)))
        )
    )
  );

  @Effect()
  logoutUser$ = this.actions$.pipe(
    ofType(UserActionTypes.LogoutUser),
    map((payload: AuthPayload) => new LogoutUserActionFinished(payload)),
    catchError(error => of(new LogoutUserActionFailure(error)))
  );
}
