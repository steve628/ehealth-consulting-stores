import { ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import { State as AuthPayloadState, userReducer } from './user/reducers/user.reducer';
import { State as CategoryState, categoryReducer } from '@app-store/category/reducers/category.reducer';
import { State as ServiceState, serviceReducer } from '@app-store/service/reducers/service.reducers';
import { environment } from '@app-env/environment';

export const userFeatureStateName = 'userFeature';
export const categoryFeatureStateName = 'categoryFeature';
export const serviceFeatureStateName = 'serviceFeature';

export interface State {
  user: AuthPayloadState;
  category: CategoryState;
  service: ServiceState;
}

export const reducers: ActionReducerMap<State> = {
  user: userReducer,
  category: categoryReducer,
  service: serviceReducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

export const getUserFeatureState = createFeatureSelector<AuthPayloadState>(userFeatureStateName);

export const getCategoryFeatureState = createFeatureSelector<CategoryState>(categoryFeatureStateName);

export const getServiceFeatureState = createFeatureSelector<ServiceState>(serviceFeatureStateName);

export const getUser = createSelector(
  getUserFeatureState,
  (state: AuthPayloadState) => state.user
);

export const getToken = createSelector(
  getUserFeatureState,
  (state: AuthPayloadState) => state
);

export const getAllCategories = createSelector(
  getCategoryFeatureState,
  (state: CategoryState) => state.categories
);

export const getCategory = createSelector(
  getCategoryFeatureState,
  (state: CategoryState) => state.category
);

export const getAllServices = createSelector(
  getServiceFeatureState,
  (state: ServiceState) => state.services
);

export const getService = createSelector(
  getServiceFeatureState,
  (state: ServiceState) => state.service
);
