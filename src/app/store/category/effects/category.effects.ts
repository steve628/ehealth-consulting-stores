import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { CategoryService } from '@app-graphql/services/category.service';
import {
  CategoryActionTypes,
  CreateCategoryAction,
  CreateCategoryActionFailure,
  CreateCategoryActionFinished,
  CreateSubCategoryAction,
  CreateSubCategoryActionFailure,
  CreateSubCategoryActionFinished,
  GetAllCategoriesAction,
  GetAllCategoriesActionFailure,
  GetAllCategoriesActionFinished,
  GetCategoryAction,
  GetCategoryActionFailure,
  GetCategoryActionFinished
} from '../actions/category.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import {
  CreateCategoryMutation,
  CreateSubCategoryMutation,
  GetAllCategoriesQuery,
  GetCategoryQuery
} from '@app-graphql/generated/graphql';
import { SubCategoryService } from '@app-graphql/services/sub-category.service';
import { ApolloQueryResult } from 'apollo-client';
import { of } from 'rxjs';
import { FetchResult } from 'apollo-link';

@Injectable()
export class CategoryEffects {
  constructor(
    private actions$: Actions,
    private categoryService: CategoryService,
    private subCategoryService: SubCategoryService
  ) {}

  @Effect()
  getAllCategories$ = this.actions$.pipe(
    ofType(CategoryActionTypes.GetAllCategories),
    switchMap((action: GetAllCategoriesAction) =>
      this.categoryService.getAllCategories(action.payload)
        .pipe(
          // map((payload: Category[]) => new GetAllCategoriesActionFinished(payload))
          map((payload: ApolloQueryResult<GetAllCategoriesQuery>) => new GetAllCategoriesActionFinished(payload)),
          catchError(error => of(new GetAllCategoriesActionFailure(error)))
        )
    )
  );

  @Effect()
  createCategory$ = this.actions$.pipe(
    ofType(CategoryActionTypes.CreateCategory),
    switchMap((action: CreateCategoryAction) =>
      this.categoryService.createCategory(action.name)
        .pipe(
          // map((payload: Category) => new CreateCategoryActionFinished(payload))
          map((payload: FetchResult<CreateCategoryMutation>) => new CreateCategoryActionFinished(payload)),
          catchError(error => of(new CreateCategoryActionFailure(error)))
        )
    )
  );

  @Effect()
  getCategory$ = this.actions$.pipe(
    ofType(CategoryActionTypes.GetCategory),
    switchMap((action: GetCategoryAction) =>
      this.categoryService.getCategory(action.id)
        .pipe(
          // map((payload: Category) => new GetCategoryActionFinished(payload))
          map((payload: ApolloQueryResult<GetCategoryQuery>) => new GetCategoryActionFinished(payload)),
          catchError(error => of(new GetCategoryActionFailure(error)))
        )
    )
  );

  @Effect()
  createSubCategory$ = this.actions$.pipe(
    ofType(CategoryActionTypes.CreateSubCategory),
    switchMap((action: CreateSubCategoryAction) =>
      this.subCategoryService.createSubCategory(action.name, action.categoryId)
        .pipe(
          // map((payload: SubCategory) => new CreateSubCategoryActionFinished(payload))
          map((payload: FetchResult<CreateSubCategoryMutation>) => new CreateSubCategoryActionFinished(payload)),
          catchError(error => of(new CreateSubCategoryActionFailure(error)))
        )
    )
  );
}
