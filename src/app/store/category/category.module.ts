import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { categoryFeatureStateName } from '../index';
import { categoryReducer } from './reducers/category.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CategoryEffects } from './effects/category.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(categoryFeatureStateName, categoryReducer),
    EffectsModule.forFeature([CategoryEffects])
  ]
})
export class CategoryModule { }
