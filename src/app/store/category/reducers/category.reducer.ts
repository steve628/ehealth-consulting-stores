import { Category } from '@app-graphql/generated/graphql';
import { CategoryActions, CategoryActionTypes } from '../actions/category.actions';
import * as cloneDeep from 'lodash/cloneDeep';
import { createEntityAdapter, EntityState } from '@ngrx/entity';

/* export interface State extends EntityState<Category>{
  loading: boolean;
  error;
} */

// export const adapter = createEntityAdapter<Category>();

/* export const initialState: State = adapter.getInitialState({
  loading: false,
  error: null
}); */

export interface State {
  categories: Category[];
  category: Category;
  loading: boolean;
  error: any;
}

export const initialState: State = {
  categories: null,
  category: null,
  loading: false,
  error: null
};

export function categoryReducer(
  state = initialState,
  action: CategoryActions
): State {
  switch (action.type) {
    case CategoryActionTypes.CreateCategory: {
      return {
        ...state,
        loading: true
      };
    }

    case CategoryActionTypes.CreateCategoryFinished: {
      const categoryArray = [...state.categories];
      categoryArray.push(action.payload.data.createCategory as Category);
      return {
        ...state,
        categories: categoryArray,
        loading: false
      };
    }

    case CategoryActionTypes.CreateCategoryFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    case CategoryActionTypes.GetAllCategories: {
      return {
        ...state,
        loading: true
      };
    }

    case CategoryActionTypes.GetAllCategoriesFinished: {
      return {
        ...state,
        categories: action.payload.data.categories as Category[],
        loading: false
      };
    }

    case CategoryActionTypes.GetAllCategoriesFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    case CategoryActionTypes.GetCategory: {
      return {
        ...state,
        loading: true
      };
    }

    case CategoryActionTypes.GetCategoryFinished: {
      return {
        ...state,
        category: action.payload.data.category as Category,
        loading: false
      };
    }

    case CategoryActionTypes.GetCategoryFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    case CategoryActionTypes.CreateSubCategory: {
      return {
        ...state,
        loading: true
      };
    }

    case CategoryActionTypes.CreateSubCategoryFinished: {
      const categoryArray = cloneDeep(state.categories);
      // const index = categoryArray.findIndex(elem => elem.id === action.payload.category.id);
      const index = categoryArray.findIndex(elem => elem.id === action.payload.data.createSubCategory.category.id);
      categoryArray[index].subCategories.push(action.payload.data.createSubCategory);
      return {
        ...state,
        categories: categoryArray,
        loading: false
      };
    }

    case CategoryActionTypes.CreateSubCategoryFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    default: return state;
  }
}

// export const { selectAll, selectEntities, selectIds, selectTotal } = adapter.getSelectors();
