import { Action } from '@ngrx/store';
import {
  CreateCategoryMutation,
  CreateSubCategoryMutation,
  GetAllCategoriesQuery,
  GetCategoryQuery,
} from '@app-graphql/generated/graphql';
import { CategorySearch } from '@app-graphql/services/category.service';
import { ApolloQueryResult } from 'apollo-client';
import { HttpErrorResponse } from '@angular/common/http';
import { FetchResult } from 'apollo-link';

export enum CategoryActionTypes {
  GetCategory = '[Category] Get Category',
  GetCategoryFinished = '[Category] Get Category Finished',
  GetCategoryFailure = '[Category] Get Category Failure',
  GetAllCategories = '[Category] Get all categories',
  GetAllCategoriesFinished = '[Category] Get all categories finished',
  GetAllCategoriesFailure = '[Category] Get all categories failure',
  CreateCategory = '[Category] Create a category',
  CreateCategoryFinished = '[Category] Create a category finished',
  CreateCategoryFailure = '[Category] Create a category failure',
  CreateSubCategory = '[SubCategory] Create subcategory',
  CreateSubCategoryFinished = '[SubCategory] Create subcategory finished',
  CreateSubCategoryFailure = '[SubCategory] Create subcategory failure'
}

export class GetCategoryAction implements Action {
  readonly type = CategoryActionTypes.GetCategory;
  constructor(public id: string) {}
}

export class GetCategoryActionFinished implements Action {
  readonly type = CategoryActionTypes.GetCategoryFinished;
  constructor(public payload: ApolloQueryResult<GetCategoryQuery>) {}
  // constructor(public payload: Category) {}
}

export class GetCategoryActionFailure implements Action {
  readonly type = CategoryActionTypes.GetCategoryFailure;
  constructor(public payload: {error: HttpErrorResponse}) {}
}

export class GetAllCategoriesAction implements Action {
  readonly type = CategoryActionTypes.GetAllCategories;
  constructor(public payload?: CategorySearch) {}
}

export class GetAllCategoriesActionFinished implements Action {
  readonly type = CategoryActionTypes.GetAllCategoriesFinished;
  constructor(public payload: ApolloQueryResult<GetAllCategoriesQuery>) {}
  // constructor(public payload: Category[]) {}
}

export class GetAllCategoriesActionFailure implements Action {
  readonly type = CategoryActionTypes.GetAllCategoriesFailure;
  constructor(public payload: {error: HttpErrorResponse}) {}
}

export class CreateCategoryAction implements Action {
  readonly type = CategoryActionTypes.CreateCategory;
  constructor(public name: string) {}
}

export class CreateCategoryActionFinished implements Action {
  readonly type = CategoryActionTypes.CreateCategoryFinished;
  constructor(public payload: FetchResult<CreateCategoryMutation>) {}
  // constructor(public payload: Category) {}
}

export class CreateCategoryActionFailure implements Action {
  readonly type = CategoryActionTypes.CreateCategoryFailure;
  constructor(public payload: {error: HttpErrorResponse}) {}
}

export class CreateSubCategoryAction implements Action {
  readonly type = CategoryActionTypes.CreateSubCategory;
  constructor(public name: string, public categoryId: string) {}
}

export class CreateSubCategoryActionFinished implements Action {
  readonly type = CategoryActionTypes.CreateSubCategoryFinished;
  constructor(public payload: FetchResult<CreateSubCategoryMutation>) {}
  // constructor(public payload: SubCategory) {}
}

export class CreateSubCategoryActionFailure implements Action {
  readonly type = CategoryActionTypes.CreateSubCategoryFailure;
  constructor(public payload: {error: HttpErrorResponse}) {}
}

export type CategoryActions =
  | GetCategoryAction
  | GetCategoryActionFinished
  | GetCategoryActionFailure
  | GetAllCategoriesAction
  | GetAllCategoriesActionFinished
  | GetAllCategoriesActionFailure
  | CreateCategoryAction
  | CreateCategoryActionFinished
  | CreateCategoryActionFailure
  | CreateSubCategoryAction
  | CreateSubCategoryActionFinished
  | CreateSubCategoryActionFailure;
