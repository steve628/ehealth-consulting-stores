import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { ServiceService } from '@app-graphql/services/service.service';
import {
  CreateServiceAction,
  CreateServiceActionFailure,
  CreateServiceActionFinished,
  GetAllServicesAction,
  GetAllServicesActionFailure,
  GetAllServicesActionFinished,
  GetServiceAction,
  GetServiceActionFailure,
  GetServiceActionFinished,
  ServiceActionTypes,
  UpdateServiceAction,
  UpdateServiceActionFailure,
  UpdateServiceActionFinished
} from '../actions/service.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import {
  CreateServiceMutation,
  GetAllServicesQuery,
  GetOneServiceQuery,
  UpdateServiceMutation
} from '@app-graphql/generated/graphql';
import { ApolloQueryResult } from 'apollo-client';
import { of } from 'rxjs';
import { FetchResult } from 'apollo-link';

@Injectable()
export class ServiceEffects {
  constructor(
    private actions$: Actions,
    private serviceService: ServiceService
  ) {}

  @Effect()
  getService$ = this.actions$.pipe(
    ofType(ServiceActionTypes.GetService),
    switchMap((action: GetServiceAction) =>
      this.serviceService.getService(action.id)
        .pipe(
          // map((payload: Service) => new GetServiceActionFinished(payload))
          map((payload: ApolloQueryResult<GetOneServiceQuery>) => new GetServiceActionFinished(payload)),
          catchError(error => of(new GetServiceActionFailure(error)))
        )
    )
  );

  @Effect()
  getAllServices$ = this.actions$.pipe(
    ofType(ServiceActionTypes.GetAllServices),
    switchMap((action: GetAllServicesAction) =>
      this.serviceService.getAllServices(action.payload)
        .pipe(
          // map((payload: Service[]) => new GetAllServicesActionFinished(payload))
          map((payload: ApolloQueryResult<GetAllServicesQuery>) => new GetAllServicesActionFinished(payload)),
          catchError(error => of(new GetAllServicesActionFailure(error)))
        )
    )
  );

  @Effect()
  createService$ = this.actions$.pipe(
    ofType(ServiceActionTypes.CreateService),
    switchMap((action: CreateServiceAction) =>
      this.serviceService.createService(action.payload)
        .pipe(
          // map((payload: Service) => new CreateServiceActionFinished(payload))
          map((payload: FetchResult<CreateServiceMutation>) => new CreateServiceActionFinished(payload)),
          catchError(error => of(new CreateServiceActionFailure(error)))
        )
    )
  );

  @Effect()
  updateService$ = this.actions$.pipe(
    ofType(ServiceActionTypes.UpdateService),
    switchMap((action: UpdateServiceAction) =>
      this.serviceService.updateService(action.data, action.serviceId)
        .pipe(
          // map((payload: Service) => new UpdateServiceActionFinished(payload))
          map((payload: FetchResult<UpdateServiceMutation>) => new UpdateServiceActionFinished(payload)),
          catchError(error => of(new UpdateServiceActionFailure(error)))
        )
    )
  );
}
