import { Action } from '@ngrx/store';
import {
  CreateServiceMutation,
  GetAllServicesQuery,
  GetOneServiceQuery,
  ServiceInput,
  ServiceUpdateInput,
  UpdateServiceMutation
} from '@app-graphql/generated/graphql';
import { ServiceSearch } from '@app-graphql/services/service.service';
import { ApolloQueryResult } from 'apollo-client';
import { HttpErrorResponse } from '@angular/common/http';
import { FetchResult } from 'apollo-link';

export enum ServiceActionTypes {
  GetService = '[Service] Get Service',
  GetServiceFinished = '[Service] Get Service Finished',
  GetServiceFailure = '[Service] Get Service Failure',
  GetAllServices = '[Service] Get All Services',
  GetAllServicesFinished = '[Service] Get All Services Finished',
  GetAllServicesFailure = '[Service] Get All Services Failure',
  CreateService = '[Service] Create Service',
  CreateServiceFinished = '[Service] Create Service Finished',
  CreateServiceFailure = '[Service] Create Service Failure',
  UpdateService = '[Service] Update Service',
  UpdateServiceFinished = '[Service] Update Service Finished',
  UpdateServiceFailure = '[Service] Update Service Failure'
}

export class GetServiceAction implements Action {
  readonly type = ServiceActionTypes.GetService;
  constructor(public id: string) {}
}

export class GetServiceActionFinished implements Action {
  readonly type = ServiceActionTypes.GetServiceFinished;
  constructor(public payload: ApolloQueryResult<GetOneServiceQuery>) {}
}

export class GetServiceActionFailure implements Action {
  readonly type = ServiceActionTypes.GetServiceFailure;
  constructor(public payload: {error: HttpErrorResponse}) {}
}

export class GetAllServicesAction implements Action {
  readonly type = ServiceActionTypes.GetAllServices;
  constructor(public payload?: ServiceSearch) {}
}

export class GetAllServicesActionFinished implements Action {
  readonly type = ServiceActionTypes.GetAllServicesFinished;
  constructor(public payload: ApolloQueryResult<GetAllServicesQuery>) {}
}

export class GetAllServicesActionFailure implements Action {
  readonly type = ServiceActionTypes.GetAllServicesFailure;
  constructor(public payload: {error: HttpErrorResponse}) {}
}

export class CreateServiceAction implements Action {
  readonly type = ServiceActionTypes.CreateService;
  constructor(public payload: ServiceInput) {}
}

export class CreateServiceActionFinished implements Action {
  readonly type = ServiceActionTypes.CreateServiceFinished;
  constructor(public payload: FetchResult<CreateServiceMutation>) {}
}

export class CreateServiceActionFailure implements Action {
  readonly type = ServiceActionTypes.CreateServiceFailure;
  constructor(public payload: {error: HttpErrorResponse}) {}
}

export class UpdateServiceAction implements Action {
  readonly type = ServiceActionTypes.UpdateService;
  constructor(public data: ServiceUpdateInput, public serviceId: string) {}
}

export class UpdateServiceActionFinished implements Action {
  readonly type = ServiceActionTypes.UpdateServiceFinished;
  constructor(public payload: FetchResult<UpdateServiceMutation>) {}
}

export class UpdateServiceActionFailure implements Action {
  readonly type = ServiceActionTypes.UpdateServiceFailure;
  constructor(public payload: {error: HttpErrorResponse}) {}
}

export type ServiceActions =
  | GetServiceAction
  | GetServiceActionFinished
  | GetServiceActionFailure
  | GetAllServicesAction
  | GetAllServicesActionFinished
  | GetAllServicesActionFailure
  | CreateServiceAction
  | CreateServiceActionFinished
  | CreateServiceActionFailure
  | UpdateServiceAction
  | UpdateServiceActionFinished
  | UpdateServiceActionFailure;
