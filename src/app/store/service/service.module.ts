import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { serviceFeatureStateName } from '../index';
import { serviceReducer } from './reducers/service.reducers';
import { ServiceEffects } from './effects/service.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(serviceFeatureStateName, serviceReducer),
    EffectsModule.forFeature([ServiceEffects])
  ]
})
export class ServiceModule { }
