import { Service } from '@app-graphql/generated/graphql';
import { ServiceActions, ServiceActionTypes } from '../actions/service.actions';

export interface State {
  services: Service[];
  service: Service;
  loading: boolean;
  error: any;
}

export const initialState: State = {
  services: null,
  service: null,
  loading: false,
  error: null
};

export function serviceReducer(
  state = initialState,
  action: ServiceActions
): State {
  switch (action.type) {
    case ServiceActionTypes.GetService: {
      return {
        ...state,
        loading: true
      };
    }

    case ServiceActionTypes.GetServiceFinished: {
      return {
        ...state,
        service: action.payload.data.service as Service,
        loading: false
      };
    }

    case ServiceActionTypes.GetServiceFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    case ServiceActionTypes.GetAllServices: {
      return {
        ...state,
        loading: true
      };
    }

    case ServiceActionTypes.GetAllServicesFinished: {
      return {
        ...state,
        services: action.payload.data.services as Service[],
        loading: false
      };
    }

    case ServiceActionTypes.GetAllServicesFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    case ServiceActionTypes.CreateService: {
      return {
        ...state,
        loading: true
      };
    }

    case ServiceActionTypes.CreateServiceFinished: {
      const serviceArray = [...state.services];
      serviceArray.push(action.payload.data.createService as Service);
      return {
        ...state,
        services: serviceArray,
        loading: false
      };
    }

    case ServiceActionTypes.CreateServiceFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    case ServiceActionTypes.UpdateService: {
      return {
        ...state,
        loading: true
      };
    }

    case ServiceActionTypes.UpdateServiceFinished: {
      return {
        ...state,
        service: action.payload.data.updateService as Service,
        loading: false
      };
    }

    case ServiceActionTypes.UpdateServiceFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    default: return state;
  }
}
